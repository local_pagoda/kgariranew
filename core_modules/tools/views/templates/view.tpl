<!-- JQuery DataTable Css -->
<link href="{PHP_START} echo base_url(); {PHP_END}assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="{PHP_START} echo base_url(); {PHP_END}assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>{PHP_START} echo lang('{TABLE_NAME}'){PHP_END}</h2>
		</div>

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							{PHP_START} echo lang('{TABLE_NAME}'){PHP_END}
						</h2>
					</div>

					<div class="body">
					</div>
				</div>
			</div>
		</div> <!-- close row clearfix -->

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							{PHP_START} echo lang('{TABLE_NAME}'){PHP_END}
						</h2>
						<div id="toolbar" style="padding:5px;height:auto">
							<p>
								<a href="#" class="btn bg-green waves-effect" onclick="create()" title="{PHP_START}  echo lang('create_{VIEWTABLE}'){PHP_END}">{PHP_START} echo lang('create'){PHP_END}</a>
								<a href="#" class="btn bg-blue-grey waves-effect" onclick="removeSelected()"  title="{PHP_START}  echo lang('delete_{VIEWTABLE}'){PHP_END}">{PHP_START} echo lang('remove_selected'){PHP_END}</a>
							</p>

						</div>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<table id="{VIEWTABLE}-table" class="table table-bordered table-striped table-hover dataTable " >
							<thead>
								{TABLE_FIELDS}
								<th>Action</th>

							</thead>

							<tbody>
							</tbody>

						</table>
					</div><!-- close body -->
				</div><!-- close card -->
			</div><!-- close col -->
		</div><!-- close rowclearfix -->

		<!--for create and edit {VIEWTABLE} form-->
		<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none">

			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Add or Edit Members</h4>
					</div>
					<div class="modal-body">
						<form id="form-{VIEWTABLE}" method="post" class="form-horizontal" >

							{FORMFIELDS}

						</form>
					</div> <!-- end body -->
					<div class="modal-footer">
						<button type="button" class="btn bg-green waves-effect" onClick="save()">{PHP_START} echo  lang('general_save'){PHP_END}</button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">{PHP_START} echo  lang('general_cancel'){PHP_END}</button>
					</div>

				</div><!-- end content -->
			</div><!-- end modal-dialog -->



		</div><!-- end edit modal -->

	</div><!-- end container fluid -->
</section> <!-- end content -->


<!-- Input Mask Plugin Js -->
<script src="{PHP_START} echo base_url(); {PHP_END}assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script language="javascript" type="text/javascript">

	var dataTable; 
	$(function(){

		$('.date').inputmask('yyyy-mm-dd', { placeholder: '____-__-__' });
		$('.email').inputmask({ alias: "email" });

		dataTable = $('#{VIEWTABLE}-table').DataTable({
			dom: 'Bfrtip',
			scrollX: true,
			"serverSide": true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "{PHP_START} echo site_url('{VIEWTABLE}/admin/{VIEWTABLE}/json'); {PHP_END}",type: 'POST' },
			columns: [
			{COLUMN_FIELDS}
			{ data: function(data,b,c,table) { 
				var buttons = '';

				buttons += "<button type='button' class='btn bg-grey waves-effect' data-toggle='modal' data-target='#edit-modal' onclick='edit("+table.row+")'>Edit</button>&nbsp";
				buttons += "<button class='btn bg-red waves-effect' onclick='remove{VIEWTABLE}("+data.{PRIMARY_KEY}+")'>Delete</button>";

				return buttons;
			}, name:'action',searchable: false},
			]
		});

	});

	function create(){
		$('#form-{VIEWTABLE}')[0].reset();
		$('#{PRIMARY_KEY}').val('');
		$('#edit-modal').modal('show');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = dataTable.row(index).data();

		$('#{PRIMARY_KEY}').val(row.{PRIMARY_KEY});
		$("#form-{VIEWTABLE}").find('input:checkbox').prop('checked',false);
		$("#form-{VIEWTABLE}").find('input:text,select').val(function(i,v){

			/*if(row.gender == 'M')
			{
				$('input:radio[name=gender][id=radio_1]').prop('checked',true);
			}else{
				$('input:radio[name=gender][id=radio_2]').prop('checked',true);
			}*/
			return row[this.name];
		});
		$('select').selectpicker('render');

		$("#form-{VIEWTABLE}").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	
	}

	function remove{VIEWTABLE}(index)
	{
		$.post("{PHP_START}  echo site_url('{VIEWTABLE}/admin/{VIEWTABLE}/delete_json'){PHP_END}", {id:[index]}, function(){
			dataTable.ajax.reload( null, false );
		});
	}

	function save()
	{
		var validator = $('#form-{VIEWTABLE}').validate();
		if(validator.form())
		{
			$.ajax({
				url: '{PHP_START}  echo site_url('{VIEWTABLE}/admin/{VIEWTABLE}/save'){PHP_END}',
				data: $('#form-{VIEWTABLE}').serialize(),
				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#edit-modal').modal('hide');
						$('#form-{VIEWTABLE}')[0].reset();
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
	}
</script>
<script type="text/javascript">
	/* Rules for the form*/
	$(function () {
		$('#form-{VIEWTABLE}').validate({
			rules: {
				// 'name': {
				// 	required: true
				// }
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	});
</script>