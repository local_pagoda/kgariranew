<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<section class="content">
    <div class="container-fluid">

        <div class="card">
            <div class="header">
                <h2><?php print $header?></h2>
                
            </div>
            <div class="body">

                <?php print form_open('auth/admin/acl_groups/form/'.$this->validation->id,array('class'=>'form-horizontal'))?>
                <fieldset>
                    <ol>
                        <li>
                            <?php print form_label($this->lang->line('access_name'),'name')?>
                            <div class="form-group">
                                <div class="form-line">
                                    <?php print form_input('name',$this->validation->name,'class="form-control" id="name"')?>
                                </div>
                            </div>
                        </li>

                        <li>
                            <?php print form_label($this->lang->line('access_disabled'),'disabled')?>
                            <?php print form_radio('disabled','1',$this->validation->set_radio('disabled','1'),'id="radio_disable1"')?>
                            <label for="radio_disable1">Yes</label> 
                            <?php print form_radio('disabled','0',$this->validation->set_radio('disabled','0'),'id="radio_disable2"')?>
                            <label for="radio_disable2">No</label> 
                        </li>
                        <li>
                            <?php print form_label($this->lang->line('access_parent_name'),'parent')?>
                            <?php print form_dropdown('parent',$groups,$this->validation->parent,'size=10 style="width:20.3em;"')?>
                        </li>
                        <li class="submit">
                            <?php print form_hidden('id',$this->validation->id)?>
                            <div class="buttons">
                               <button type="submit" class="btn btn-primary waves-effect" name="submit" value="submit">
                                  <?php print $this->bep_assets->icon('disk')?>
                                  <?php print $this->lang->line('general_save')?>
                              </button>

                              <a href="<?php print site_url('auth/admin/acl_groups')?>" class="btn btn-default waves-effect">
                                  <?php print $this->bep_assets->icon('cross')?>
                                  <?php print $this->lang->line('general_cancel')?>
                              </a>
                          </div>
                      </li>
                  </ol>
              </fieldset>
              <?php print form_close()?>

          </div>
      </div>

  </div>
</section>