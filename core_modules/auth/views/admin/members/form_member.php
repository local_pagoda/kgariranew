<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<section class="content">
    <div class="container-fluid">

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="card">
                    <div class="header">
                        <h2><?php print $header?></h2>
                        <p><?php print $this->lang->line('userlib_password_info')?></p>

                    </div>
                    <div class="body">


                        <?php print form_open('auth/admin/members/form/'.$this->validation->id,array('class'=>'form-horizontal'))?>
                        <fieldset>
                            <table style="width:100%">
                                <tr>
                                    <td style="width:20%">
                                        <label for="username"><?php echo $this->lang->line('userlib_username')?></label>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <div class="form-line">
                                            <input type="text" name="username" class="form-control" value="<?php echo $this->validation->username?>" id="username" style="width:300px"/>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="email"><?php echo $this->lang->line('userlib_email')?></label></td>
                                    <td>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" name="email" class="form-control" id="email" value="<?php echo $this->validation->email?>" style="width:300px"/>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="password"><?php echo $this->lang->line('userlib_password')?></label></td>
                                    <td>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="password" name="password" class="form-control" id="password" value="" style="width:300px"/>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="confirm_password"><?php echo $this->lang->line('userlib_confirm_password')?></label></td>
                                    <td>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="password" name="confirm_password" class="form-control" id="confirm_password" value="" style="width:300px"/>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="group"><?php echo $this->lang->line('userlib_group')?></label></td>
                                    <td>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <?php print form_dropdown('group',$groups,$this->validation->group,'id="group" class="form-control" size="10" style="width:20.3em;"')?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                 <td><label for="active"><?php echo $this->lang->line('userlib_active')?></label></td>
                                 <td>
                                     <div class="form-group">
                                        <div class="form-line">
                                            <?php print form_radio('active','1',$this->validation->set_radio('active','1'),'id="user_active1"')?>
                                            <label for="user_active1"><?php print $this->lang->line('general_yes')?></label> 
                                            <?php print form_radio('active','0',$this->validation->set_radio('active','0'),'id="user_active2"')?>
                                            <label for="user_active2"><?php print $this->lang->line('general_no')?></label> 
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="submit">
                                    <?php print form_hidden('id',$this->validation->id)?>
                                    <div class="buttons">
                                       <button type="submit" class="btn btn-primary waves-effect" name="submit" value="submit">
                                          <?php print  $this->bep_assets->icon('disk');?>
                                          <?php print $this->lang->line('general_save')?>
                                      </button>

                                      <a href="<?php print  site_url('auth/admin/members')?>" class="btn btn-default waves-effect">
                                          <?php print  $this->bep_assets->icon('cross');?>
                                          <?php print $this->lang->line('general_cancel')?>
                                      </a>
                                  </div>
                              </td>
                          </tr>
                      </table>
                  </fieldset>

                  <?php print form_close()?>
              </div>
          </div>

      </div>
  </div>

</div>
</section>