<link href="<?php echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>User Control</h2>
		</div>

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2> Users </h2>
						<h2> <a href="<?php echo site_url('auth/admin/members/form'); ?>" class="btn bg-indigo waves-effect" onclick="create()">Create</a> </h2>
					</div>

					<div class="body">
						<table id="user-table" class="table table-bordered table-striped table-hover dataTable ">
							<thead>
								<th >Sn</th>
								<th >Username</th>
								<th >Email</th>
								<th >Group</th>
								<th >Last Visit</th>
								<th >Created</th>
								<th >Modified</th>
								<th >Active</th>
								<th >Actions</th>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div><!-- end row -->

		
		<!-- <div class="modal fade in" id="user-modal" tabindex="-1" role="dialog" style="display: none;">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">User Modal</h4>
					</div>
					<div class="modal-body">
						<form id="user-form" method="post" class="form-horizontal" >

							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="username"><?php echo lang("username")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" name="username" id="username" class="form-control" placeholder="<?php echo lang('username'); ?>" required>
										</div>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="email"><?php echo lang("email")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="text" name="email" id="email" class="form-control" placeholder="<?php echo lang('email'); ?>" required>
										</div>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="password"><?php echo lang("password")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<input type="password" name="password" id="password" class="form-control" placeholder="<?php echo lang('password'); ?>" required>
										</div>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="group"><?php echo $this->lang->line('userlib_group')?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<?php echo form_dropdown('group',$groups,'id="group" class="form-control show-tick" ')?>
										</div>
									</div>
								</div>
							</div>

							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="active"><?php echo $this->lang->line('userlib_active')?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<?php print form_radio('active','1',$this->validation->set_radio('active','1'),'id="user_active1"')?>
											<label for="user_active1"><?php print $this->lang->line('general_yes')?></label> 
											<?php print form_radio('active','0',$this->validation->set_radio('active','0'),'id="user_active2"')?>
											<label for="user_active2"><?php print $this->lang->line('general_no')?></label> 
										</div>
									</div>
								</div>
							</div>
							<input type="hidden" name="id" id="id"/>


						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-link waves-effect" onClick="save()">SAVE CHANGES</button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
					</div>
				</div>
			</div>
		</div> -->
	</div>
</section>

<script language="javascript" type="text/javascript">
	var dataTable; 
	$(function(){

		dataTable = $('#user-table').DataTable({
			dom: 'Bfrtip',
			"serverSide": true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "<?php  echo site_url('auth/admin/members/json'); ?>",type: 'POST' },
			columns: [
			{ data: function (data, type, row, meta) {
				return meta.row + meta.settings._iDisplayStart + 1;
			},name: "sn"},
			{ data: 'username',name: "username"},
			{ data: "email",name: "email"},
			{ data: "group",name: "group"},
			{ data: "last_visit",name: "last_visit"},
			{ data: "created",name: "created"},
			{ data: "modified",name: "modified"},
			{ data: function(data){
				if(data.active)
					return "<i class='material-icons'>check</i>";
				return "<i class='material-icons'>cross</i>";
			},name: "active"},
			{ data: function(data,b,c,table) { 
				var buttons = '';
				//data-toggle='modal' data-target='#user-modal' onclick='edit("+table.row+")'
				buttons += "<a href='<?php echo site_url('auth/admin/members/form'); ?>/"+data.id+"' class='btn bg-grey waves-effect' ><i class='material-icons'>edit</i></a>&nbsp";

				if(data.id!='1')
					buttons += "<button class='btn bg-red waves-effect' onclick='removearchive("+data.archive_id+")'><i class='material-icons'>delete</i></button>";

				return buttons;
			}, name:'action',searchable: false},
			]
		});
	});

	function contact(index)
	{
		$('#contact-form').form('clear');
		var row = $('#user-table').datagrid('getRows')[index];
		if (row){
			$('#contact_email').val(row.email);
			$('#contact-dialog').window('open').window('setTitle','<?=lang('contact_user')?> ' + row.username );
		}
		else
		{
			$.messager.alert('Error','<?=lang('edit_selection_error')?>');				
		}		
	}

	function remove(index)
	{
		$.messager.confirm('Confirm','<?=lang('delete_confirm')?>',function(r){
			if (r){
				var row = $('#user-table').datagrid('getRows')[index];
				$.post('<?=site_url('auth/admin/members/delete_json')?>', {id:[row.id]}, function(){
					$('#user-table').datagrid('deleteRow', index);
					$('#user-table').datagrid('reload');
				});

			}
		});
	}

	function removeSelected()
	{
		var rows=$('#user-table').datagrid('getSelections');
		if(rows.length>0)
		{
			selected=[];
			for(i=0;i<rows.length;i++)
			{
				selected.push(rows[i].id);
			}

			$.messager.confirm('Confirm','<?=lang('delete_confirm')?>',function(r){
				if(r){				
					$.post('<?=site_url('auth/admin/members/delete_json')?>',{id:selected},function(data){
						$('#user-table').datagrid('reload');
					});
				}

			});

		}
		else
		{
			$.messager.alert('Error','<?=lang('edit_selection_error')?>');	
		}

	}

	function sendMessage()
	{
		$('#contact-form').form('submit',{
			url: '<?=site_url('auth/admin/members/send_message_json')?>',
			onSubmit: function(){
				return $(this).form('validate');
			},
			success: function(result){
				var result = eval('('+result+')');
				if (result.success)
				{
					$('#contact-form').form('clear');
$('#contact-dialog').window('close');		// close the dialog
$.messager.show({title: '<?=lang('success')?>',msg: result.msg});
} 
else 
{
	$.messager.show({title: '<?=lang('error')?>',msg: result.msg});
} //if close
}//success close

});		

	}	



</script>