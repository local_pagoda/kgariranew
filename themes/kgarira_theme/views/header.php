<!DOCTYPE HTML>

<html><head>
      
        <title>K garira</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        
        
  <link rel="stylesheet" href="<?php echo theme_url()?>/assets/css/bootstrap.min.css">
        
        <link rel="stylesheet" href="<?php echo theme_url()?>/assets/css/slick-theme.css">
        <link rel="stylesheet" href="<?php echo theme_url()?>/assets/css/slick.css">
    
        
        <link rel="stylesheet" href="<?php echo theme_url()?>/assets/css/global.css">

        <link rel="stylesheet" href="<?php echo theme_url()?>/assets/css/responsive.css">
        <link href="<?php echo theme_url()?>/assets/css/lightbox.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo theme_url()?>/assets/js/jquery-1.11.0.min.js"></script>
         
        
        <link href="jquery-3.2.1.minassets/date_time_picker/bootstrap-datetimepicker.css" rel="stylesheet">
  <script src="<?php echo theme_url() ?>assets/date_time_picker/moment-with-locales.js"></script>
        <script src="<?php echo theme_url() ?>assets/date_time_picker/bootstrap-datetimepicker.js"></script>
        
        
       
        
        <style>
      .dropdown-menu li a {
        color: #58AE5F; 
      }
    </style>
        
         <?php /** facebook login script **/?>
     <script>
            var fb_user_id = '';
          // This is called with the results from from FB.getLoginStatus().
          function statusChangeCallback(response) {
            
            // The response object is returned with a status field that lets the
            // app know the current login status of the person.
            // Full docs on the response object can be found in the documentation
            // for FB.getLoginStatus().
            if (response.status === 'connected') {
              // Logged into your app and Facebook.
              testAPI();
            } else if (response.status === 'not_authorized') {
              // The person is logged into Facebook, but not your app.
              document.getElementById('status').innerHTML = 'Please log ' +
                'into this app.';
            } else {
              // The person is not logged into Facebook, so we're not sure if
              // they are logged into this app or not.
              document.getElementById('status').innerHTML = 'Please log ' +
                'into Facebook.';
            }
          }
        
          // This function is called when someone finishes with the Login
          // Button.  See the onlogin handler attached to it in the sample
          // code below.
          function checkLoginState() {
            FB.getLoginStatus(function(response) {
              statusChangeCallback(response);
            });
          }
               
          window.fbAsyncInit = function() {
              FB.init({
                appId      : '218853404792659',
                cookie     : true,  // enable cookies to allow the server to access 
                                    // the session
                xfbml      : true,  // parse social plugins on this page
                version    : 'v2.4' // use version 2.2
              });
            
             
              // Now that we've initialized the JavaScript SDK, we call 
              // FB.getLoginStatus().  This function gets the state of the
              // person visiting this page and can return one of three states to
              // the callback you provide.  They can be:
              //
              // 1. Logged into your app ('connected')
              // 2. Logged into Facebook, but not your app ('not_authorized')
              // 3. Not logged into Facebook and can't tell if they are logged into
              //    your app or not.
              //
              // These three cases are handled in the callback function.
            
              /*FB.getLoginStatus(function(response) {
                    statusChangeCallback(response);
                    if (response.status === 'connected') {
                    //console.log(response.authResponse.accessToken);
                  }
              });*/
              
              
             
          };
        
            
        
        
          // Load the SDK asynchronously
          (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));
        
          // Here we run a very simple test of the Graph API after login is
          // successful.  See statusChangeCallback() for when this call is made.
          function testAPI() {
            //console.log('Welcome!  Fetching your information.... ');
            FB.api('/me?fields=id,name,first_name,middle_name,last_name,email,gender,birthday,location,picture', function(response) {

                /** check user in database**/
                var user_details = JSON.stringify(response);
                fb_user_id = response.id;
                var redirect = '';/*'<?php /*echo ($this->input->get('redirect_url'))?$this->input->get('redirect_url'):$redirect ;*/?>'*/;
                $.post('<?php echo site_url("auth/verifyFacebookUser")?>',{fbUser:user_details,redirect:redirect},function(redirect_url){
                    window.location.href = '<?php echo site_url()?>'+redirect_url;
                },'json');
                
                
            });
            
            // Only works after `FB.init` is called
            
          }
          
          function fbLogin() {
              FB.login(function(){testAPI();}, {scope: 'publish_actions,email'});
            }
        </script>
    </head>
    
    <body>
      
        <div id="wrapper">
 
          <div id="header-wrapper">
        <div class="container">
                  <div class="row">
                        <div class="col-sm-12">
                            <div class="header-top">
                                <div class="logo">
                                    <a href="<?php echo site_url()?>"><img src="<?php echo theme_url()?>/assets/images/logo.png"></a>
                                </div>

                                <div class="top-right">
                                    <div class="login">
                                    <a href="#" onclick="return add_event()">Add Events</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <?php if($this->session->userdata('id')) {?>
                                    <a href="#" class="dropdown-toggle" style="text-transform: uppercase;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('username')?> <span class="caret"></span></a>
                                    <?php } ?>
                                      <ul class="dropdown-menu" style="top: 30% !important; color: #58ae5f !important; 
                                right: 0 !important; left:inherit !important; border: 1px solid #EDEDED !important; box-shadow: 0 3px 12px rgba(0,0,0,.175) !important; border-radius: 0px !important;">
                                           <li><a href="<?php echo site_url('account')?>">My Account</a></li>
<!--                                        <li><a href="<?php echo site_url('account/wishlist')?>">My Wishlist</a></li>-->
                                        <li><a href="<?php echo site_url('account/TicketHistory')?>">View Ticket History</a></li>
                                        
                                        <?php if($this->session->userdata['group_id'] == 3){?>
                                            
                                            <li><a href="<?php echo site_url('account/event')?>">View Events</a></li>
                                            <li><a href="<?php echo site_url('account/event/TicketSales')?>">View Ticket Sales</a></li> 
                                            <li><a href="<?php echo site_url('account/guest')?>">View Guest List</a></li>
                                            
                                        <?php }else{?>
                                            
                                        <?php }?> <!-- end of if-->
                                        
                                        <li><a href="<?php echo site_url('account/change_password')?>">Change Password</a></li>
                                        <li><a href="<?php echo site_url('auth/logout')?>">Logout</a></li>
                                      </ul>
                                      
                                    
                  <?php 
                                    if(!$this->session->userdata('id')) {
                  ?>  
                                    <a href="<?php echo site_url('auth/register') ?>">REGISTER</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a href="<?php echo site_url('auth/login')?>">LOGIN</a>
                                    <?php } ?>
                                    </div>
                                    <form method="get"action="<?php echo site_url('search')  ?>" >
                                    <input type="text" placeholder="Search" id="q" name="q">
                                    <button class="submit"><img src="<?php echo theme_url()?>/assets/images/search.png"></button>
                                    </form>
                                </div>
                                <div class="clearfix"></div>
                                <div class="menu">
                                    <ul>
                                        <li><a href="<?php echo site_url('home/venue')?>">Bars & Pubs</a></li>
                                        <li><a href="<?php echo site_url('artist')?>">Bands</a></li>
                                        <li><a href="<?php echo site_url('home/contact')?>">Contacts</a></li>
                                        <li><a href="<?php echo site_url('home/gallery')?>">Galleries</a></li>

                                    </ul>
                                </div>                               
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!--Header Wrapper Ends-->
<div id="content-wrapper"> <!--Content Wrapper Starts-->
        <div class="container">
