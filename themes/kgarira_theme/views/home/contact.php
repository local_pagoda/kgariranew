<div id="content-wrapper">
    <div class="container">

        <div class="row">

            <div class="col-md-6">
                <div class="contact-title">
                    <h4> Contact Us</h4>
                </div><!-- end contact title -->
            </div>

            <div class="col-md-6">
                <div class="contact-title">
                    <h4> Contacts</h4>
                </div><!-- end contact title -->
            </div>
        </div>

        <div class="row" style="min-height: 600px">

            <div class="col-md-6">
                <form role="form" method="post" action="<?php echo site_url('home/contact')  ?>">
                    <div class="form-group">

                        <input type="text" placeholder="Frist Name" class="form-control"  name="first_name" id="first_name" required="">
                    </div>
                    <div class="form-group">

                        <input type="text" placeholder="Last Name" class="form-control" name="last_name" id="last_name" required="">
                    </div>
                    <div class="form-group">

                        <input type="email" placeholder = "Email" class="form-control" id="email" name="email" required="">
                    </div>
                    <div class="form-group">

                        <input type="text" placeholder="subject" class="form-control" id="subject" name="subject" required="">
                    </div>
                    <div class="form-group">

                        <textarea type="text" placeholder ="Message" class="form-control" id="message" name="message" required=""></textarea>
                    </div>

                    <button type="submit" class="btn btn-default">Submit</button>
                </form>

            </div><!-- end col md 6 -->

            <div class="col-md-6">
                <div class="row">
                <div class="col-md-12">
                   Feel free to write us at any time you want to. Please use our contact form to write your queries, suggestions feedbacks or anything you'd like to tell us.  
                   </div>

                <h5> </h5>                       
                    <div class="col-md-2">Email: </div>
                    <div class="col-md-10">hello@pagodalabs.com | info@kgarira.com </div>
                    <div class="col-md-2">Work: </div>
                    <div class="col-md-10">+977 1 5529041 </div>
                </div >
                </div>
            </div><!-- end col md 6 -->

        </div>


    </div><!-- end container --> 
</div><!-- end CONTENT WRAPPER -->