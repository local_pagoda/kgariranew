      <!-- Main hero unit for a primary marketing message or call to action -->
      <!--<div class="hero-unit">
        <h1>Welcome to Momo Engine!</h1>
        <p>This is a template for a simple marketing or informational website. It includes a large callout called the hero unit and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
        <p><a href="http://momoengine.com" class="btn btn-primary btn-large" target="_blank">Learn more &raquo;</a></p>
      </div>-->

      <!-- Example row of columns -->
  <link rel="stylesheet" href="<?php echo theme_url()?>/assets/css/wizard.css">
  <!-- <link rel="stylesheet" href="<?php echo theme_url()?>/assets/css/wizard.css" -->
<script type="text/javascript" src="<?php echo theme_url()?>/assets/js/wizard.js"></script>
      <section>
              <div class="bannerSlider windowheight"> 

              <?php foreach($banners as $banner){ ?>
                  <div class="bannerItem" style="background-image: url('<?php echo base_url();?>uploads/banner/<?php echo $banner['banner_image'] ?>');">
                 
                  </div>
               <?php  } ?>
              </div>
      </section>

      <section>
        <div class="row">
        
        
           <div class="col-sm-8"  >
                     <div class="row">
                      <div class="gallery-head">
                        <h4 style="color: #3ba837;">Current Events</h4>
                      </div>
                        <?php foreach ($events as $event) { ?>
                        <div class="col-md-6">
                              <div class="gallery-wrap">

                                <a >
                                
                                  <div class="home-event-img">
                                    <img src="<?php echo base_url();  ?>uploads/event/<?php echo $event['image']; ?>" onclick = "image_view('<?php echo $event["image"]; ?>' ,'event')">
                                  </div>
                                 
                                </a>                            

                              
                                 
                                  <div class="row">
                                    <div class="col-md-6">
                                   
                                      <div class="gallery-title">

                                        <h4><?php echo $event['name'] ?></h4>
                                        <p></p>
                                      </div> <!-- end gallery title -->

                                 
                                    </div>

                                    <div class="col-md-6 gallery-center">
                                      <div class="gallery-icon">
                                        <div class="cal-gallery">
                                          <a  class="calender">
                                            <img src="<?php echo theme_url()?>/assets/images/calender.png">
                                          </a>
                                           
                                          <a  class="add-event">
                                             <?php $date =  $event['start_date']; echo date("F d, Y", strtotime($date)); ?>
                                          </a>
                                           
                                        </div>
                                          
                                       </div> <!-- end gallery icon -->
                                     </div><!-- end col md 6 -->
                                   </div><!-- end row -->
                                 </div>
                           

                        </div>
                      <?php } ?> 
                       
                     </div>


              <div class="row">
                 
                        <div class="gallery-head">
                        <h4 style="color: #3ba837;">News Feeds</h4>
                      </div>
                      <?php foreach ($blogs as $blog) { ?>
                    <div class="col-md-6">
                          <div class="gallery-wrap">
                               
                                <a  >
                                 
                                  <div class="home-event-img">
                                     <img src="<?php echo base_url();  ?>uploads/blog/<?php echo $blog['image']; ?>" onclick="image_view('<?php echo $blog["image"]; ?>' , 'blog')">
                                  </div>
                                 
                                </a>                            

                              
                                 
                                  <div class="row">
                                    <div class="col-md-6">
                                 
                                      <div class="gallery-title">
                                        <h4>
                                        <?php echo $blog['title']; ?>
                                          
                                        </h4>
                                        <p></p>
                                      </div> <!-- end gallery title -->

                           
                                    </div>

                                    <div class="col-md-6 gallery-center">
                                      <div class="gallery-icon">
                                        <div class="cal-gallery">
                                          <a  class="calender">
                                            <img src="<?php echo theme_url()?>/assets/images/calender.png">
                                          </a>
                                           

                                          <a  class="add-event">
                                             <?php $date = $blog['created_date']; echo date("F d, Y", strtotime($date)); ?>
                                          </a>
                                           
                                        </div>
                                          
                                       </div> <!-- end gallery icon -->
                                     </div><!-- end col md 6 -->
                                   </div><!-- end row -->
                                 </div>

             
                  </div>
                  <?php } ?>
              </div>
          </div>

         
          <div class="col-md-4">
              <div class="gallery-head">
                        <h4 style="color: #3ba837;">Upcoming Events</h4>
                      </div>
                    
                              <?php foreach ($upcoming_events as $upcoming_event) { ?>
                              <div class="gallery-wrap">

                                <a  >
                               
                                  <div class="home-event-img">
                                    <img src="<?php echo base_url();  ?>uploads/event/<?php echo $upcoming_event['image']; ?> " onclick="image_view('<?php echo $upcoming_event["image"]; ?>' , 'event' )">
                                  </div>
                                  
                                </a>                            

                              
                                 
                                  <div class="row">
                                    <div class="col-md-6">
                                    
                                      <div class="gallery-title">

                                        <h4> <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#ticketModal" onclick="tickets('<?php echo $upcoming_event['id']; ?>')">Buy Ticket</button></h4>
                                        <p></p>
                                      </div> <!-- end gallery title -->

                                
                                    </div>

                                    <div class="col-md-6 gallery-center">
                                      <div class="gallery-icon">
                                        <div class="cal-gallery">
                                          <a  class="calender">
                                            <img src="<?php echo theme_url()?>/assets/images/calender.png">
                                          </a>
                                            
                                          <a  class="add-event">
                                             <?php $date = $event['start_date']; echo date("F d, Y", strtotime($date)); ?>
                                          </a>
                                          
                                        </div>
                                          
                                       </div> <!-- end gallery icon -->
                                     </div><!-- end col md 6 -->
                                   </div><!-- end row -->
                                 </div>
                               <?php } ?>

                             
          
          </div>

        </div>
     <div class="row">
     <div class="gallery-head">
          <h4 style="color: #3ba837;">Gallery</h4>
     </div>
                    
      <?php foreach ($galleries as $gallery) { ?>
          <div class="col-md-4">
              
                             

                              <div class="gallery-wrap">

                                <a  >
                               
                                  <div class="home-event-img">
                                              <a href="<?php echo site_url();?>home/find_image/<?php echo $gallery['gallery_id']; ?>"><img src="<?php echo base_url();  ?>uploads/gallery/<?php echo $gallery['image_name']; ?>"></a>
                                  </div>
                                  
                                </a>                            

                              
                                 
                                  <div class="row">
                                    <div class="col-md-6">
                                    
                                      <div class="gallery-title">

                                        <h4><?php echo $gallery['album_name'] ?></h4>
                                        <p></p>
                                      </div> <!-- end gallery title -->

                                
                                    </div>

                                    <div class="col-md-6 gallery-center">
                                      <div class="gallery-icon">
                                        <div class="cal-gallery">
                                          <a  class="calender">
                                            <img src="<?php echo theme_url()?>/assets/images/calender.png">
                                          </a>
                                            
                                          <a  class="add-event">
                                             <?php $date = $gallery['created_date']; echo date("F d, Y", strtotime($date)); ?>
                                          </a>
                                          
                                        </div>
                                          
                                       </div> <!-- end gallery icon -->
                                     </div><!-- end col md 6 -->
                                   </div><!-- end row -->
                                 </div>
                               
                             
          
          </div>
      <?php } ?>
    
      </div>   
       
      </section>
      <section>

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
            
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body" id="image_div">
             
              </div>
              <div class="modal-footer">
              
              </div>
            </div>

          </div>
        </div>
      </section>
     
<section>
<div id="ticketModal" class="modal fade" role="dialog" style="background-color: #d8d8d8; opacity: 0.8;">
            <div class="modal-dialog">
           
              <!-- Modal content-->
              <div class="modal-content modal-lg" style="margin-left: -146px !important;">
                <div class="modal-header" style="margin-top: -80px;">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                 
                </div>
        <div class="wizard" style="margin-top: -50px !important;">
            <div class="wizard-inner">
                <div class="connecting-line"></div>
                <ul class="nav nav-tabs" role="tablist">

                    <li role="presentation" class="active">
                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                            
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                           
                        </a>
                    </li>
                    <li role="presentation" class="disabled">
                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                            
                        </a>
                    </li>

                    <li role="presentation" class="disabled">
                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                           
                        </a>
                    </li>
                </ul>
            </div>

            <form role="form" id="ticket_buy_form" action="<?php   echo site_url('home/store_user_data')?>" method = "post">
                <div class="tab-content">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        
                             <div class="modal-body">
                              <h3 class="modal-title" id ="event-title" style="margin-top:-30px;color:  #4f4b4b; text-align: center;"></h3>
                  <div id = "event-image"></div>
                  <br>
                  <br>
                  <br>
                  <h4 id = "event-type"></h4>
                  <h4 id="event-description"></h4>
                  <h4 id="event-country"></h4>
                  <h4 id="event-venue"></h4>
                  <br>
                  <br>
                  
                    <label>Tickets</label>
                      <select id="ticket_quantity" name="ordered_quantity" onchange="calculate_price()">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>

                      </select>
                      <select id = "event_ticket_price" name="ordered_ticket_price" onchange="calculate_price()"></select>
                      <lable id ="ticket_currency"></lable><br>
                      <input type="text" name="total_price" style="background-color: #f2d191; width: 500px;" id="calculation" disabled>

                   <input type="hidden" name="event_id" id="event_id">
                   <!-- <button class="btn btn-primary btn-block" id="button_sell" onclick="ticket_sell()">Buy Now</button> -->

                </div>
                        <ul class="list-inline pull-right">


                            <li><button type="button" class="btn btn-primary next-step">Buy Now</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                       <h3>Your Personal Details</h3>
                       <span><label>First Name:*</label><input type="text" name="buyer_first_name"  class="form-control"></span>
                       <span><label>Last Name:*</label><input type="text" name="buyer_last_name"  class="form-control"></span>
                       <span><label>Email:*</label><input type="text" name="buyer_email_address"  class="form-control"></span>
                       <span><label>Phone Number:*</label><input type="text" name="buyer_phone_number"  class="form-control"></span>
                       <span><label>Country:</label><input type="text" name="buyer_country"  class="form-control"></span>
                       <span><label>Address:</label><input type="text" name="buyer_address"  class="form-control"></span>
                       <span><label>City:</label><input type="text" name="buyer_city"  class="form-control"></span>
                       <span><label>Zip:</label><input type="text" name="buyer_country_zip"  class="form-control"></span>


                       <input type="hidden" name = "processing" id="processing_fees">

                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            <li><button type="button" class="btn btn-primary next-step">Save and continue</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step3">
                        <h3>Select Payment Method</h3>

                          <div id="paypalView" class="form-group">

                              <div class="row">  
                              
                                    <div class="col-sm-12">

                                  <label class="radio inline">

                                    <input type="radio" name="payment_method" value="npay"><img src="http://localhost/kgarira/themes/default/assets/images/nPay.png">

                                  </label>

                                </div>

                                <div class="col-sm-12">

                                  <label class="radio inline">

                                    <input type="radio" name="payment_method" value="ipay"><img src="http://localhost/kgarira/themes/default/assets/images/ipay.png">

                                  </label>

                                </div>
                                 <div class="col-sm-12">

                                  <label class="radio inline">

                                    <input type="radio" name="payment_method" value="cod"><img src="http://localhost/kgarira/themes/default/assets/images/cod.png">

                                  </label>

                                </div>
                              

                              </div> 

                            </div>
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-default prev-step">Previous</button></li>
                            
                            <li><button type="button" class="btn btn-primary btn-info-full next-step" onclick="ticket_sell()">Save and continue</button></li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="complete">
                        <h3>Complete</h3>
                        <p>You have successfully completed all steps.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </form>
        </div>
        
               
                
              </div>

            </div>
          </div>
    </section>


      <hr>

<script type="text/javascript">
var processing = 0;
  function tickets(id) {
    $.ajax({
        url: '<?php   echo site_url('home/upcoming_event')?>',
        data: {id:id},
        dataType: 'json',
        success: function(result){

            $('#ticketModal').modal('show');
            $('#event-title').html('Detail of '+result[0].name);
            $('#event_id').val(result[0].id);

            $('#event-type').html('Event type: '+result[0].type_id);
            $('#event-description').html('Event Description: '+result[0].description);
            $('#event-country').html('Country: '+result[0].country_id);
            $('#event-venue').html('Venue: '+result[0].venue_id);
           $('#event-image').html('<img src="<?php echo base_url()?>uploads/event/'+result[0].image+'" width="200px">');
           processing = parseInt(result[0].processing_fees);
           $('#processing_fees').val(processing)
            $('#event_ticket_price').empty();
           $.each( result.ticket_data, function(k, v){
        
              $('#event_ticket_price').append($('<option/>', { 
                  value: v.ticket_price,
                  text : v.ticket_price 
              }));

              $('#ticket_currency').html(v.currency);

            });

            calculate_price();
        },
        type: 'POST'
      });

   
  }
  
</script>
  <!-- <script src="<?php echo theme_url()?>/assets/js/form-wizard.js"></script> -->

<script type="text/javascript">
 
  function calculate_price(){
   
      var quantity = $('#ticket_quantity').val();
     var price = $('#event_ticket_price').val();
       $('#button_sell').show();
     console.log(quantity);
     console.log(price);

    if(processing >0 && price >0)
    {

     quantity = parseInt(quantity);
     price = parseInt(price);
     var total = quantity * price;

     var processing_fees = total * processing/100;
     var g_total = total + processing_fees;
     // alert(total);
      $('#calculation').val('The total Price will be:'+g_total+'  (Booking Charge'+processing_fees+')');

    }
    else
    {
      $('#button_sell').hide();
      
      $('#calculation').html('Free Event');
  }
 } 

 function ticket_sell()
 {
     $('#ticket_buy_form').submit();
 }


</script>
