
<div id="content-wrapper" class="content-wrap">
    <div class="my-container">
        <div class="row">
          <div class="col-sm-12 body">
           <h1>Gallery</h1>
            <hr>
                   
            <div id="gallery-main" role="gallery-main">
             <?php foreach($images as $image){?>
               <div class="image-info" style="margin:10px">  
                     
                         <img src="<?php echo base_url()?>uploads/gallery/<?php echo $image['image_name'];?>" width="50%" onclick="image_view('<?php echo $image["image_name"]; ?>' , 'gallery')">
                 
                    <div class="caption">
                      
                    </div>
                </div>
              <?php }?>
              
                <!-- End of grid blocks -->
              </div>
              <div class="clearfix"></div>
              <br/>
        
          </div>
        </div>
    </div>
</div>  <!--Content Wrapper--> 