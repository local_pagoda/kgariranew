<div class="row">
     <div class="gallery-head">
          <h4 style="color: #3ba837;">Gallery</h4>
     </div>
                    
      <?php foreach ($galleries as $gallery) { ?>
          <div class="col-md-4">
              
                             

                              <div class="gallery-wrap">

                                <a  >
                               
                                  <div class="home-event-img">
                                              <a href="<?php echo site_url();?>home/find_image/<?php echo $gallery['gallery_id']; ?>"><img src="<?php echo base_url();  ?>uploads/gallery/<?php echo $gallery['image_name']; ?>"></a>
                                  </div>
                                  
                                </a>                            

                              
                                 
                                  <div class="row">
                                    <div class="col-md-6">
                                    
                                      <div class="gallery-title">

                                        <h4><?php echo $gallery['album_name'] ?></h4>
                                        <p></p>
                                      </div> <!-- end gallery title -->

                                
                                    </div>

                                    <div class="col-md-6 gallery-center">
                                      <div class="gallery-icon">
                                        <div class="cal-gallery">
                                          <a  class="calender">
                                            <img src="<?php echo theme_url()?>/assets/images/calender.png">
                                          </a>
                                            
                                          <a  class="add-event">
                                             <?php $date = $gallery['created_date']; echo date("F d, Y", strtotime($date)); ?>
                                          </a>
                                          
                                        </div>
                                          
                                       </div> <!-- end gallery icon -->
                                     </div><!-- end col md 6 -->
                                   </div><!-- end row -->
                                 </div>
                               
                             
          
          </div>
      <?php } ?>
    
      </div>   