<div class="row">
     <div class="gallery-head">
          <h4 style="color: #3ba837;">Venues</h4>
     </div>
                    
      <?php foreach ($venues as $venue) { ?>
          <div class="col-md-4">
              
                             

                              <div class="gallery-wrap">

                                  <div class="home-event-img">
                                            <a href="<?php echo site_url();?>home/venue_details/<?php echo $venue['id']; ?>">  <img src="<?php echo base_url();  ?>uploads/venue/<?php echo $venue['venue_image']; ?>"></a>
                                  </div>
                                  
                                                       

                              
                                 
                                  <div class="row">
                                    <div class="col-md-6">
                                    
                                      <div class="gallery-title">

                                        <h4><?php echo $venue['venue_name'] ?></h4>
                                        <p></p>
                                      </div> <!-- end gallery title -->

                                
                                    </div>

                                    <div class="col-md-6 gallery-center">
                                      <div class="gallery-title">
                              
                                      <h4><?php echo $venue['venue_location']; ?>,<?php echo $venue['venue_city']; ?></h4>
                                      </div>
                                     </div><!-- end col md 6 -->
                                   </div><!-- end row -->
                                 </div>
                               
                             
          
          </div>
      <?php } ?>
    
      </div>   