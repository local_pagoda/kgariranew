

           <div id="footer-wrapper">
            <div class="container">
             <div class="row">
              <div class="col-sm-12">
                <div class="footer-menu">
                  <ul>
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Bars & Pubs</a></li>
                    <li><a href="#">Bands</a></li>                            
                  </ul>
                  <p>Copyright &copy 2015 kgarira.com  |  All Rights reserved</p>
                </div>
                <div class="footer-links">
                  <div class="footer-social">
                    <a href="#" class="fb social"></a>
                    <a href="#" class="tw social"></a>
                    <a href="#" class="google social"></a>
                    <a href="#" class="yt social"></a>
                  </div>
                  <div class="clearfix"></div>
                  <div class="contact">
                    <p>977-015-253678 | 015-253-678</p>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>  <!--Footer Wrapper Ends-->

      </div> <!--Main Wrapper Ends-->

      <script src="<?php echo theme_url()?>assets/js/bootstrap.min.js"></script>
      <script src="<?php echo theme_url()?>assets/js/lightbox.js"></script>
      <script>
        lightbox.option({
          'alwaysShowNavOnTouchDevices': false,
          'showImageNumberLabel':false

        })
      </script>
      <div class="modal fade" id="addevent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
       <div class="background-image"></div> 
       <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <div class="col-sm-12">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Add New Event</h4>
            </div>
          </div>
          <div class="modal-body event-detail">
            <div class="col-sm-12">
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#add_event_tab">Add Event</a></li>
                <li><a data-toggle="tab" href="#fb_event_tab">Add Facebook Event</a></li>

              </ul>
              <div class="tab-content">
                <div id="add_event_tab" class="tab-pane fade in active">
                 <form method="post" action="<?php echo site_url('event/addEvent') ?>"id="form-addevent" name="form-addevent" enctype="multipart/form-data">

                   <div class="row">
                     <div class="col-lg-6" class="form-horizontal">
                       <input type="text" class="form-control" id="event_name"placeholder="Event Name" id="event_name"name="event_name" required="">

                     </div>
                     <div class="col-lg-6">
                       <select  type="text" name="event_type_id" id="event_type_id" required class="form-control" >     
                         <option selected disabled>Event Types</option>
                         <?php   if($event_venues_types){ ?>
                         <?php foreach ($event_venues_types as $key => $value) {?>
                         <option value="<?php echo $value['event_type_id']; ?>"><?php echo $value['event_type']; ?></option>
                         <?php   } ?>
                         <?php   } ?>
                       </select>
                     </div>

                   </div>
                   <div class="row">
                    <div class="col-lg-6">
                      <select  type="text" name="venue_id"id="venue_id" required=""class="form-control">
                        <option selected disabled value="">Venue</option>
                        <?php   if($event_venues){ ?>
                        <?php foreach ($event_venues as $key => $value) {?>

                        <option value="<?php echo $value['venue_id']; ?>"><?php echo $value['venue_name']; ?></option>
                        <?php   } ?>
                        <?php   } ?>
                      </select>
                    </div>
                    <div class="col-lg-6">
                      <select  type="text" id="promoter_id" name="promoter_id" required="" class="form-control">
                        <option selected disabled value="">Promoter</option>
                        <?php   if($promoters){ ?>
                        <?php foreach ($promoters as $key => $value) {?>
                        <option value="<?php echo $value['id']; ?>"><?php echo $value['username']; ?></option>
                        <?php   } ?>
                        <?php   } ?>
                      </select>
                    </div>

                  </div>

                  <div class="row">
                    <div class="col-lg-6">
                      <select  type="text" name="country_id" id="country_id" required="" class="form-control">
                        <option selected disabled value="">Country</option>
                        <?php   if($countries){ ?>
                        <?php foreach ($countries as $key => $value) {?>
                        <option value="<?php echo $value['country_code']; ?>"><?php echo $value['country_name']; ?></option>
                        <?php   } ?>
                        <?php   } ?>
                      </select>
                    </div>
                    <div class="col-lg-6">
                     <label>Event Image</label><input type="file"  id="image" name="event_image"  >

                   </div>

                 </div>


                 <div class="row">

                   <div class="col-lg-10" class="form-horizontal">


                     <textarea class="form-control" id="event_description" name="event_description" required="" placeholder="Event Description"></textarea>
                   </div>

                 </div>
                 <div class="row">
                   <div class="col-lg-6" class="form-horizontal">
                     <div class='input-group date'  class="form-control" >
                       <input data-format="yyyy-MM-dd hh:mm:ss" type='text' class="form-control" id='event_start' name="event_start_date" placeholder="Start Date" />

                     </div>

                   </div>
                   <div class="col-lg-6" class="form-horizontal">
                     <div class='input-group date'  class="form-control" >
                       <input data-format="yyyy-MM-dd hh:mm:ss" type='text' class="form-control" id='event_end' name="event_end_date" placeholder="End Date" />

                     </div>

                   </div>





                 </div>
                 <div class="row">
                   <div class="col-lg-9" class="form-horizontal">
                     <label>Allow Ticket Sell</label>
                     <input type="radio" name="allow_ticket_sell" id='allow_ticket_sell'  checked="" value="1">Yes
                     <input type="radio" name="allow_ticket_sell" id='allow_ticket_sell'  value="0">No
                   </div>


                 </div>

                                   <!-- <div class="row">
                                     <div class="col-lg-9" class="form-horizontal">
                                       <label>Status</label>
                                       <input type="radio" name="status" checked="" value="1">Yes
                                       <input type="radio" name="status" value="0">No
                                     </div>



                                   </div> -->

                                   <div class="modal-footer">

                                     <div class="col-sm-12">
                                      <button type="submit"  class="btn btn-success btn-xs">Add Event</button>

                                      <!--                                    <a href="#" onclick="loadbtn()" >Add Event</a>-->
                                      <!--                        <a href="#" onclick="submitAddEventForm()" >Add Event</a>-->

                                    </div>


                                  </div>
                                </form>

                              </div>


                              <div id="fb_event_tab" class="tab-pane fade">
                                <label>Facebook Event Url:</label>
                                <input type="text" class="form-control" id="fb_url" name="fb_url" required="">
                                <button type="submit" class="btn btn-success btn-xs"  id="fb_event_submit">Add event </button>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </body>

                    </html>


                    

                    <script>

                      function submitCheckoutForm()
                      {
                        $('#form-checkout').submit();
                      }
                    </script>
                    <script>
                     function add_event()
                     {
                      <?php  if($this->session->userdata('id'))
                      {
                        $id=1;
                      }
                      else
                      {
                        $id=0;
                      }
                      ?>

                      var id='<?php echo $id ?>';  

                      if(id==1)
                      {
                        $('#addevent').modal();
                      }
                      else
                      {
                        window.location.assign("<?php echo site_url('auth/login') ?>");
                      }


                    }
                  </script>

                  <script type="text/javascript">
  
                        function image_view(image , name)
                        {
                          //   var image_name = image;
                          //   var catagory = name;
                          //   $.ajax({
                          //   url: '<?php   echo site_url('home/home/find_image')?>',
                          //   data: image_name + catagory;
                          //   dataType: 'json',
                          //   success: function(result){
                          //     console.log(result);
                          //     if(result.success)
                          //     {
                          //       $('#ticket-modal').modal('hide');
                          //       $('#form-ticket')[0].reset();
                          //       ticket_dataTable.ajax.reload( null, false );
                          //     }
                          //   },
                          //   type: 'POST'
                          // });
                            $('#image_div').html('<img src="<?php echo base_url()?>uploads/' + name + '/' + image + '">');
                            $('#myModal').show();
                            $('#myModal').modal('show'); 
                        }
              </script>

                  <script src="<?php echo theme_url()?>/assets/js/slick.min.js"></script>

                  <script type="text/javascript">
                    $(document).ready(function(){
                      var windowheight = $(window).height();
                        console.log(windowheight);
                        
                        $('.bannerSlider').slick({
                            infinite: true,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                              autoplay: true,
                              autoplaySpeed: 2000,
                            arrow: false,
                            dots: true,
                            
                            fade: true,
                            
                            
                        });
                        
                    });
 

                  </script>


