
<p style="font-size: 16px"><strong>Dear Kgarira,</strong></p>

<p><strong>Tickets for {EVENT_NAME} ({EVENT_ID}) has been purchased. Please see the following Details:</strong></p>
<p>Event ID: {EVENT_ID}</p>
<p>Event Name: {EVENT_NAME}</p>
<p>Event Starts At:  {EVENT_STARTS}</p>
<p>Event Ends At: {EVENT_ENDS} </p>

<p><strong>Which will held at</strong></p>
<p>Venue Name:{VENUE_NAME} </p>
<p>Venue Location : {VENUE_LOCATION}</p>
<p>Venue City:{VENUE_CITY} </p>

<p><strong>And the ticket has been ordered by</strong></p>
<p>Ordered By:{ORDER_BY} </p>
<p>Ordered Person Email: {ORDERER_EMAIL}</p>
<p>Ordered Person Address: {ORDERER_ADDRESS}</p>
<p>Ordered Person CONTACT: {ORDERER_CONTACT}</p>

<p>Ticket Currency: {CURRENCY}</p>

<p><strong>Ticket Pricing Detail</strong></p>
<p>No. of Tix:{QUANTITY}</p>
<p>Ticket Price:{PRICE}</p>
<p>Ticket Price without Processing Fees:{TWPF}</p>
<p>Ticket Processing Fees {PROCESSING_FEES}</p>
<p>Total: {TOTAL}</p>
<p>Order Date: {ORDER_DATE}</p>


<p>Thank you</p>
<p>&nbsp;</p>
