
<p><strong>Dear {ORDER_BY},</strong> </p>
<br>
<p>You have brought {QUANTITY} of ticket for
{EVENT_NAME} which will be starting at {EVENT_STARTS}.
</p>
<p>The event will be held at {VENUE_NAME} which is located at {VENUE_LOCATION}</p>
<br>
<p>And also you ticket pricing detail is given below. Please check the details properly</p>
<p>No. of Tix:{QUANTITY}</p>
<p>Ticket Price:{PRICE}</p>
<p>Ticket Price without Processing Fees:{TWPF}</p>
<p>Ticket Processing Fees {PROCESSING_FEES}</p>
<p>Total: {TOTAL}</p>
<p>Order Date: {ORDER_DATE}</p>
<br><p>&nbsp;</p><p>&nbsp;</p>
<p>Thank you</p>
<p>Kgarira.com</p>