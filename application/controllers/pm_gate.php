<?php 
/* Single page for all user payment checkouts*/

class Pm_gate extends Public_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->module_model('ticket','ticket_model');
		$this->load->library('cart');
                //$this->load->library('paypal_lib');
		$this->load->module_model('event','event_model');
		$this->load->module_model('venue','venue_model');
		// $this->load->module_model('event','gallery_model');
		$this->load->module_model('ticket','ticket_model');
		$this->load->module_model('ticket_order','ticket_order_model');
		$this->lang->module_load('event','event');
		
		// $this->load->helper('common');
		$this->load->helper('barcode');
		$this->load->library('email');
		$this->load->helper(array('url','file'));
		$this->load->library('session');
		$this->load->helper('date');
		require_once APPPATH. 'libraries/newcode/code128.class.php';

	}

	public function cod()
	{
		$this->confirmation_mail();
		
		$data['view_page'] ="cod/sucess_page";

		$this->load->view($this->_container,$data);

	}

  //all function of ipay gateway
	public function ipay()
	{

		foreach ($this->cart->contents() as $data)
		{
		// $url=base_url()'pm_gate/ipay_success?success=1';
			$data['success_url'] ='http://localhost/kgariranew/pm_gate/ipay_success?success=1';
       	// print_r($data['success_url']);exit;
			$data['cancel_url'] = 'http://localhost/kgariranew/pm_gate/cancel?cancel=0';
			$this->load->view('ipay/ipay_form',$data);

		}
		
		
	}

	public function ipay_success()
	{

		if($this->input->get('success') == 1)
		{
			set_time_limit(0);
			foreach ($this->cart->contents() as $key => $value);
			$post = $this->input->post();

			$merchantid_from_db = $this->_directory_check($post['transactionid']);

			if($this->input->get('success') == 1 && !$merchantid_from_db){
				$data = $this->validate_ipay($post);

			}

			$data = explode('|', $data);   
			/*reference: http://www.html-form-guide.com/php-form/php-form-submit.html*/

			if($data[0] != "Sucess") 
			{
				redirect('pm_gate/cancel');
			}

			$this->confirmation_mail();
			$data['view_page'] ="ipay/success_page";

			$this->load->view($this->_container,$data);
		}

	}

	private function _ipay_data($data)
	{
		$transactionid = $data['transactionid'];
	
	}

	public function validate_ipay($post) 
	{

		$url  = 'https://www.ipay.com.np/Payment/VerifyTransaction';
		$returnUrl = 'http://localhost/kgariranew/pm_gate/ipay_success?success=2';
		$errorUrl = 'http://localhost/kgariranew/pm_gate/cancel?cancel=1';

			     //create array of data to be posted
			   $post_data['MerchantId']           = '939a8cc6-cda2-47f7-a25a-a1ccf0f45721';// test = '939a8cc6-cda2-47f7-a25a-a1ccf0f45721'
			   $post_data['TransactionId']        = $post['transactionid'];
			   $post_data['Confirmation_Code']    = $post['confirmation_code'];
			   $post_data['ReturnUrl']            = $returnUrl;
			   $post_data['ErrorUrl']             = $errorUrl;
			   $post_data['Amount']               = $post['amount'];
			   $post_data['SessionKey']           = $post['session_key'];

			     //traverse array and prepare data for posting (key1=value1)
			   foreach ( $post_data as $key => $value) 
			   {
			   	$post_items[] = $key . '=' . $value;
			   }
			    //create the final string to be posted using implode()
			   $post_string = implode ('&', $post_items);

			    //create cURL connection
			   $curl_connection =  curl_init($url);
			    //set options
			   curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
			   curl_setopt($curl_connection, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
			   curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
			   curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);
			   curl_setopt($curl_connection, CURLOPT_FOLLOWLOCATION, 1);
			    //set data to be posted
			   curl_setopt($curl_connection, CURLOPT_POSTFIELDS, $post_string);

			    //perform our request
			   $result = curl_exec($curl_connection);
			    //close the connection
			   curl_close($curl_connection);

			   return $result;
			}

 //end ipay gateway

			private function _directory_check($merchantID)
			{	
				$merchantid_from_db = $this->db->get_where('tbl_ticket_orders',array('merchant_txn_id'=>$merchantID));
				return $merchantid_from_db->num_rows();
			}

	//all function for npay
			public function npay()
			{       
				$this->load->view('npay/npay_form');
			}
			public function npay_success() {

	        //set_time_limit(0);
				$data = $this->in_validate_npay();
	        // $merchantid_from_db = $this->_directory_check($this->input->get('MERCHANTTXNID'));

				$status= $data['SOAP:ENVELOPE']['SOAP:BODY']['CHECKTRANSACTIONSTATUSRESPONSE']['CHECKTRANSACTIONSTATUSRESULT']['STATUS_CODE'];
				$status_transaction = $data['SOAP:ENVELOPE']['SOAP:BODY']['CHECKTRANSACTIONSTATUSRESPONSE']['CHECKTRANSACTIONSTATUSRESULT']['TRANSACTION_STATUS'];

		// echo "<pre>";print_r($data);exit;
				if($status=="0" && $status_transaction == "SUCCESS")
				{
	        		
	        		$this->confirmation_mail();
			    }
			    else{
			    	redirect('pp_gate/cancel');
			    }
			    $data['header']="nPay SCT Success";   
			    $data['pp_info'] = $this->input->post();
			        // echo '<pre>'; print_r($data['pp_info']);exit;
			    $data['view_page'] ="npay/success_page";

			    $this->load->view($this->_container,$data);
	}

	public   function XMLtoArray($XML)
	{
		$xml_parser = xml_parser_create();
		xml_parse_into_struct($xml_parser, $XML, $vals);
		xml_parser_free($xml_parser);
		                // wyznaczamy tablice z powtarzajacymi sie tagami na tym samym poziomie
		$_tmp='';
		foreach ($vals as $xml_elem) {
			$x_tag=$xml_elem['tag'];
			$x_level=$xml_elem['level'];
			$x_type=$xml_elem['type'];
			if ($x_level!=1 && $x_type == 'close') {
				if (isset($multi_key[$x_tag][$x_level]))
					$multi_key[$x_tag][$x_level]=1;
				else
					$multi_key[$x_tag][$x_level]=0;
			}
			if ($x_level!=1 && $x_type == 'complete') {
				if ($_tmp==$x_tag)
					$multi_key[$x_tag][$x_level]=1;
				$_tmp=$x_tag;
			}
		}
		                // jedziemy po tablicy
		foreach ($vals as $xml_elem) {
			$x_tag=$xml_elem['tag'];
			$x_level=$xml_elem['level'];
			$x_type=$xml_elem['type'];
			if ($x_type == 'open')
				$level[$x_level] = $x_tag;
			$start_level = 1;
			$php_stmt = '$xml_array';
			if ($x_type=='close' && $x_level!=1)
				$multi_key[$x_tag][$x_level]++;
			while ($start_level < $x_level) {
				$php_stmt .= '[$level['.$start_level.']]';
				if (isset($multi_key[$level[$start_level]][$start_level]) && $multi_key[$level[$start_level]][$start_level])
					$php_stmt .= '['.($multi_key[$level[$start_level]][$start_level]-1).']';
				$start_level++;
			}
			$add='';
			if (isset($multi_key[$x_tag][$x_level]) && $multi_key[$x_tag][$x_level] && ($x_type=='open' || $x_type=='complete')) {
				if (!isset($multi_key2[$x_tag][$x_level]))
					$multi_key2[$x_tag][$x_level]=0;
				else
					$multi_key2[$x_tag][$x_level]++;
				$add='['.$multi_key2[$x_tag][$x_level].']';
			}
			if (isset($xml_elem['value']) && trim($xml_elem['value'])!='' && !array_key_exists('attributes', $xml_elem)) {
				if ($x_type == 'open')
					$php_stmt_main=$php_stmt.'[$x_type]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
				else
					$php_stmt_main=$php_stmt.'[$x_tag]'.$add.' = $xml_elem[\'value\'];';
				eval($php_stmt_main);
			}
			if (array_key_exists('attributes', $xml_elem)) {
				if (isset($xml_elem['value'])) {
					$php_stmt_main=$php_stmt.'[$x_tag]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
					eval($php_stmt_main);
				}
				foreach ($xml_elem['attributes'] as $key=>$value) {
					$php_stmt_att=$php_stmt.'[$x_tag]'.$add.'[$key] = $value;';
					eval($php_stmt_att);
				}
			}
		}
		return $xml_array;
	}


	public function in_validate_npay(){
		   // $MerchantId = "55";
		   // $GTWREFNO = $this->input->get('GTWREFNO');
		   // $MerchantTxnId = $this->input->get('MERCHANTTXNID');

		   // $MerchantUserName="kgarira";
		   // $MerchantPassword="kagarira@live-16!!";
		   // $Signature="KGLIVE01";


		   // $salt1= $MerchantUserName.'kagarira@live-api';
		   //          $salt2=$Signature.$MerchantUserName.$MerchantTxnId;

		$MerchantId = "81";
		$GTWREFNO = $this->input->get('GTWREFNO');
		$MerchantTxnId = $this->input->get('MERCHANTTXNID');


		$MerchantUserName="kgarira_uat";
		$MerchantPassword="kgarira-api";
		$Signature="KAUAT01";


		$salt1= $MerchantUserName.$MerchantPassword;
		$salt2=$Signature.$MerchantUserName.$MerchantTxnId;        


		$merchant_pass = hash('sha256',$salt1);
		$signature_pass = hash('sha256',$salt2);


		$theData ='<?xml version="1.0" encoding="utf-8"?> '
		. '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"> '
		. '<soap:Body> '
		. '<CheckTransactionStatus xmlns="nPAY"> '
		. '<MerchantId>'.$MerchantId.'</MerchantId>'
		. ' <MerchantTxnId>'.$MerchantTxnId.'</MerchantTxnId>'
		. ' <MerchantUserName>'.$MerchantUserName.'</MerchantUserName>'
		. ' <MerchantPassword>'.$merchant_pass.'</MerchantPassword>'
		. ' <Signature>'.$signature_pass.'</Signature>'
		. ' <GTWREFNO>'.$GTWREFNO.'</GTWREFNO>'

		. ' </CheckTransactionStatus>'
		. ' </soap:Body>'
		. ' </soap:Envelope>';

		   // $url="https://gateway.npay.com.np/websrv/Service.asmx";


		$url="https://gateway.sandbox.npay.com.np/websrv/Service.asmx";

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $theData );

		$result = curl_exec($ch);
		curl_close($ch);

		$data = $this->XMLtoArray($result);

		return $data;

	}

	public function validate_npay() {

		$data = $this->in_validate_npay();

		$status= $data['SOAP:ENVELOPE']['SOAP:BODY']['CHECKTRANSACTIONSTATUSRESPONSE']['CHECKTRANSACTIONSTATUSRESULT']['STATUS_CODE'];
		$status_transaction = $data['SOAP:ENVELOPE']['SOAP:BODY']['CHECKTRANSACTIONSTATUSRESPONSE']['CHECKTRANSACTIONSTATUSRESULT']['TRANSACTION_STATUS'];
		// $merchantid_from_db = $this->_directory_check($this->input->get('MERCHANTTXNID'));

		if($status=="0" && $status_transaction == "SUCCESS")
		{
			echo '0';
		}
		else
		{
			echo "User's data not stored in database.";
		}
	}

  //end npay gateway

       //cancel  payment
	function cancel()
	{

		//$data['content'] = $this->content_page_model->getContentPages()->result_array();
		$data['header']='Payment Cancelled';
		//$data['module']='paypal';
		$data['view_page'] ="paypal/cancel";


		flashMsg("danger", "You cancelled your ticket");

		$this->load->view($this->_container,$data);
	}


  //updating ticket order when merchant transaction id is generated
	private function _update_ticket_order_table($transactionid, $gtwrefno)
	{
		
		foreach ($this->cart->contents() as $cart_values){
			$id = $cart_values['id'];
		}

		$this->db->set(array('merchant_txn_id'=> $transactionid, 'gateway_ref_no' => $gtwrefno,'payment_status' => (($transactionid!=null)?'complete':'pending'))); 
		$this->db->where('id', $id); 
		$this->db->update('tbl_ticket_orders'); 
	}

  //confirmation mail
  //@param resent ticket name
			//@return null
			//@function send mail to user and admin with ticket attached 
	public function confirmation_mail()
	{

		$this->load->library('email');
		$config = Array(        
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.vianet.com.np',


			'mailtype'  => 'html', 
					// 'charset'   => 'utf-8',
					// 'smtp_port' => 465,
			'charset'   => 'iso-8859-1'
			);


		$this->email->set_newline("\r\n");
		$this->email->initialize($config);
		$this->load->library('parser');	

        
        foreach ($this->cart->contents() as $key => $value)
         	$method = $value['payment_method'];
         
		if($method == 'ipay')
		{
			$post = $this->input->post();
		    $merchant_txn_id = $post['transactionid'];
		    $this->_update_ticket_order_table($merchant_txn_id ,null);

		}
		if($method == 'npay')
		{
			$get =$this->input->get();
			$merchant_txn_id = $get['MERCHANTTXNID'];
			$gtrefno = $get['GTWREFNO'];
		    $this->_update_ticket_order_table($merchant_txn_id ,$gtrefno);

			
		}
		if($method == 'cod')
		{
			$this->_update_ticket_order_table(null ,null);
		}
          
         $event_id  = $value['event_id'];
          $this->event_model->_table = "tbl_events";
		  $this->db->where('events.id', $event_id);
		  $data['event'] = $this->event_model->getEvents()->result_array();
		 
		 $venue_id = $data['event'][0]['venue_id'];
		 $this->db->where('id',$venue_id);
    	$data['venue'] = $this->venue_model->getVenues()->result_array(); 

		$total_without_pf = $value['qty'] * $value['item_price'];
		$processing_fees = $total_without_pf * $data['event'][0]['processing_fees']/100;
		$total = $total_without_pf + $processing_fees;
		$parse_data=array(

				'EVENT_ID' => $value['event_id'],
				'EVENT_NAME' =>$data['event'][0]['name'],
				'EVENT_STARTS' =>$data['event'][0]['start_date'],
				'EVENT_ENDS' =>$data['event'][0]['end_date'],
				'VENUE_NAME' =>$data['venue'][0]['venue_name'],
				'VENUE_LOCATION' =>$data['venue'][0]['venue_location'],
				'VENUE_CITY' => $data['venue'][0]['venue_city'],
				'ORDER_BY' => $value['name'],
				'ORDERER_EMAIL' => $value['email'],
				'QUANTITY' => $value['qty'],
				'ORDERER_ADDRESS' => $value['address'],
				'ORDERER_CONTACT' => $value['phone'],
				'PRICE' => $value['item_price'],
				'TWPF' => $total_without_pf,
				'PROCESSING_FEES' => $processing_fees,
				'TOTAL'=> $total,
				'ORDER_DATE' => date('Y-m-d H:i:s'),
			);
		$ticket = $this->_generateTicket();	
	 
		if($method == 'npay' || $method == 'ipay')
		{
			foreach ($this->cart->contents() as $key => $value) 

			$this->email->from('prajapati.niroj123@gmail.com');
							$this->email->to('prajapati.niroj123@gmail.com');
							$query = $this->db->get_where('email_templates', array('email_template_id' => 3))->result_array();

							foreach ($query as $q) {
								$subject = $q['subject'];
								$body = $q['body'];
							}
							$this->email->subject($subject);  
							$body = $this->parser->parse_string($body,$parse_data,TRUE);


							$this->email->message($body); 
							 
							$this->email->send();
							$this->email->clear();


							$name = $value['name'];
							$email= $value['email'];

							$this->email->from('prajapati.niroj123@gmail.com','Kgarira');
							$this->email->to($email);
							
							$query = $this->db->get_where('email_templates', array('email_template_id' => 4))->result_array();

							foreach ($query as $q) {
								$subject = $q['subject'];
								$body = $q['body'];
								
							}

							$this->email->subject($subject);  
							$body = $this->parser->parse_string($body,$parse_data,TRUE);

							$this->email->message($body); 
							$file_name = FCPATH.'tickets/'.$ticket;
							$this->email->attach($file_name);  
							$this->email->send();

		}
		else
		{
			foreach ($this->cart->contents() as $key => $value) 
							
							$this->email->from('prajapati.niroj123@gmail.com');
							$this->email->to('prajapati.niroj123@gmail.com');
							$query = $this->db->get_where('email_templates', array('email_template_id' => 3))->result_array();

							foreach ($query as $q) {
								$subject = $q['subject'];
								$body = $q['body'];
							}
							$this->email->subject($subject);  
							$body = $this->parser->parse_string($body,$parse_data,TRUE);


							$this->email->message($body); 
							$file_name = FCPATH.'tickets/'.$ticket;
							$this->email->attach($file_name);  
							$this->email->send();
							$this->email->clear(TRUE);


							$name = $value['name'];
							$email= $value['email'];
							$this->email->from('prajapati.niroj123@gmail.com','Kgarira');
							$this->email->to($email);
							
							$query = $this->db->get_where('email_templates', array('email_template_id' => 5))->result_array();

							foreach ($query as $q) {
								$subject = $q['subject'];
								$body = $q['body'];
							}
							$this->email->subject($subject);  
							$body = $this->parser->parse_string($body,$parse_data,TRUE);

							$this->email->message($body); 

							$this->email->send();

		}
	}

			private function _generateBarCodes($nos)
			{

				$barcodes=array();
				for($i=1;$i<=$nos;$i++)
				{

					$text=strtoupper(rand(10000,99999));
					$code = 'k'.$text.date('Ymd');
					$dest="assets/barcodes/".$code.".jpg";

					$barcode = new phpCode128($code, 100, 'c:\windows\fonts\verdana.ttf', 18);
					$barcode->setAutoAdjustFontSize(true);
					$barcode->setBorderWidth(1);
					$barcode->setShowText(true);
					$barcode->setPixelWidth(1);
					$barcode->setTextSpacing(5);
					$barcode->saveBarcode($dest);
					$barcodes[$i]=$code;

				}


				return $barcodes;
			}
			private function _generateTicket()
			{		
	
							$this->load->library('html2pdf');
							foreach ($this->cart->contents() as $key => $value) 
								
							$id = $value['event_id'];
				       		 $file_name=$id.date('_Y_m_d_h_i_s').'.pdf';

						    $barcodes = $this->_generateBarCodes($value['qty']);

						    $buyer = array(
						    		'orderer_name' => $value['name'],
						    		'order_date' => date('Y-m-d H:i:s'),
						    		'order_by' => ((!is_user())?$this->user_id:0),
						    		'order_by_email' => $value['email'],
						    		'quantity' => $value['qty'],
						    		'contact' => $value['phone'],
						    		'address' =>$value['address'],
						    	);

							$data_['event'] = $this->db->get_where('tbl_events', array('id' => $id))->result_array();

							$this->db->select("venue_id");
							$this->db->from("tbl_events");
							$this->db->where('id',$id);
							$query = $this->db->get();
							$venue_id_array = $query->result_array();
							$venue_id = $venue_id_array[0]['venue_id'];


							$this->db->select("venue_name");
							$this->db->from("tbl_venues");
							$this->db->where('id',$venue_id);
							$query = $this->db->get();
							$venue_name = $query->result_array();


							$data_['buyer']=$buyer;
							$data_['barcodes']=$barcodes;
							$data_['venue_name'] = $venue_name;
							$content=$this->load->view('pdfnew',$data_,TRUE);

							$this->html2pdf->WriteHTML($content);
							$path='tickets/';

							$this->html2pdf->Output($path.$file_name,'F');	
							

							foreach ($this->cart->contents() as $cart_values){
								$id = $cart_values['id'];
							}

							$this->db->set('ticket_name',$file_name); 
							$this->db->where('id', $id); 
							$this->db->update('tbl_ticket_orders'); 
							
							$ticket_order_id = $this->db->insert_id();
				        	$data_barcode = array();
				        	foreach ($barcodes as $unique_barcode) { //inserting a ticket code in tiket code table with unique  barcode
				        		$data_barcode[] = array('event_id'=>$value['event_id'], 'ticket_order_id'=>$ticket_order_id , 'code'=> $unique_barcode, 'status'=> 'valid');
				        	}
				        	$this->db->insert_batch('tbl_ticket_codes', $data_barcode); 


					return $file_name;
			}

}
