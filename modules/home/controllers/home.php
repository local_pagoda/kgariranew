<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Public_Controller
{
 function __construct()
 {
  parent::__construct();
                    // $this->load->module_model('event','event_type_model');
  $this->load->module_model('event','event_model');
  $this->load->module_model('blog','blog_model');
  $this->load->module_model('banner','banner_model');
  $this->load->module_model('ticket','ticket_model');
  $this->load->module_model('ticket_order','ticket_order_model');
  $this->lang->module_load('ticket_order','ticket_order');

  $this->load->module_model('venue','venue_model');
  $this->load->module_model('gallery','gallery_model');
  $this->load->module_model('auth','user_model');
                    // $this->load->module_model('country','country_model');
  $this->load->module_model('ticket','ticket_model');
  $this->lang->module_load('event','event');
  $this->load->helper('date');
  $this->load->library('cart');
  $this->load->helper('string');
  $this->load->library('email');
  $this->load->library('validation');
$this->load->library('session');

}

function index()
{
  $this->event_model->_table = "tbl_events";
  $this->db->where('events.status', 1);
  $this->db->order_by('start_date asc');
  $this->event_model->joins=array('VENUES');
  $data['events'] = $this->event_model->getEvents(NULL,NULL,array('limit'=>4,'offset'=>''))->result_array();

  $this->event_model->_table = "tbl_events";
  $this->db->where('events.status', 1);
  $this->db->order_by('id desc');
  $this->event_model->joins=array('VENUES');
  $data['upcoming_events'] = $this->event_model->getEvents(NULL,NULL,array('limit'=>5,'offset'=>''))->result_array();	

  $this->db->where(array('blogs.status' => 1,'type_id' =>26 ));
  $this->db->order_by('created_date desc');
  $data['blogs'] = $this->blog_model->getBlogs(NULL,NULL,array('limit'=>6,'offset'=>''))->result_array(); 

  $this->db->where(array('banners.status' => 1));
  $this->db->order_by('created_date desc');
  $data['banners'] = $this->banner_model->getBanners()->result_array(); 


  $this->event_model->_table = "viewtbl_gallery";
  $this->db->order_by('gallery_id desc');
  $this->db->group_by('event_id');
  $data['galleries'] = $this->event_model->getEvents()->result_array();
                    // print_r($data['galleries']);exit;
                    // $data['galleries'] = $this->db->get('viewtbl_gallery')->result_array();


  $data['header'] = "Home";
  $data['view_page'] = 'home/index';
  $this->load->view($this->_container,$data);
}


public function gallery()
{

  $this->event_model->_table = "viewtbl_gallery";
  $this->db->order_by('gallery_id desc');
  $this->db->group_by('event_id');
  $data['galleries'] = $this->event_model->getEvents()->result_array();

  $data['header'] = "Gallery";
  $data['view_page'] = 'gallery/index';
  $this->load->view($this->_container,$data);
}

public function find_image($id)
{


 $this->event_model->_table = "tbl_gallery_image";
 $this->db->select('image_name');
 $this->db->where('gallery_id',$id);
 $data['images'] = $this->event_model->getEvents()->result_array();


 $data['header'] = "Home";
 $data['view_page'] = 'gallery/images';
 $this->load->view($this->_container,$data);
}

public function venue()
{
  $this->db->where(array('venues.status' => 1 ));
  $this->db->order_by('id desc');
  $data['venues'] = $this->venue_model->getVenues()->result_array();

  $data['header'] = "Venue";
  $data['view_page'] = 'venue/index';
  $this->load->view($this->_container,$data);


}

public function venue_details($id)
{

 $this->event_model->_table = "view_venue";
               // $this->db->select('image_name');
 $this->db->where('id',$id);
 $this->db->order_by('id desc');
 $data['venues'] = $this->event_model->getEvents()->result_array();

 $data['header'] = "Venue";
 $data['view_page'] = 'venue/detail';
 $this->load->view($this->_container,$data);


}

public function contact() 
{

 if($this->input->post())
 {
  $this->load->library('email');
  $config = Array(        
    'protocol' => 'smtp',
    'smtp_host' => 'smtp.vianet.com.np',


    'mailtype'  => 'html', 
                    // 'charset'   => 'utf-8',
                    // 'smtp_port' => 465,
    'charset'   => 'iso-8859-1'
    );

  $this->email->set_newline("\r\n");
  $this->email->initialize($config);

  $this->email->from($this->input->post('email'), $this->input->post('first_name').' '.$this->input->post('last_name'));
  $this->email->to('prajapati.niroj123@gmail.com');

  $this->email->subject($this->input->post('subject'));
  $this->email->message($this->input->post('message'));

  $this->email->send();


  flashMsg('success','Successfully your querry  has been sent to admin');
  redirect();

}
else
{
  $data['view_page'] = 'home/contact';

  $data['header'] = "Home";
  $this->load->view($this->_container,$data);
}
}

public function upcoming_event()
{
  $id = $this->input->post('id');
  $this->event_model->_table = "tbl_events";
  $this->db->where('events.id', $id);
  $event_data = $this->event_model->getEvents()->result_array();

  /**/     $this->db->where('tickets.event_id',$id);
  $this->db->select('ticket_price');
           // $this->db->order_by('created_date desc');
  $ticket['ticket_data'] = $this->ticket_model->getTickets()->result_array(); 

  $data =  array_merge($event_data,$ticket);
  echo json_encode($data);
} 
    public function insert_items_into_cart($id)
    {
      
      $data=$this->_user_data_from_post(); //Retrive Posted Data  
   
       $cart_data= array(
        'id'      => $id,
        'event_id' => $data['event_id'],
        'qty'     => $data['quantity'],
        'price'   => $data['total_price'],
        'name'    => $data['orderer_name'],
        'item_price' => $data['item_price'],
        'email' => $data['orderer_email'],
        'buyer_id' => $data['buyer_id'],
        'address' => $data['order_address'],
        'phone'=>$data['order_phone'],
        'country'=>$data['orderer_country'],
        'city'=>$data['orderer_city'],
        'zip'=>$data['orderer_zip'],
        'payment_method' =>$data['payment_method'],
        'payment_status'=>$data['payment_status'],


        );

       $this->cart->insert($cart_data);
    }

    public function store_user_data()
    {
        
            $data=$this->_user_data_from_post(); //Retrive Posted Data  
            $this->cart->destroy();

            if(!$this->input->post('id'))
            {
              $data['created_date'] = date("Y-m-d H:i:s");
               if(is_user())
                {
                  $data['created_by'] = $this->user_id;
                }
              $success=$this->ticket_order_model->insert('TICKET_ORDERS',$data);
              $data['order_id'] = $this->db->insert_id(); 

            }
            else
            {
             $data['updated_date'] = date("Y-m-d H:i:s");
            
             $success=$this->ticket_order_model->update('TICKET_ORDERS',$data,array('id'=>$data['id']));

           }
            $id= $data['order_id'];
            $this->insert_items_into_cart($id);
         
         if($success)
         {
          if($data['payment_method']=='ipay')
          {
             
              redirect('pm_gate/ipay');
          }
          else if($data['payment_method'] == 'npay')
          {
              redirect('pm_gate/npay');

          }
          else
          {
              redirect('pm_gate/cod');

          }
         }

    }  

        private function _user_data_from_post()
        {
         $data=array();

         if($this->input->post('id'))
         {
          $data['id'] = $this->input->post('id');
        }        

        $data['event_id'] = $this->input->post('event_id');
       

        $fname = $this->input->post('buyer_first_name');
        $lname = $this->input->post('buyer_last_name');
        $data['orderer_name'] = $fname.' '.$lname;
        $data['orderer_email'] = $this->input->post('buyer_email_address');

        if(is_user())
        {
          $data['buyer_id'] = $this->user_id;
        }
        else
        {
          $data['buyer_id'] = 0;
        }

        $data['quantity'] = $this->input->post('ordered_quantity');
        $data['item_price'] = $this->input->post('ordered_ticket_price');
        $data['order_phone'] = $this->input->post('buyer_phone_number');
        $data['order_address'] = $this->input->post('buyer_address');
        $data['orderer_country'] = $this->input->post('buyer_country');
              // $data['orderer_company'] = $this->input->post('orderer_company');
        $data['orderer_city'] = $this->input->post('buyer_city');
        $data['orderer_zip'] = $this->input->post('buyer_country_zip');
        $processing = $this->input->post('processing');

        $data['payment_status'] = "pending";
        $total = ($data['quantity'] * $data['item_price'] * $processing)/100 + ($data['item_price'] * $data['quantity']);
        $data['total_price'] = $total;
        $data['payment_method'] = $this->input->post('payment_method');

        return $data;
      }

/* End of file welcome.php 
/* Location: ./modules/welcome/controllers/welcome.php */

}
