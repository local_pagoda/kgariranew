 <?php

class Event extends Admin_Controller
{
	protected $uploadPath = 'uploads/event';
protected $uploadthumbpath= 'uploads/event/thumb/';

	public function __construct(){
    	parent::__construct();
        $this->load->module_model('event','event_model');
        $this->lang->module_load('event','event');
        $this->load->module_model('venue','venue_model');
 		$this->load->module_model('type','type_model');
 		$this->load->module_model('guest','guest_model');

 		//for ticket
 		
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$where = '(id = 154 OR id = 13 OR id = 230)';
		$data['venues'] = $this->db->get('tbl_venues')->result_array();
		$data['event_types'] = $this->db->get_where('mst_types', array('parent_id' => 1))->result_array();
		$data['guests'] = $this->db->get('tbl_guests')->result_array();
		$data['countries'] = $this->db->get('mst_countries')->result_array();
		$data['codes'] = $this->db->get_where('mst_countries', $where)->result_array();


		$data['promoters'] = $this->db->get('be_users')->result_array();
		$data['events'] = $this->db->get('tbl_events')->result_array();



		// Display Page
		$data['header'] = 'event';
		$data['page'] = $this->config->item('template_admin') . "event/index";
		$data['module'] = 'event';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$this->event_model->_table = "tbl_events";
		$where = array();
		$params = $this->input->post(); 

		$total=$this->event_model->count();



		/*Count for filters*/
		$this->_get_search_param($params);	
		$filter_total=$this->event_model->count();
 	
		/*Filters*/
		$this->_get_search_param($params);	
		
		$rows=$this->event_model->getEvents(NULL,NULL,array('limit'=>$params['length'],'offset'=>$params['start']))->result_array();
		//echo $this->db->last_query();


		echo json_encode(array('draw'=>$params['draw'],'recordsTotal'=>$total,'recordsFiltered'=>$filter_total,'data'=>$rows));
	}
	
	public function _get_search_param($params)
	{

		foreach ($params['columns'] as $value) {
			if($value['searchable'] == 'true'){
				if($params['search']['value'] != '')
				{
					$this->db->or_where(array('artists.'.$value["name"].' like'=>'%'.$params['search']['value'].'%'));
				}

				if($value['search']['value'] != '')
				{
					$temp = explode(',', $value['search']['value']);
					$this->db->where_in('artists.'.$value['name'],$temp);
				}
			}
		}
	


		// Search Param Goes Here
		/*
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['name']!='')?$this->db->like('name',$params['search']['name']):'';
($params['search']['venue_id']!='')?$this->db->where('venue_id',$params['search']['venue_id']):'';
($params['search']['type_id']!='')?$this->db->where('type_id',$params['search']['type_id']):'';
($params['search']['promoters_id']!='')?$this->db->where('promoters_id',$params['search']['promoters_id']):'';
($params['search']['country_id']!='')?$this->db->where('country_id',$params['search']['country_id']):'';
($params['search']['fb_event_id']!='')?$this->db->where('fb_event_id',$params['search']['fb_event_id']):'';
(isset($params['search']['is_fb_event']))?$this->db->where('is_fb_event',$params['search']['is_fb_event']):'';
(isset($params['search']['is_guest_event']))?$this->db->where('is_guest_event',$params['search']['is_guest_event']):'';
($params['search']['is_spam']!='')?$this->db->where('is_spam',$params['search']['is_spam']):'';
(isset($params['search']['is_featured']))?$this->db->where('is_featured',$params['search']['is_featured']):'';
($params['search']['guest_id']!='')?$this->db->where('guest_id',$params['search']['guest_id']):'';
(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';
($params['search']['created_by']!='')?$this->db->where('created_by',$params['search']['created_by']):'';
($params['search']['updated_by']!='')?$this->db->where('updated_by',$params['search']['updated_by']):'';

		}  

		
		if(!empty($params['date']))
				{
					foreach($params['date'] as $key=>$value){
						$this->_datewise($key,$value['from'],$value['to']);	
					}
				}
		*/
		               
        
			}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->event_model->getEvents()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->event_model->delete('EVENTS',array('id'=>$row));
            endforeach;
		}
	}  

	
	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $data['created_date'] = date("Y-m-d H:i:s");
        	$data['created_by'] = $this->user_id;
            $success=$this->event_model->insert('EVENTS',$data);
        }
        else
        {
        	$data['updated_date'] = date('Y-m-d H:i:s');
    		$data['updated_by'] = $this->user_id;

            $success=$this->event_model->update('EVENTS',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id'))
    	{
    		$data['id'] = $this->input->post('id');
    	}        

      
$data['name'] = $this->input->post('name');
$data['venue_id'] = $this->input->post('venue_id');
$data['type_id'] = $this->input->post('type_id');
$data['promoters_id'] = $this->input->post('promoters_id');
$data['country_id'] = $this->input->post('country_id');
$data['start_date'] = $this->input->post('start_date');
$data['end_date'] = $this->input->post('end_date');
$data['description'] = $this->input->post('description');
$data['processing_fees'] = $this->input->post('processing_fees');

$data['image'] = $this->input->post('image');
$data['fb_event_id'] = $this->input->post('fb_event_id');
$data['is_fb_event'] = $this->input->post('is_fb_event');
$data['is_guest_event'] = $this->input->post('is_guest_event');
$data['is_spam'] = $this->input->post('is_spam');
$data['is_featured'] = $this->input->post('is_featured');
$data['guest_id'] = $this->input->post('guest_id');
$data['status'] = $this->input->post('status');

        return $data;
   }
   
      function upload_image(){
		//Image Upload Config
		$config['upload_path'] = $this->uploadPath;
		$config['allowed_types'] = 'gif|png|jpg';
		$config['max_size']	= '10240';
		$config['remove_spaces']  = true;
		//load upload library
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload())
		{
			$data['error'] = $this->upload->display_errors('','');
			echo json_encode($data);
		}
		else
		{
		  $data = $this->upload->data();
 		  $config['image_library'] = 'gd2';
		  $config['source_image'] = $data['full_path'];
          $config['new_image']    = $this->uploadthumbpath;
		  //$config['create_thumb'] = TRUE;
		  $config['maintain_ratio'] = TRUE;
		  $config['height'] =100;
		  $config['width'] = 100;

		  $this->load->library('image_lib', $config);
		  $this->image_lib->resize();
		  echo json_encode($data);
	    }
	}
	
	function upload_delete(){
		//get filename
		$filename = $this->input->post('filename');
		@unlink($this->uploadPath . '/' . $filename);
	} 	
	    
}