<!-- JQuery DataTable Css -->
<link href="<?php  echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?php  echo lang('ticket')?></h2>
		</div>

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('ticket')?>
						</h2>
					</div>

					<div class="body">
					</div>
				</div>
			</div>
		</div> <!-- close row clearfix -->

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('ticket')?>
						</h2>
						<div id="toolbar" style="padding:5px;height:auto">
							<p>
								<a href="#" class="btn bg-green waves-effect" onclick="create()" title="<?php   echo lang('create_ticket')?>"><?php  echo lang('create')?></a>
								<a href="#" class="btn bg-blue-grey waves-effect" onclick="removeSelected()"  title="<?php   echo lang('delete_ticket')?>"><?php  echo lang('remove_selected')?></a>
							</p>

						</div>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<table id="ticket-table" class="table table-bordered table-striped table-hover dataTable " >
							<thead>
								<th ><?php echo lang('id')?></th>
<th ><?php echo lang('event_id')?></th>
<th ><?php echo lang('image')?></th>
<th ><?php echo lang('ticket_count')?></th>
<th ><?php echo lang('ticket_price')?></th>
<th ><?php echo lang('currency')?></th>
<th><?php echo lang('status')?></th>
<th ><?php echo lang('created_date')?></th>
<th ><?php echo lang('updated_date')?></th>
<th ><?php echo lang('created_by')?></th>
<th ><?php echo lang('updated_by')?></th>

								<th>Action</th>

							</thead>

							<tbody>
							</tbody>

						</table>
					</div><!-- close body -->
				</div><!-- close card -->
			</div><!-- close col -->
		</div><!-- close rowclearfix -->

		<!--for create and edit ticket form-->
		<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none">

			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Add or Edit Members</h4>
					</div>
					<div class="modal-body">
						<form id="form-ticket" method="post" class="form-horizontal" >

							<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="event_id"><?php echo lang("event_id")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="event_id" id="event_id" class="form-control number" placeholder="<?php echo lang('event_id'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="image"><?php echo lang("image")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="image" id="image" class="form-control" placeholder="<?php echo lang('image'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="ticket_count"><?php echo lang("ticket_count")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="ticket_count" id="ticket_count" class="form-control" placeholder="<?php echo lang('ticket_count'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="ticket_price"><?php echo lang("ticket_price")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="ticket_price" id="ticket_price" class="form-control" placeholder="<?php echo lang('ticket_price'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="currency"><?php echo lang("currency")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="currency" id="currency" class="form-control" placeholder="<?php echo lang('currency'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="status"><?php echo lang("status")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="radio" value="1" name="status" id="status1" /><label for="status1"><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="status" id="status0" /><label for="status0"><?php echo lang("general_no")?></label></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="created_date"><?php echo lang("created_date")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="created_date" id="created_date" class="form-control datetime" placeholder="<?php echo lang('created_date'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="updated_date"><?php echo lang("updated_date")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="updated_date" id="updated_date" class="form-control datetime" placeholder="<?php echo lang('updated_date'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="created_by"><?php echo lang("created_by")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="created_by" id="created_by" class="form-control" placeholder="<?php echo lang('created_by'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="updated_by"><?php echo lang("updated_by")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="updated_by" id="updated_by" class="form-control" placeholder="<?php echo lang('updated_by'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<input type="hidden" name="id" id="id"/>

						</form>
					</div> <!-- end body -->
					<div class="modal-footer">
						<button type="button" class="btn bg-green waves-effect" onClick="save()"><?php  echo  lang('general_save')?></button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
					</div>

				</div><!-- end content -->
			</div><!-- end modal-dialog -->



		</div><!-- end edit modal -->

	</div><!-- end container fluid -->
</section> <!-- end content -->


<!-- Input Mask Plugin Js -->
<script src="<?php  echo base_url(); ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script language="javascript" type="text/javascript">

	var dataTable; 
	$(function(){

		$('.date').inputmask('yyyy-mm-dd', { placeholder: '____-__-__' });
		$('.email').inputmask({ alias: "email" });

		dataTable = $('#ticket-table').DataTable({
			dom: 'Bfrtip',
			scrollX: true,
			"serverSide": true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "<?php  echo site_url('ticket/admin/ticket/json'); ?>",type: 'POST' },
			columns: [
			{ data: "id",name: "id"},
				{ data: "event_id",name: "event_id"},
				{ data: "image",name: "image"},
				{ data: "ticket_count",name: "ticket_count"},
				{ data: "ticket_price",name: "ticket_price"},
				{ data: "currency",name: "currency"},
				{ data: "status",name: "status"},
				{ data: "created_date",name: "created_date"},
				{ data: "updated_date",name: "updated_date"},
				{ data: "created_by",name: "created_by"},
				{ data: "updated_by",name: "updated_by"},
				
			{ data: function(data,b,c,table) { 
				var buttons = '';

				buttons += "<button type='button' class='btn bg-grey waves-effect' data-toggle='modal' data-target='#edit-modal' onclick='edit("+table.row+")'>Edit</button>&nbsp";
				buttons += "<button class='btn bg-red waves-effect' onclick='removeticket("+data.id+")'>Delete</button>";

				return buttons;
			}, name:'action',searchable: false},
			]
		});

	});

	function create(){
		$('#form-ticket')[0].reset();
		$('#id').val('');
		$('#edit-modal').modal('show');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = dataTable.row(index).data();

		$('#id').val(row.id);
		$("#form-ticket").find('input:checkbox').prop('checked',false);
		$("#form-ticket").find('input:text,select').val(function(i,v){

			/*if(row.gender == 'M')
			{
				$('input:radio[name=gender][id=radio_1]').prop('checked',true);
			}else{
				$('input:radio[name=gender][id=radio_2]').prop('checked',true);
			}*/
			return row[this.name];
		});
		$('select').selectpicker('render');

		$("#form-ticket").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	
	}

	function removeticket(index)
	{
		$.post("<?php   echo site_url('ticket/admin/ticket/delete_json')?>", {id:[index]}, function(){
			dataTable.ajax.reload( null, false );
		});
	}

	function save()
	{
		var validator = $('#form-ticket').validate();
		if(validator.form())
		{
			$.ajax({
				url: '<?php   echo site_url('ticket/admin/ticket/save')?>',
				data: $('#form-ticket').serialize(),
				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#edit-modal').modal('hide');
						$('#form-ticket')[0].reset();
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
	}
</script>
<script type="text/javascript">
	/* Rules for the form*/
	$(function () {
		$('#form-ticket').validate({
			rules: {
				// 'name': {
				// 	required: true
				// }
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	});
</script>