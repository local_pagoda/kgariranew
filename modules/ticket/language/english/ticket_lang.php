<?php


$lang['id'] = 'Id';
$lang['event_id'] = 'Event Id';
$lang['image'] = 'Image';
$lang['ticket_count'] = 'Ticket Count';
$lang['ticket_price'] = 'Ticket Price';
$lang['currency'] = 'Currency';
$lang['status'] = 'Status';
$lang['created_date'] = 'Created Date';
$lang['updated_date'] = 'Updated Date';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';

$lang['create_ticket']='Create Ticket';
$lang['edit_ticket']='Edit Ticket';
$lang['delete_ticket']='Delete Ticket';
$lang['ticket_search']='Ticket Search';

$lang['ticket']='Ticket';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

