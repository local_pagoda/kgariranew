<?php

class Photographer extends Admin_Controller
{
	protected $uploadPath = 'uploads/photographer';
protected $uploadthumbpath= 'uploads/photographer/thumb/';

	public function __construct(){
    	parent::__construct();
        $this->load->module_model('photographer','photographer_model');
        $this->lang->module_load('photographer','photographer');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'photographer';
		$data['page'] = $this->config->item('template_admin') . "photographer/index";
		$data['module'] = 'photographer';
		$this->load->view($this->_container,$data);		
	}
 
	public function json()
	{
		$where = array();
		$params = $this->input->post(); 

		$total=$this->photographer_model->count();

		/*Count for filters*/
		$this->_get_search_param($params);	
		$filter_total=$this->photographer_model->count();

		/*Filters*/
		$this->_get_search_param($params);	
		
		$rows=$this->photographer_model->getPhotographers(NULL,NULL,array('limit'=>$params['length'],'offset'=>$params['start']))->result_array();
		echo json_encode(array('draw'=>$params['draw'],'recordsTotal'=>$total,'recordsFiltered'=>$filter_total,'data'=>$rows));
	}
	
	public function _get_search_param($params)
	{

		foreach ($params['columns'] as $value) {
			if($value['searchable'] == 'true'){
				if($params['search']['value'] != '')
				{
					$this->db->or_where(array('photographers.'.$value["name"].' like'=>'%'.$params['search']['value'].'%'));
				}

				if($value['search']['value'] != '')
				{
					$temp = explode(',', $value['search']['value']);
					$this->db->where_in('photographers.'.$value['name'],$temp);
				}
			}
		}


		// Search Param Goes Here
		/*
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['user_id']!='')?$this->db->where('user_id',$params['search']['user_id']):'';
($params['search']['firstname']!='')?$this->db->like('firstname',$params['search']['firstname']):'';
($params['search']['lastname']!='')?$this->db->like('lastname',$params['search']['lastname']):'';
($params['search']['qualification']!='')?$this->db->like('qualification',$params['search']['qualification']):'';
($params['search']['country']!='')?$this->db->like('country',$params['search']['country']):'';
($params['search']['phone']!='')?$this->db->like('phone',$params['search']['phone']):'';
($params['search']['citizenship_no']!='')?$this->db->like('citizenship_no',$params['search']['citizenship_no']):'';
($params['search']['passport_no']!='')?$this->db->like('passport_no',$params['search']['passport_no']):'';
($params['search']['status']!='')?$this->db->where('status',$params['search']['status']):'';
($params['search']['created_by']!='')?$this->db->where('created_by',$params['search']['created_by']):'';
($params['search']['updated_by']!='')?$this->db->where('updated_by',$params['search']['updated_by']):'';

		}  

		
		if(!empty($params['date']))
				{
					foreach($params['date'] as $key=>$value){
						$this->_datewise($key,$value['from'],$value['to']);	
					}
				}
		*/
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->photographer_model->getPhotographers()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->photographer_model->delete('PHOTOGRAPHERS',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
           	
           	 $data['created_date'] = date("Y-m-d H:i:s");
            $data['created_by'] = $this->user_id;
            $success=$this->photographer_model->insert('PHOTOGRAPHERS',$data);
        }
        else
        {
             $data['updated_date'] = date("Y-m-d H:i:s");
            $data['updated_by'] =  $this->user_id;
            $success=$this->photographer_model->update('PHOTOGRAPHERS',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
   		if($this->input->post('id')){
   			 $data['id'] = $this->input->post('id');
   		}
		
		$data['user_id'] = $this->user_id;
		$data['firstname'] = $this->input->post('firstname');
		$data['lastname'] = $this->input->post('lastname');
		$data['profile_image'] = $this->input->post('profile_image');
		$data['last_visit'] = $this->input->post('last_visit');
		$data['qualification'] = $this->input->post('qualification');
		$data['country'] = $this->input->post('country');
		$data['phone'] = $this->input->post('phone');
		$data['citizenship_no'] = $this->input->post('citizenship_no');
		$data['passport_no'] = $this->input->post('passport_no');
		$data['status'] = $this->input->post('status');
		// $data['created_date'] = $this->input->post('created_date');
		// $data['updated_date'] = $this->input->post('updated_date');
		// $data['created_by'] = $this->input->post('created_by');
		// $data['updated_by'] = $this->input->post('updated_by');
        return $data;

   }
   
      function upload_image(){
		//Image Upload Config
		$config['upload_path'] = $this->uploadPath;
		$config['allowed_types'] = 'gif|png|jpg';
		$config['max_size']	= '10240';
		$config['remove_spaces']  = true;
		//load upload library
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload())
		{
			$data['error'] = $this->upload->display_errors('','');
			echo json_encode($data);
		}
		else
		{
		  $data = $this->upload->data();
 		  $config['image_library'] = 'gd2';
		  $config['source_image'] = $data['full_path'];
          $config['new_image']    = $this->uploadthumbpath;
		  //$config['create_thumb'] = TRUE;
		  $config['maintain_ratio'] = TRUE;
		  $config['height'] =100;
		  $config['width'] = 100;

		  $this->load->library('image_lib', $config);
		  $this->image_lib->resize();
		  echo json_encode($data);
	    }
	}
	
	function upload_delete(){
		//get filename
		$filename = $this->input->post('filename');
		@unlink($this->uploadPath . '/' . $filename);
	} 	
	    
}