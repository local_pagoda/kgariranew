<?php


$lang['id'] = 'Id';
$lang['user_id'] = 'User Id';
$lang['firstname'] = 'Firstname';
$lang['lastname'] = 'Lastname';
$lang['profile_image'] = 'Profile Image';
$lang['last_visit'] = 'Last Visit';
$lang['qualification'] = 'Qualification';
$lang['country'] = 'Country';
$lang['phone'] = 'Phone';
$lang['citizenship_no'] = 'Citizenship No';
$lang['passport_no'] = 'Passport No';
$lang['status'] = 'Status';
$lang['created_date'] = 'Created Date';
$lang['updated_date'] = 'Updated Date';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';

$lang['create_photographer']='Create Photographer';
$lang['edit_photographer']='Edit Photographer';
$lang['delete_photographer']='Delete Photographer';
$lang['photographer_search']='Photographer Search';

$lang['photographer']='Photographer';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

