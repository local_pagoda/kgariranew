<!-- JQuery DataTable Css -->
<link href="<?php  echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?php  echo lang('photographer')?></h2>
		</div>

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('photographer')?>
						</h2>
					</div>

					<div class="body">
					</div>
				</div>
			</div>
		</div> <!-- close row clearfix -->

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('photographer')?>
						</h2>
						<div id="toolbar" style="padding:5px;height:auto">
							<p>
								<a href="#" class="btn bg-green waves-effect" onclick="create()" title="<?php   echo lang('create_photographer')?>"><?php  echo lang('create')?></a>
								<a href="#" class="btn bg-blue-grey waves-effect" onclick="removeSelected()"  title="<?php   echo lang('delete_photographer')?>"><?php  echo lang('remove_selected')?></a>
							</p>

						</div>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<table id="photographer-table" class="table table-bordered table-striped table-hover dataTable " >
							<thead>
								

<th ><?php echo lang('firstname')?></th>
<th ><?php echo lang('lastname')?></th>
<th ><?php echo lang('profile_image')?></th>
<th ><?php echo lang('last_visit')?></th>
<th ><?php echo lang('qualification')?></th>
<th ><?php echo lang('country')?></th>
<th ><?php echo lang('phone')?></th>
<th ><?php echo lang('citizenship_no')?></th>
<th ><?php echo lang('passport_no')?></th>
<th><?php echo lang('status')?></th>


								<th>Action</th>

							</thead>

							<tbody>
							</tbody>

						</table>
					</div><!-- close body -->
				</div><!-- close card -->
			</div><!-- close col -->
		</div><!-- close rowclearfix -->

		<!--for create and edit photographer form-->
		<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none">

			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Add or Edit Members</h4>
					</div>
					<div class="modal-body">
						<form id="form-photographer" method="post" class="form-horizontal" enctype="multipart/form-data">

						
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="firstname"><?php echo lang("firstname")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="firstname" id="firstname" class="form-control" placeholder="<?php echo lang('firstname'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="lastname"><?php echo lang("lastname")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="lastname" id="lastname" class="form-control" placeholder="<?php echo lang('lastname'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				
				<div class="row clearfix">
									<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
										<label for="image"><?php echo lang("profile_image")?></label>
									</div>
									<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">

												<label id="upload_image_name" ></label>
												<input type="file" name="image" id="profile_image" class="form-control" placeholder="<?php echo lang('profile_image'); ?>">
												<input type="text" name="profile_image" id="upload_image" hidden>

												<button type="button" class="btn btn-default waves-effect" id="change_image" title="Change Image" hidden> <i class="material-icons">switch_camera</i>Change Image </button>

											</div>
										</div>
									</div>
								</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="last_visit"><?php echo lang("last_visit")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="last_visit" id="last_visit" class="form-control date" placeholder="<?php echo lang('last_visit'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="qualification"><?php echo lang("qualification")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="qualification" id="qualification" class="form-control" placeholder="<?php echo lang('qualification'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="country"><?php echo lang("country")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="country" id="country" class="form-control" placeholder="<?php echo lang('country'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="phone"><?php echo lang("phone")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="phone" id="phone" class="form-control" placeholder="<?php echo lang('phone'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="citizenship_no"><?php echo lang("citizenship_no")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="citizenship_no" id="citizenship_no" class="form-control" placeholder="<?php echo lang('citizenship_no'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="passport_no"><?php echo lang("passport_no")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="passport_no" id="passport_no" class="form-control" placeholder="<?php echo lang('passport_no'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="status"><?php echo lang("status")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="radio" value="1" name="status" id="status1" checked/><label for="status1"><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="status" id="status0" /><label for="status0"><?php echo lang("general_no")?></label></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				
				
				<input type="hidden" name="id" id="id"/>

						</form>
					</div> <!-- end body -->
					<div class="modal-footer">
						<button type="button" class="btn bg-green waves-effect" onClick="save()"><?php  echo  lang('general_save')?></button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
					</div>

				</div><!-- end content -->
			</div><!-- end modal-dialog -->



		</div><!-- end edit modal -->

	</div><!-- end container fluid -->
</section> <!-- end content -->


<!-- Input Mask Plugin Js -->
<script src="<?php  echo base_url(); ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script language="javascript" type="text/javascript">
    var base_url = '<?php echo base_url(); ?>';
	var dataTable; 
	$(function(){

		$('.date').inputmask('yyyy-mm-dd', { placeholder: '____-__-__' });
		$('.email').inputmask({ alias: "email" });

		dataTable = $('#photographer-table').DataTable({
			dom: 'Bfrtip',
			scrollX: true,
			"serverSide": true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "<?php  echo site_url('photographer/admin/photographer/json'); ?>",type: 'POST' },
			columns: [
			// { data: "id",name: "id"},
				// { data: "user_id",name: "user_id"},
				{ data: "firstname",name: "firstname"},
				{ data: "lastname",name: "lastname"},
				{ data: "profile_image",name: "profile_image"},
				{ data: "last_visit",name: "last_visit"},
				{ data: "qualification",name: "qualification"},
				{ data: "country",name: "country"},
				{ data: "phone",name: "phone"},
				{ data: "citizenship_no",name: "citizenship_no"},
				{ data: "passport_no",name: "passport_no"},
				{ data: "status",name: "status"},
				// { data: "created_date",name: "created_date"},
				// { data: "updated_date",name: "updated_date"},
				// { data: "created_by",name: "created_by"},
				// { data: "updated_by",name: "updated_by"},
				
			{ data: function(data,b,c,table) { 
				var buttons = '';

				buttons += "<button type='button' class='btn bg-grey waves-effect' data-toggle='modal' data-target='#edit-modal' onclick='edit("+table.row+")'>Edit</button>&nbsp";
				buttons += "<button class='btn bg-red waves-effect' onclick='removephotographer("+data.id+")'>Delete</button>";

				return buttons;
			}, name:'action',searchable: false},
			]
		});
			/*Change Image*/
			$("#change_image").click(function(){
				var filename = $('#upload_image').val();
				$.post("<?php echo site_url('photographer/admin/photographer/upload_delete')?>", {filename:filename}, function(){
					$("#change_image").hide();
					$('#profile_image').show();
					$('#upload_image').val('');
					$('#upload_image_name').text('');
					$.notify({
						title: "<strong><?php  echo lang('success')?>:</strong> ",
						message: "Removed previous image !"
					});
				});
			});


	});

	function create(){
		$('#form-photographer')[0].reset();
		$('#id').val('');
		$('#edit-modal').modal('show');
		uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = dataTable.row(index).data();

		$('#id').val(row.id);
		$("#form-photographer").find('input:checkbox').prop('checked',false);
		$("#form-photographer").find('input:text,select').val(function(i,v){

			if(row.status == '1')
			{
				$('input:radio[name=status1][id=radio_1]').prop('checked',true);
			}else{
				$('input:radio[name=status0][id=radio_2]').prop('checked',true);
			}
			return row[this.name];
		});
		$('select').selectpicker('render');

		$("#form-photographer").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	

		if(Boolean(row.image))
		{
			$('#upload_image').val(row.image);
			$('#profile_image').hide();
			$('#upload_image_name').html("<img src='"+base_url+"/uploads/photographer/thumb/"+row.image+"' max-width='400px' height='70px'>");
			$('#change_image').show();
		}
		uploadReady();
	}

	function removephotographer(index)
	{
		$.post("<?php   echo site_url('photographer/admin/photographer/delete_json')?>", {id:[index]}, function(){
			dataTable.ajax.reload( null, false );
		});
	}

	function save()
	{
		var validator = $('#form-photographer').validate();
		if(validator.form())
		{
			$.ajax({
				url: '<?php   echo site_url('photographer/admin/photographer/save')?>',
				data: $('#form-photographer').serialize(),
				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#edit-modal').modal('hide');
						$('#form-photographer')[0].reset();
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
	}


	function uploadReady()
	{
		uploader=$('#profile_image');
		new AjaxUpload(uploader, {
			action: '<?php  echo site_url('photographer/admin/photographer/upload_image')?>',
			name: 'userfile',
			responseType: "json",
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
                    $.notify({
                    	title: "<strong><?php  echo lang('error')?>:</strong> ",
                    	message: "Only JPG, PNG or GIF files are allowed"
                    });
                    return false;
                }
				//status.text('Uploading...');
			},
			onComplete: function(file, response){
				if(response.error==null){
					var filename = response.file_name;
					$('#profile_image').hide();
					$('#upload_image').val(filename);
					$('#upload_image_name').html("<img src='"+base_url+"/uploads/photographer/thumb/"+filename+"' max-width='400px' height='70px'>");
					$('#upload_image_name').show();
					$('#change_image').show();
				}
				else
				{
					$.notify({
						title: "<strong><?php  echo lang('error')?>:</strong> ",
						message: response.error
					});
				}
			}		
		});		
	}
</script>



<script type="text/javascript">
	/* Rules for the form*/
	$(function () {
		$('#form-photographer').validate({
			rules: {
				// 'name': {
				// 	required: true
				// }
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	});
</script>