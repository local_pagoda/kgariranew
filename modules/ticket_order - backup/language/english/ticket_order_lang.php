<?php


$lang['id'] = 'Id';
$lang['event_id'] = 'Event Id';
$lang['buyer_id'] = 'Buyer Id';
$lang['orderer_name'] = 'Orderer Name';
$lang['orberer_email'] = 'Orberer Email';
$lang['quantity'] = 'Quantity';
$lang['item_price'] = 'Item Price';
$lang['order_phone'] = 'Order Phone';
$lang['order_address'] = 'Order Address';
$lang['orderer_country'] = 'Orderer Country';
$lang['orderer_company'] = 'Orderer Company';
$lang['orderer_city'] = 'Orderer City';
$lang['orderer_zip'] = 'Orderer Zip';
$lang['payment_status'] = 'Payment Status';
$lang['total_price'] = 'Total Price';
$lang['payment_method'] = 'Payment Method';
$lang['merchant_txn_id'] = 'Merchant Txn Id';
$lang['gateway_ref_no'] = 'Gateway Ref No';
$lang['ticket_name'] = 'Ticket Name';
$lang['status'] = 'Status';
$lang['created_date'] = 'Created Date';
$lang['updated_date'] = 'Updated Date';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';

$lang['create_ticket_order']='Create Ticket Order';
$lang['edit_ticket_order']='Edit Ticket Order';
$lang['delete_ticket_order']='Delete Ticket Order';
$lang['ticket_order_search']='Ticket Order Search';

$lang['ticket_order']='Ticket Order';
$lang['none']='none';

//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

