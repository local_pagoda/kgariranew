<!-- JQuery DataTable Css -->
<link href="<?php  echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?php  echo lang('ticket_order')?></h2>
		</div>

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('ticket_order')?>
						</h2>
					</div>

					<div class="body">
					</div>
				</div>
			</div>
		</div> <!-- close row clearfix -->

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('ticket_order')?>
						</h2>
						<div id="toolbar" style="padding:5px;height:auto">
							<p>
								<!-- <a href="#" class="btn bg-green waves-effect" onclick="create()" title="<?php   echo lang('create_ticket_order')?>"><?php  echo lang('create')?></a> -->
								<a href="#" class="btn bg-blue-grey waves-effect" onclick="removeSelected()"  title="<?php   echo lang('delete_ticket_order')?>"><?php  echo lang('remove_selected')?></a>
							</p>

						</div>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<table id="ticket_order-table" class="table table-bordered table-striped table-hover dataTable " >
							<thead>
				
<th ><?php echo lang('event_id')?></th>
<th ><?php echo lang('buyer_id')?></th>
<th ><?php echo lang('orderer_name')?></th>
<th ><?php echo lang('orderer_email')?></th>
<th ><?php echo lang('quantity')?></th>
<th ><?php echo lang('item_price')?></th>
<th ><?php echo lang('order_phone')?></th>
<th ><?php echo lang('order_address')?></th>
<th ><?php echo lang('orderer_country')?></th>
<th ><?php echo lang('orderer_company')?></th>
<th ><?php echo lang('orderer_city')?></th>
<th ><?php echo lang('orderer_zip')?></th>
<th><?php echo lang('payment_status')?></th>
<th ><?php echo lang('total_price')?></th>
<th ><?php echo lang('payment_method')?></th>
<th ><?php echo lang('merchant_txn_id')?></th>
<th ><?php echo lang('gateway_ref_no')?></th>
<th ><?php echo lang('ticket_name')?></th>
<th><?php echo lang('status')?></th>


					

							</thead>

							<tbody>
							</tbody>

						</table>
					</div><!-- close body -->
				</div><!-- close card -->
			</div><!-- close col -->
		</div><!-- close rowclearfix -->

		<!--for create and edit ticket_order form-->
		<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none">

			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Add or Edit Members</h4>
					</div>
					<div class="modal-body">
						<form id="form-ticket_order" method="post" class="form-horizontal" >

				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="orderer_name"><?php echo lang("orderer_name")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="orderer_name" id="orderer_name" class="form-control" placeholder="<?php echo lang('orderer_name'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="orderer_email"><?php echo lang("orderer_email")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="orderer_email" id="orderer_email" class="form-control" placeholder="<?php echo lang('orderer_email'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="quantity"><?php echo lang("quantity")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="quantity" id="quantity" class="form-control number" placeholder="<?php echo lang('quantity'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="order_phone"><?php echo lang("order_phone")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="order_phone" id="order_phone" class="form-control" placeholder="<?php echo lang('order_phone'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="order_address"><?php echo lang("order_address")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="order_address" id="order_address" class="form-control" placeholder="<?php echo lang('order_address'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="status"><?php echo lang("status")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="radio" value="1" name="status" id="status1" /><label for="status1"><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="status" id="status0" /><label for="status0"><?php echo lang("general_no")?></label></div>
						</div>
					</div>
			
				<input type="hidden" name="id" id="id"/>

						</form>
					</div> <!-- end body -->
					<div class="modal-footer">
						<button type="button" class="btn bg-green waves-effect" onClick="save()"><?php  echo  lang('general_save')?></button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
					</div>

				</div><!-- end content -->
			</div><!-- end modal-dialog -->



		</div><!-- end edit modal -->

	</div><!-- end container fluid -->
</section> <!-- end content -->


<!-- Input Mask Plugin Js -->
<script src="<?php  echo base_url(); ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script language="javascript" type="text/javascript">

	var dataTable; 
	$(function(){

		$('.date').inputmask('yyyy-mm-dd', { placeholder: '____-__-__' });
		$('.email').inputmask({ alias: "email" });

		dataTable = $('#ticket_order-table').DataTable({
			dom: 'Bfrtip',
			scrollX: true,
			"serverSide": true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "<?php  echo site_url('ticket_order/admin/ticket_order/json'); ?>",type: 'POST' },
			columns: [
		
				{ data: "event_id",name: "event_id"},
				{ data: "buyer_id",name: "buyer_id"},
				{ data: "orderer_name",name: "orderer_name"},
				{ data: "orderer_email",name: "orderer_email"},
				{ data: "quantity",name: "quantity"},
				{ data: "item_price",name: "item_price"},
				{ data: "order_phone",name: "order_phone"},
				{ data: "order_address",name: "order_address"},
				{ data: "orderer_country",name: "orderer_country"},
				{ data: "orderer_company",name: "orderer_company"},
				{ data: "orderer_city",name: "orderer_city"},
				{ data: "orderer_zip",name: "orderer_zip"},
				{ data: "payment_status",name: "payment_status"},
				{ data: "total_price",name: "total_price"},
				{ data: "payment_method",name: "payment_method"},
				{ data: "merchant_txn_id",name: "merchant_txn_id"},
				{ data: "gateway_ref_no",name: "gateway_ref_no"},
				{ data: "ticket_name",name: "ticket_name"},
				{ data: "status",name: "status"},
			
			
			]
		});

	});

	function create(){
		$('#form-ticket_order')[0].reset();
		$('#id').val('');
		$('#edit-modal').modal('show');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = dataTable.row(index).data();

		$('#id').val(row.id);
		$("#form-ticket_order").find('input:checkbox').prop('checked',false);
		$("#form-ticket_order").find('input:text,select').val(function(i,v){

			if(row.status == '1')
			{
				$('input:radio[name=status][id=status1]').prop('checked',true);
			}else{
				$('input:radio[name=status][id=status0]').prop('checked',true);
			}
			return row[this.name];
		});
		$('select').selectpicker('render');

		$("#form-ticket_order").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	
	}

	function removeticket_order(index)
	{
		$.post("<?php   echo site_url('ticket_order/admin/ticket_order/delete_json')?>", {id:[index]}, function(){
			dataTable.ajax.reload( null, false );
		});
	}

	function save()
	{
		var validator = $('#form-ticket_order').validate();
		if(validator.form())
		{
			$.ajax({
				url: '<?php   echo site_url('ticket_order/admin/ticket_order/save')?>',
				data: $('#form-ticket_order').serialize(),
				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#ticket_order-modal').modal('hide');
						$('#form-ticket_order')[0].reset();
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
	}
</script>
<script type="text/javascript">
	/* Rules for the form*/
	$(function () {
		$('#form-ticket_order').validate({
			rules: {
				// 'name': {
				// 	required: true
				// }
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	});
</script>