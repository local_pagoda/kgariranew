<?php

class Ticket_order extends Admin_Controller
{
	
	public function __construct(){
		parent::__construct();
		$this->load->module_model('ticket_order','ticket_order_model');
		$this->lang->module_load('ticket_order','ticket_order');

		$this->load->module_model('ticket_code','ticket_code_model');
		$this->load->module_model('event','event_model');
		$this->load->helper(array('url','file'));


		$this->load->helper('barcode');
		require_once APPPATH. 'libraries/newcode/code128.class.php';
	
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
	    // $this->load->library('vendor/autoload');
		/*require_once APPPATH. 'libraries/vendor/autoload.php';*/

		

	/*	require_once APPPATH. 'libraries/barcode_new/BCGDrawing.php';
		// require_once APPPATH. 'libraries/barcode_new/BCGFontFile.php';
		require_once APPPATH. 'libraries/barcode_new/BCGcode128.barcode.php';


*/

		/*C:\xampp\htdocs\kgariranew\application\libraries\vendor*/
	}

	public function index()
	{
		// Display Page
		$data['header'] = 'ticket_order';
		$data['page'] = $this->config->item('template_admin') . "ticket_order/index";
		$data['module'] = 'ticket_order';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$where = array();
		$params = $this->input->post(); 

		$total=$this->ticket_order_model->count();

		/*Count for filters*/
		$this->_get_search_param($params);	
		$filter_total=$this->ticket_order_model->count();

		/*Filters*/
		$this->_get_search_param($params);	
		
		$rows=$this->ticket_order_model->getTicketOrders(NULL,NULL,array('limit'=>$params['length'],'offset'=>$params['start']))->result_array();
		echo json_encode(array('draw'=>$params['draw'],'recordsTotal'=>$total,'recordsFiltered'=>$filter_total,'data'=>$rows));
	}
	
	public function _get_search_param($params)
	{

		foreach ($params['columns'] as $value) {
			if($value['searchable'] == 'true'){
				if($params['search']['value'] != '')
				{
					$this->db->or_where(array('ticket_orders.'.$value["name"].' like'=>'%'.$params['search']['value'].'%'));
				}

				if($value['search']['value'] != '')
				{
					$temp = explode(',', $value['search']['value']);
					$this->db->where_in('ticket_orders.'.$value['name'],$temp);
				}
			}
		}


					// Search Param Goes Here
					/*
					parse_str($this->input->post('data'),$params);
					if(!empty($params['search']))
					{
						($params['search']['event_id']!='')?$this->db->where('event_id',$params['search']['event_id']):'';
			($params['search']['buyer_id']!='')?$this->db->where('buyer_id',$params['search']['buyer_id']):'';
			($params['search']['orderer_name']!='')?$this->db->like('orderer_name',$params['search']['orderer_name']):'';
			($params['search']['orberer_email']!='')?$this->db->like('orberer_email',$params['search']['orberer_email']):'';
			($params['search']['quantity']!='')?$this->db->where('quantity',$params['search']['quantity']):'';
			($params['search']['item_price']!='')?$this->db->like('item_price',$params['search']['item_price']):'';
			($params['search']['order_phone']!='')?$this->db->like('order_phone',$params['search']['order_phone']):'';
			($params['search']['order_address']!='')?$this->db->like('order_address',$params['search']['order_address']):'';
			($params['search']['orderer_country']!='')?$this->db->like('orderer_country',$params['search']['orderer_country']):'';
			($params['search']['orderer_company']!='')?$this->db->like('orderer_company',$params['search']['orderer_company']):'';
			($params['search']['orderer_city']!='')?$this->db->like('orderer_city',$params['search']['orderer_city']):'';
			($params['search']['orderer_zip']!='')?$this->db->like('orderer_zip',$params['search']['orderer_zip']):'';
			($params['search']['payment_status']!='')?$this->db->like('payment_status',$params['search']['payment_status']):'';
			($params['search']['total_price']!='')?$this->db->like('total_price',$params['search']['total_price']):'';
			($params['search']['payment_method']!='')?$this->db->like('payment_method',$params['search']['payment_method']):'';
			($params['search']['merchant_txn_id']!='')?$this->db->like('merchant_txn_id',$params['search']['merchant_txn_id']):'';
			($params['search']['gateway_ref_no']!='')?$this->db->like('gateway_ref_no',$params['search']['gateway_ref_no']):'';
			($params['search']['ticket_name']!='')?$this->db->like('ticket_name',$params['search']['ticket_name']):'';
			(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';
			($params['search']['created_by']!='')?$this->db->like('created_by',$params['search']['created_by']):'';
			($params['search']['updated_by']!='')?$this->db->like('updated_by',$params['search']['updated_by']):'';

					}  

					
					if(!empty($params['date']))
							{
								foreach($params['date'] as $key=>$value){
									$this->_datewise($key,$value['from'],$value['to']);	
								}
							}
					*/


						}

						private function _datewise($field,$from,$to)
						{
							if(!empty($from) && !empty($to))
							{
								$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
									"' and '".date('Y-m-d',strtotime($to))."')");
							}
							else if(!empty($from))
							{
								$this->db->like($field,date('Y-m-d',strtotime($from)));				
							}		
						}	

						public function combo_json()
						{
							$rows=$this->ticket_order_model->getTicketOrders()->result_array();
							echo json_encode($rows);    	
						}    

						public function delete_json()
						{
							$id=$this->input->post('id');
							if($id && is_array($id))
							{
								foreach($id as $row):
									$this->ticket_order_model->delete('TICKET_ORDERS',array('id'=>$row));
								endforeach;
							}
						}

			//@param number of ticket quantity
			//@return barcode name of generated barcodes
			//@ function generate 128 code barcode      
						private function _generateBarCodes($nos)
						{

							$barcodes=array();
							for($i=1;$i<=$nos;$i++)
							{

								$text=strtoupper(rand(10000,99999));
								$code = 'k'.$text.date('Ymd');
								$dest="assets/barcodes/".$code.".jpg";

								$barcode = new phpCode128($code, 100, 'c:\windows\fonts\verdana.ttf', 18);
								$barcode->setAutoAdjustFontSize(true);
								$barcode->setBorderWidth(1);
								$barcode->setShowText(true);
								$barcode->setPixelWidth(1);
								$barcode->setTextSpacing(5);
								$barcode->saveBarcode($dest);
								$barcodes[$i]=$code;

							}


							return $barcodes;
						}

			//@param null
			//@return user data of resent user
			//@function collect user data to generate pdf file and email
						private function _userdata()
						{
							$user_data=array();
							$user_data['orderer_name'] = $this->input->post('orderer_name');
							$user_data['order_date']=date('Y-m-d H:i:s');
							$user_data['order_by']=$this->user_id;

							$user_data['order_by_email']=$this->input->post('orderer_email');
							$user_data['quantity']=$this->input->post('quantity');
							$user_data['contact']=$this->input->post('order_phone');


							$user_data['payment_status']="Completed";
							$user_data['address']=$this->input->post('order_address');

							return $user_data;

						}

			// @param current event id
			// @return null
			// @function get detail of event, venue, ticket etc from database
						private function _getdeatils($event_id)
						{
							$id = $event_id;
							$data['event'] = $this->db->get_where('tbl_events', array('id' => $id))->result_array();


							$this->db->select("venue_id");
							$this->db->from("tbl_events");
							$this->db->where('id',$id);
							$query = $this->db->get();
							$venue_id_array = $query->result_array();
							$venue_id = $venue_id_array[0]['venue_id'];

							$data['venue'] = $this->db->get_where('tbl_venues', array('id' => $venue_id))->result_array();



							$data['currency'] = $this->input->post('ticket_currency');
							$data['price'] = $this->input->post('price');

							return $data;
						}

			//@param Ticket name for recently added ticket
			// @return null
			// function send mail to admin and issuing user

   			 //ticket generation with barcode at admin issue
			//@param buyer from user data function
			//@param name of resent ticket to be generated
			//@param ticket of current event
			//@param name of resently generated barcode
			//@return null
			//@function generate a pdg ticket 

						private function _generateTicket($buyer,$file_name,$event_id,$barcodes)
						{		


							$id=$event_id;
							$this->load->library('html2pdf');

							$this->db->select("venue_id");
							$this->db->from("tbl_events");
							$this->db->where('id',$id);
							$query = $this->db->get();
							$venue_id_array = $query->result_array();
							$venue_id = $venue_id_array[0]['venue_id'];


							$this->db->select("venue_name");
							$this->db->from("tbl_venues");
							$this->db->where('id',$venue_id);
							$query = $this->db->get();
							$venue_name = $query->result_array();


							$data_['event'] = $this->db->get_where('tbl_events', array('id' => $id))->result_array();
							$data_['buyer']=$buyer;
							$data_['barcodes']=$barcodes;
							$data_['venue_name'] = $venue_name;
							$content=$this->load->view('pdfnew',$data_,TRUE);

							$this->html2pdf->WriteHTML($content);
							$path='tickets/';

							$this->html2pdf->Output($path.$file_name,'F');	


						}
			//@param resent ticket name
			//@return null
			//@function send mail to user and admin with ticket attached 
						public function send_mail($ticket_file_name,$info)
						{

							$this->load->library('email');
 							$config = Array(        
								'protocol' => 'smtp',
								'smtp_host' => 'smtp.vianet.com.np',


								'mailtype'  => 'html', 
					// 'charset'   => 'utf-8',
					// 'smtp_port' => 465,
								'charset'   => 'iso-8859-1'
								);

							/*$this->email->set_mailtype('html');*/

							$this->email->set_newline("\r\n");
							$this->email->initialize($config);
							$data['info'] = $this->_userdata();

							$data['event_details'] = $info;
							$processing_percent = $data['event_details']['event'][0]['processing_fees'];
							$total_without_pf = $data['event_details']['price'] * $data['info']['quantity'];
							$processing_fees = ($total_without_pf * $processing_percent)/100;
							$total = $total_without_pf + $processing_fees;

							$this->load->library('parser');		
							$parse_data=array(

								'EVENT_ID' => $data['event_details']['event'][0]['id'],
								'EVENT_NAME' =>$data['event_details']['event'][0]['name'],
								'EVENT_STARTS' =>$data['event_details']['event'][0]['start_date'],
								'EVENT_ENDS' =>$data['event_details']['event'][0]['end_date'],
								'VENUE_NAME' =>$data['event_details']['venue'][0]['venue_name'],
								'VENUE_LOCATION' =>$data['event_details']['venue'][0]['venue_location'],
								'VENUE_CITY' => $data['event_details']['venue'][0]['venue_city'],
								'ORDER_BY' => $data['info']['orderer_name'],
								'ORDERER_EMAIL' => $data['info']['order_by_email'],
								'QUANTITY' => $data['info']['quantity'],
								'ORDER_BY' => $data['info']['orderer_name'],
								'ORDERER_ADDRESS' => $data['info']['address'],
								'ORDERER_CONTACT' => $data['info']['contact'],
								'CURRENCY' => $data['event_details']['currency'],
								'PRICE' => $data['event_details']['price'],
								'TWPF' => $total_without_pf,
								'PROCESSING_FEES' => $processing_fees,
								'TOTAL'=> $total,
								'ORDER_DATE' => $data['info']['order_date'],
								);


							$this->email->from('prajapati.niroj123@gmail.com');
							$this->email->to('prajapati.niroj123@gmail.com');
							$query = $this->db->get_where('email_templates', array('email_template_id' => 1))->result_array();

							foreach ($query as $value) {
								$subject = $value['subject'];
								$body = $value['body'];
							}
							$this->email->subject($subject);  
							$body = $this->parser->parse_string($body,$parse_data,TRUE);


							$this->email->message($body); 
							$file_name = FCPATH.'tickets/'.$ticket_file_name;
							$this->email->attach($file_name);  
							$this->email->send();
							$this->email->clear(TRUE);


							$name = $data['info']['orderer_name'];
							$email= $data['info']['order_by_email'];
							$this->email->from('prajapati.niroj123@gmail.com','Kgarira');
							$this->email->to($email);
							
							$query = $this->db->get_where('email_templates', array('email_template_id' => 2))->result_array();

							foreach ($query as $value) {
								$subject = $value['subject'];
								$body = $value['body'];
							}
							$this->email->subject($subject);  
							$body = $this->parser->parse_string($body,$parse_data,TRUE);

							$this->email->message($body); 

							$this->email->send();


				$this->_mailbox_insert($name,$email,$subject,$body); // insert user info in mail box table

			}

			//@param name of a user who issue ticket 
			//@param email of a ticket issuer
			//@param subject of a email
			//@param mail body sends to issuer
			//@return null
			//@function add data to mail box
			private function _mailbox_insert($name,$email,$subject,$body)
			{
				$data['sender'] = $name;
				$data['sender_email'] = $email;
				$data['subject'] = $subject;
				$data['body'] = $body;
				$data['mailed_date'] = date('Y-m-d H:i:s');
				$data['created_date'] = date('Y-m-d H:i:s');
				$date['created_by'] = $this->user_id;
				$data['is_read'] = '0';
				$data['mail_type'] = 'Payment';		
				$this->db->insert('tbl_mailboxes', $data); 
			}


			public function save()
			{
				        $data=$this->_get_posted_data(); //Retrive Posted Data	

				        $user_email = $data['orderer_email'];

				        $barcodes = $this->_generateBarCodes($data['quantity']);
				        $event_id = $data['event_id'];
				        $ticket_file_name=$event_id.date('_Y_m_d_h_i_s').'.pdf';

				        $user_data = $this->_userdata();

				        $this->_generateTicket($user_data,$ticket_file_name,$event_id,$barcodes);
				        $getdata = $this->_getdeatils($event_id);
				        $data_['event_details']= $getdata;
				        $processing_percent = $data_['event_details']['event'][0]['processing_fees'];
				        $total_without_pf = $data_['event_details']['price'] * $data['quantity'];
				        $processing_fees = ($total_without_pf * $processing_percent)/100;
				        $total = $total_without_pf + $processing_fees;

				        if(!$this->input->post('id'))
				        {
				        	$data['created_date'] = date("Y-m-d H:i:s");
				        	$data['created_by'] = $this->user_id;
				        	$data['ticket_name'] = $ticket_file_name;
				        	$data['item_price'] = $total_without_pf;
				        	$data['total_price'] = $total;
				        	$success=$this->ticket_order_model->insert('TICKET_ORDERS',$data);
				        }
				        else
				        {
				        	$data['updated_date'] = date('Y-m-d H:i:s');
				        	$data['updated_by'] = $this->user_id;
				        	$success=$this->ticket_order_model->update('TICKET_ORDERS',$data,array('id'=>$data['id']));
				        }
				        
				        if($success)
				        {
				        	$ticket_order_id = $this->db->insert_id();
				        	$data_barcode = array();
				        	foreach ($barcodes as $unique_barcode) { //inserting a ticket code in tiket code table with unique  barcode
				        		$data_barcode[] = array('event_id'=>$event_id, 'ticket_order_id'=>$ticket_order_id , 'code'=> $unique_barcode, 'status'=> 'valid');
				        	}
				        	$this->db->insert_batch('tbl_ticket_codes', $data_barcode); 
				        	$this->send_mail($ticket_file_name,$getdata);

				        	$success = TRUE;
				        	$msg=lang('success_message');
				        	

				        } 
				        else
				        {
				        	$success = FALSE;
				        	$msg=lang('failure_message');
				        }

				        echo json_encode(array('msg'=>$msg,'success'=>$success));		

				    }

				    private function _get_posted_data()
				    {

				    	
				    	$data=array();
				    	if($this->input->post('id'))
				    	{
				    		$data['id'] = $this->input->post('id');
				    	}        
				    	if($this->input->post('order_event_id'))
				    	{
				    		$data['event_id'] = $this->input->post('order_event_id');
				    	}
				    	$data['buyer_id'] = $this->user_id;
				    	$data['orderer_name'] = $this->input->post('orderer_name');
				    	$data['orderer_email'] = $this->input->post('orderer_email');
				    	$data['quantity'] = $this->input->post('quantity');
				    	$data['item_price'] = $this->input->post('item_price');
				    	$data['order_phone'] = $this->input->post('order_phone');
				    	$data['order_address'] = $this->input->post('order_address');
				    	$data['orderer_country'] = $this->input->post('orderer_country');
				    	$data['orderer_company'] = $this->input->post('orderer_company');
				    	$data['orderer_city'] = $this->input->post('orderer_city');
				    	$data['orderer_zip'] = $this->input->post('orderer_zip');
				    	$data['payment_status'] = $this->input->post('payment_status');
				    	$data['total_price'] = $this->input->post('total_price');
				    	$data['payment_method'] = lang('none'); 
				    	$data['merchant_txn_id'] = $this->input->post('merchant_txn_id');
				    	$data['gateway_ref_no'] = $this->input->post('gateway_ref_no');
				    	$data['ticket_name'] = $this->input->post('ticket_name');
				    	$data['status'] = $this->input->post('status');

				    	return $data;
				    }
				}