<?php

class Rsvp extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('rsvp','rsvp_model');
        $this->lang->module_load('rsvp','rsvp');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'rsvp';
		$data['page'] = $this->config->item('template_admin') . "rsvp/index";
		$data['module'] = 'rsvp';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$where = array();
		$params = $this->input->post(); 

		$total=$this->rsvp_model->count();

		/*Count for filters*/
		$this->_get_search_param($params);	
		$filter_total=$this->rsvp_model->count();

		/*Filters*/
		$this->_get_search_param($params);	
		
		$rows=$this->rsvp_model->getRsvps(NULL,NULL,array('limit'=>$params['length'],'offset'=>$params['start']))->result_array();
		echo json_encode(array('draw'=>$params['draw'],'recordsTotal'=>$total,'recordsFiltered'=>$filter_total,'data'=>$rows));
	}
	
	public function _get_search_param($params)
	{

		foreach ($params['columns'] as $value) {
			if($value['searchable'] == 'true'){
				if($params['search']['value'] != '')
				{
					$this->db->or_where(array('rsvps.'.$value["name"].' like'=>'%'.$params['search']['value'].'%'));
				}

				if($value['search']['value'] != '')
				{
					$temp = explode(',', $value['search']['value']);
					$this->db->where_in('rsvps.'.$value['name'],$temp);
				}
			}
		}


		// Search Param Goes Here
		/*
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['event_id']!='')?$this->db->where('event_id',$params['search']['event_id']):'';
($params['search']['user_email']!='')?$this->db->like('user_email',$params['search']['user_email']):'';

		}  

		
		if(!empty($params['date']))
				{
					foreach($params['date'] as $key=>$value){
						$this->_datewise($key,$value['from'],$value['to']);	
					}
				}
		*/
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->rsvp_model->getRsvps()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->rsvp_model->delete('RSVP',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->rsvp_model->insert('RSVP',$data);
        }
        else
        {
            $success=$this->rsvp_model->update('RSVP',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['event_id'] = $this->input->post('event_id');
$data['user_email'] = $this->input->post('user_email');

        return $data;
   }
   
   	
	    
}