<?php


$lang['id'] = 'Id';
$lang['name'] = 'Name';
$lang['venue_id'] = 'Venue Id';
$lang['venue_name'] = 'Venue';


$lang['type_id'] = 'Type Id';
$lang['type_name'] = 'Type';

$lang['promoters_id'] = 'Promoters Id';
$lang['promoter_name'] = 'Promoters';

$lang['country_id'] = 'Country Id';
$lang['country_name'] = 'Country';

$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['description'] = 'Description';
$lang['image'] = 'Image';
$lang['fb_event_id'] = 'Fb Event Id';
$lang['is_fb_event'] = 'Is Fb Event';
$lang['is_guest_event'] = 'Is Guest Event';
$lang['is_spam'] = 'Is Spam';
$lang['is_featured'] = 'Is Featured';
$lang['guest_id'] = 'Guest Id';
$lang['guest_name'] = 'Guest';

$lang['status'] = 'Status';


$lang['created_date'] = 'Created Date';
$lang['updated_date'] = 'Updated Date';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';
$lang['ticket_image'] = 'Ticket Image';

$lang['create_event']='Create Event';
$lang['edit_event']='Edit Event';
$lang['delete_event']='Delete Event';
$lang['event_search']='Event Search';

$lang['event']='Event';



//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

$lang['id'] = 'Id';
$lang['event_id'] = 'Event Id';
$lang['image'] = 'Image';
$lang['ticket_count'] = 'Ticket Count';
$lang['ticket_price'] = 'Ticket Price';
$lang['currency'] = 'Currency';


$lang['create_ticket']='Create Ticket';
$lang['edit_ticket']='Edit Ticket';
$lang['delete_ticket']='Delete Ticket';
$lang['ticket_search']='Ticket Search';

$lang['ticket']='Ticket';

//language for ticket order
$lang['buyer_id'] = 'Buyer Id';
$lang['orderer_name'] = 'Orderer Name';
$lang['orberer_email'] = 'Orberer Email';
$lang['quantity'] = 'Quantity';
$lang['item_price'] = 'Item Price';
$lang['order_phone'] = 'Order Phone';
$lang['order_address'] = 'Order Address';
$lang['orderer_country'] = 'Orderer Country';
$lang['orderer_company'] = 'Orderer Company';
$lang['orderer_city'] = 'Orderer City';
$lang['orderer_zip'] = 'Orderer Zip';
$lang['payment_status'] = 'Payment Status';
$lang['total_price'] = 'Total Price';
$lang['payment_method'] = 'Payment Method';
$lang['merchant_txn_id'] = 'Merchant Txn Id';
$lang['gateway_ref_no'] = 'Gateway Ref No';
$lang['ticket_name'] = 'Ticket Name';
$lang['status'] = 'Status';
$lang['created_date'] = 'Created Date';
$lang['updated_date'] = 'Updated Date';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';

$lang['create_ticket_order']='Create Ticket Order';
$lang['edit_ticket_order']='Edit Ticket Order';
$lang['delete_ticket_order']='Delete Ticket Order';
$lang['ticket_order_search']='Ticket Order Search';

$lang['ticket_order']='Ticket Order';