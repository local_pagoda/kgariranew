<!-- JQuery DataTable Css -->
<link href="<?php  echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?php  echo lang('event')?></h2>
		</div>

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('event')?>
						</h2>
					</div>

					<div class="body">
					</div>
				</div>
			</div>
		</div> <!-- close row clearfix -->

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('event')?>
						</h2>
						<div id="toolbar" style="padding:5px;height:auto">
							<p>
								<a href="#" class="btn bg-green waves-effect" onclick="create()" title="<?php   echo lang('create_event')?>"><?php  echo lang('create')?></a>
								<a href="#" class="btn bg-blue-grey waves-effect" onclick="removeSelected()"  title="<?php   echo lang('delete_event')?>"><?php  echo lang('remove_selected')?></a>
							</p>

						</div>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<table id="event-table" class="table table-bordered table-striped table-hover dataTable " >
							<thead>
								
								<th ><?php echo lang('name')?></th>
								<th ><?php echo lang('venue_id')?></th>
								<th ><?php echo lang('type_id')?></th>
								<th ><?php echo lang('promoters_id')?></th>
								<th ><?php echo lang('country_id')?></th>
								<th ><?php echo lang('start_date')?></th>
								<th ><?php echo lang('end_date')?></th>
								<th ><?php echo lang('image')?></th>
								<!-- <th ><?php echo lang('fb_event_id')?></th> -->
								<th ><?php echo lang('is_fb_event')?></th>
								<th ><?php echo lang('is_guest_event')?></th>
								<th ><?php echo lang('guest_id')?></th>
								<!-- <th ><?php echo lang('is_spam')?></th> -->
								<th ><?php echo lang('is_featured')?></th>
								<!-- <th ><?php echo lang('guest_id')?></th> -->
								<th><?php echo lang('status')?></th>

								<th>Ticket</th>
								<th>Action</th>

							</thead>

							<tbody>
							</tbody>

						</table>
					</div><!-- close body -->
				</div><!-- close card -->
			</div><!-- close col -->
		</div><!-- close rowclearfix -->

		<!--for create and edit event form-->
		<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none">

			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Create Events</h4>
					</div>
					<div class="modal-body">
						<form id="form-event" method="post" class="form-horizontal" enctype="multipart/form-data" >

							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="name"><?php echo lang("name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="text" name="name" id="name" class="form-control" placeholder="<?php echo lang('name'); ?>"></div>
									</div>
								</div>
							</div><!-- close row clearfix -->


							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="venue_id"><?php echo lang("venue_name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<select style="background-color: #2b8714;"  name="venue_id" id="select" placeholder="Select Items">

												<?php foreach ($venues as $venue):  ?>

													<option value="<?php echo $venue['id']; ?>"> <?php echo $venue['venue_name']; ?></option>

												<?php endforeach; ?>   
											</select>
										</div>
									</div>
								</div>
							</div><!-- close row clearfix -->


							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="type_id"><?php echo lang("type_name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">

											<select style="background-color: #2b8714;"  name="type_id" id="select" placeholder="Select Items">

												<?php foreach ($event_types as $event):  ?>



													<option value="<?php echo $event['id']; ?>"><?php echo $event['type']; ?></option>

												<?php endforeach; ?>   
											</select>
										</div>
									</div>
								</div>
							</div><!-- close row clearfix -->


							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="promoters_id"><?php echo lang("promoter_name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<select style="background-color: #2b8714;"  name="promoters_id" id="select" placeholder="Select Items">

												<?php foreach ($promoters as $promoter):  ?>

													<option value="<?php echo $promoter['id']; ?>"> <?php echo $promoter['username']; ?></option>

												<?php endforeach; ?>   
											</select>
										</div>
									</div>
								</div>
							</div><!-- close row clearfix -->

							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="country_id"><?php echo lang("country_name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<select style="background-color: #2b8714;"  name="country_id" id="select" placeholder="Select Items">

												<?php foreach ($countries as $country):  ?>

													<option value="<?php echo $country['id']; ?>"> <?php echo $country['country_name']; ?></option>

												<?php endforeach; ?>   
											</select>
										</div>
									</div>
								</div>
							</div><!-- close row clearfix -->

							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="start_date"><?php echo lang("start_date")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="date" name="start_date" id="start_date" class="form-control datetime" placeholder="<?php echo lang('start_date'); ?>"></div>
									</div>
								</div>
							</div><!-- close row clearfix -->
							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="end_date"><?php echo lang("end_date")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="date" name="end_date" id="end_date" class="form-control datetime" placeholder="<?php echo lang('end_date'); ?>"></div>
									</div>
								</div>
							</div><!-- close row clearfix -->
							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="description"><?php echo lang("description")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><textarea name="description" id="description" class="form-control" placeholder="About Event"></textarea></div>
									</div>
								</div>
							</div><!-- close row clearfix -->


							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="image"><?php echo lang("image")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">

											<label id="upload_image_name" ></label>
											<input type="file" name="event_image" id="image" class="form-control" placeholder="<?php echo lang('image'); ?>">
											<input type="text" name="image" id="upload_image" hidden>

											<button type="button" class="btn btn-default waves-effect" id="change_image" title="Change Image" style="display: none;" > <i class="material-icons">switch_camera</i>Change Image </button>

										</div>
									</div>
								</div>
							</div><!-- close row clearfix -->
							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="is_fb_event"><?php echo lang("is_fb_event")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="radio" value="1" name="is_fb_event" id="is_fb_event1" checked /><label for="is_fb_event1"><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="is_fb_event" id="is_fb_event0" /><label for="is_fb_event0"><?php echo lang("general_no")?></label></div>
									</div>
								</div>
							</div><!-- close row clearfix -->
							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="fb_event_id"><?php echo lang("fb_event_id")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="text" name="fb_event_id" id="fb_event_id" class="form-control number" placeholder="<?php echo lang('fb_event_id'); ?>"></div>
									</div>
								</div>
							</div><!-- close row clearfix -->

							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="is_guest_event"><?php echo lang("is_guest_event")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="radio" value="1" name="is_guest_event" id="is_guest_event1" checked /><label for="is_guest_event1"><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="is_guest_event" id="is_guest_event0" /><label for="is_guest_event0"><?php echo lang("general_no")?></label></div>
									</div>
								</div>
							</div><!-- close row clearfix -->



							<div class="row clearfix"  id = "findguest">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="guest_id"><?php echo lang("guest_name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
											<select style="background-color: #2b8714;"  name="guest_id" id="select_" placeholder="Select Items">

												<?php foreach ($guests as $guest):  ?>

													<option value="<?php echo $guest['id']; ?>"> 

														<?php echo $guest['firstname']." ".$guest['lastname']; ?></option>




													<?php endforeach; ?>   
												</select>
											</div>
										</div>
									</div>
								</div><!-- close row clearfix -->


								<div class="row clearfix">
									<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
										<label for="is_spam"><?php echo lang("is_spam")?></label>
									</div>
									<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line"><input type="text" name="is_spam" id="is_spam" class="form-control" placeholder="<?php echo lang('is_spam'); ?>"></div>
										</div>
									</div>
								</div><!-- close row clearfix -->
								<div class="row clearfix">
									<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
										<label for="is_featured"><?php echo lang("is_featured")?></label>
									</div>
									<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line"><input type="radio" value="1" name="is_featured" id="is_featured1" checked /><label for="is_featured1"><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="is_featured" id="is_featured0" /><label for="is_featured0"><?php echo lang("general_no")?></label></div>
										</div>
									</div>
								</div><!-- close row clearfix -->

								<div class="row clearfix">
									<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
										<label for="status"><?php echo lang("status")?></label>
									</div>
									<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line"><input type="radio" value="1" name="status" id="status1" checked /><label for="status1" ><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="status" id="status0" /><label for="status0"><?php echo lang("general_no")?></label></div>
										</div>
									</div>
								</div><!-- close row clearfix -->

								<input type="hidden" name="id" id="id"/>

							</form>
						</div> <!-- end body -->
						<div class="modal-footer">
							<button type="button" class="btn bg-green waves-effect" onClick="save()"><?php  echo  lang('general_save')?></button>
							<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
						</div>

					</div><!-- end content -->
				</div><!-- end modal-dialog -->
			</div><!-- end edit modal -->
			<div class="modal fade in" id="mdModal" tabindex="-1" role="dialog" style="display: none;">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title" id="largeModalLabel">Tickets</h4>
						</div>
						<div class="modal-body">
							<p><button type="button" class="btn bg-green waves-effect" onClick="create_ticket()">Create Ticket</button></p> 
							<div class="body">

								<table  class="table table-bordered table-striped table-hover dataTable" id="ticket-table">
									<thead>


										<th ><?php echo lang('event_id')?></th>
										<th ><?php echo lang('ticket_image')?></th>
										<th ><?php echo lang('ticket_count')?></th>
										<th ><?php echo lang('ticket_price')?></th>
										<th ><?php echo lang('currency')?></th>
										<th><?php echo lang('status')?></th>
										<th>Issue Ticket</th>

										<th>Action</th>


									</thead>

									<tbody>
									</tbody>

								</table>
							</div><!-- close body -->
						</div>
						<div class="modal-footer">

							<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
						</div>
					</div>
				</div>
			</div>

		</div> <!-- end body -->
		<div class="modal-footer">

		</div>

	</div><!-- end content -->
</div><!-- end modal-dialog --> 



</div><!-- end edit modal -->

<div id="ticket-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none">

	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="defaultModalLabel">Create Ticket</h4>
			</div>
			<div class="modal-body">
				<form id="form-ticket" method="post" class="form-horizontal" >


					<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="ticket_image"><?php echo lang("ticket_image")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line">

									<label id="upload_ticket_image_name" ></label>
									<input type="file" name="event_ticket_image" id="ticket_image" class="form-control" placeholder="<?php echo lang('ticket_image'); ?>">
									<input type="text" name="ticket_image" id="upload_ticket_image" hidden>

									<button type="button" class="btn btn-default waves-effect" id="change_ticket_image" title="Change Image" style="display: none;" > <i class="material-icons">switch_camera</i>Change Image </button>

								</div>
							</div>
						</div>
					</div><!-- close row clearfix -->
					<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="ticket_count"><?php echo lang("ticket_count")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="ticket_count" id="ticket_count" class="form-control" placeholder="<?php echo lang('ticket_count'); ?>"></div>
							</div>
						</div>
					</div><!-- close row clearfix -->
					<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="ticket_price"><?php echo lang("ticket_price")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="ticket_price" id="ticket_price" class="form-control" placeholder="<?php echo lang('ticket_price'); ?>"></div>
							</div>
						</div>
					</div> <!-- close row prefix -->
					<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="currency"><?php echo lang("currency")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line">
									<select style="background-color: #2b8714;"  name="currency" id="currency" placeholder="Select Items">

										<?php foreach ($codes as $code):  ?>

											<option value="<?php echo $code['currency_code']; ?>"> <?php echo $code['country_name']; ?></option>

										<?php endforeach; ?>   
									</select>
								</div>
							</div>
						</div>
					</div><!-- close row clearfix -->

					<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="status"><?php echo lang("status")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="radio" value="1" name="status" id="status_1" checked /><label for="status_1"><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="status" id="status_0" /><label for="status_0"><?php echo lang("general_no")?></label></div>
							</div>
						</div>
					</div><!-- close row clearfix -->

					<input type="hidden" name="id" id="ticket_id"/>
					<input type="hidden" name="event_id" id="event_id" value="" />


				</form>
			</div> <!-- end body -->
			<div class="modal-footer">
				<button type="button" class="btn bg-green waves-effect" onClick="save_ticket()"><?php  echo  lang('general_save')?></button>
				<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
			</div>

		</div><!-- end content -->
	</div><!-- end modal-dialog -->
</div><!-- end edit modal -->

<div id="ticket-order-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none">

			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Issue Ticket</h4>
					</div>
					<div class="modal-body">
						<form id="form-ticket_order" method="post" class="form-horizontal" >

					
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="orderer_name"><?php echo lang("orderer_name")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="orderer_name" id="orderer_name" class="form-control" placeholder="<?php echo lang('orderer_name'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="orberer_email"><?php echo lang("orberer_email")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="orberer_email" id="orberer_email" class="form-control" placeholder="<?php echo lang('orberer_email'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				
				
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="order_phone"><?php echo lang("order_phone")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="order_phone" id="order_phone" class="form-control" placeholder="<?php echo lang('order_phone'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="order_address"><?php echo lang("order_address")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="order_address" id="order_address" class="form-control" placeholder="<?php echo lang('order_address'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="quantity"><?php echo lang("quantity")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="quantity" id="quantity" class="form-control number" placeholder="<?php echo lang('quantity'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="status"><?php echo lang("status")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="radio" value="1" name="status" id="status1" checked /><label for="status1"><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="status" id="status0" /><label for="status0"><?php echo lang("general_no")?></label></div>
						</div>
					</div>
			
				<input type="hidden" name="id" id="id"/>
				<input type="hidden" name="order_event_id" id="order_event_id"/>

						</form>
					</div> <!-- end body -->
					<div class="modal-footer">
						<button type="button" class="btn bg-green waves-effect" onClick="ticket_order_save()"><?php  echo  lang('general_save')?></button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
					</div>

				</div><!-- end content -->
			</div><!-- end modal-dialog -->



		</div><!-- end edit modal -->

</div><!-- end container fluid -->
</section> <!-- end content -->


<!-- Input Mask Plugin Js -->
<script src="<?php  echo base_url(); ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script language="javascript" type="text/javascript">
	var base_url = '<?php echo base_url(); ?>';
	var active_event;

	var dataTable; 
	$(function(){

		$('.date').inputmask('yyyy-mm-dd', { placeholder: '____-__-__' });
		$('.email').inputmask({ alias: "email" });

		dataTable = $('#event-table').DataTable({
			dom: 'Bfrtip',
			scrollX: true,
			"serverSide": true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "<?php  echo site_url('event/admin/event/json'); ?>",type: 'POST' },
			columns: [

			 // { data: "id",name: "id"},
			 { data: "name",name: "name"},
			 { data: "venue_id",name: "venue_id"},
			 { data: "type_id",name: "type_id"},
			 { data: "promoters_id",name: "promoters_id"},
			 { data: "country_id",name: "country_id"},
			 { data: "start_date",name: "start_date"},
			 { data: "end_date",name: "end_date"},
				// { data: "description",name: "description"},
				{ data: "image",name: "image"},
				// { data: "fb_event_id",name: "fb_event_id"},
				{ data: "is_fb_event",name: "is_fb_event"},
				{ data: "is_guest_event",name: "is_guest_event"},
				{ data: "guest_id",name: "guest_id"},

				// { data: "is_spam",name: "is_spam"},
				{ data: "is_featured",name: "is_featured"},
				// { data: "guest_id",name: "guest_id"},
				{ data: "status",name: "status"},
				// { data: "created_date",name: "created_date"},
				// { data: "updated_date",name: "updated_date"},
				// { data: "created_by",name: "created_by"},
				// { data: "updated_by",name: "updated_by"},
				{ data: function(data,b,c,table) { 
					var buttons = '';
					buttons += "<button class='btn bg-teal btn-block btn-lg waves-effect glyphicon glyphicon-credit-card' onclick='ticket("+data.id+")'></button>";

					return buttons;
				}, name:'action',searchable: false},
				
				{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<button type='button' class='btn bg-grey waves-effect glyphicon glyphicon-pencil' data-toggle='modal' data-target='#edit-modal' onclick='edit("+table.row+")'></button>&nbsp";
					buttons += "<button class='btn bg-red waves-effect glyphicon glyphicon-trash' onclick='removeevent("+data.id+")'></button>";

					return buttons;
				}, name:'action',searchable: false},
				]
			});

		/*Change Image*/
		$("#change_image").click(function(){
			var filename = $('#upload_image').val();
			$.post("<?php echo site_url('event/admin/event/upload_delete')?>", {filename:filename}, function(){
				$("#change_image").hide();
				$('#image').show();
				$('#upload_image').val('');
				$('#upload_image_name').text('');
				$.notify({
					title: "<strong><?php  echo lang('success')?>:</strong> ",
					message: "Removed previous image !"
				});
			});
		});

	});

	function create(){
		$('#form-event')[0].reset();
		$('#id').val('');
		$('#edit-modal').modal('show');
		uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = dataTable.row(index).data();

		$('#id').val(row.id);
		$("#form-event").find('input:checkbox').prop('checked',false);
		$("#form-event").find('input:text,select').val(function(i,v){

			if(row.status == '1')
			{
				$('input:radio[name=status][id=status1]').prop('checked',true);

			}else{
				$('input:radio[name=status][id=status0]').prop('checked',true);
			}

			if(row.is_fb_event == '1')
			{
				$('input:radio[name=is_fb_event][id=is_fb_event1]').prop('checked',true);
				
			}else{
				$('input:radio[name=is_fb_event][id=is_fb_event0]').prop('checked',true);
				
			}

			if(row.is_guest_event == '1')
			{
				$('input:radio[name=is_guest_event][id=is_guest_event1]').prop('checked',true);
				// $('#findguest').css('display': 'inline-block');
				
			}else{
				$('input:radio[name=is_guest_event][id=is_fb_event0]').prop('checked',true);
				  // $('#navigation ul li').css('display': 'none');

				}
				if(row.is_featured == '1')
				{
					$('input:radio[name=is_featured][id=is_featured1]').prop('checked',true);
				}else{
					$('input:radio[name=is_featured][id=is_featured0]').prop('checked',true);
				}
				return row[this.name];
			});
		$('select').selectpicker('render');

		$("#form-event").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	

		if(Boolean(row.image))
		{
			$('#upload_image').val(row.image);
			$('#image').hide();
			$('#upload_image_name').html("<img src='"+base_url+"/uploads/event/thumb/"+row.image+"' max-width='400px' height='70px'>");
			$('#change_image').show();
		}
		uploadReady();
	}

	function removeevent(index)
	{
		$.post("<?php   echo site_url('event/admin/event/delete_json')?>", {id:[index]}, function(){
			dataTable.ajax.reload( null, false );
		});
	}

	function save()
	{
		var validator = $('#form-event').validate();
		if(validator.form())
		{
			$.ajax({
				url: '<?php   echo site_url('event/admin/event/save')?>',
				data: $('#form-event').serialize(),
				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#edit-modal').modal('hide');
						$('#form-event')[0].reset();
						$('#ticket_image').val('');
						$('#upload_image_name').html('');
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
	}
	function uploadReady()
	{
		uploader=$('#image');
		new AjaxUpload(uploader, {
			action: '<?php  echo site_url('event/admin/event/upload_image')?>',
			name: 'userfile',
			responseType: "json",
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
                    $.notify({
                    	title: "<strong><?php  echo lang('error')?>:</strong> ",
                    	message: "Only JPG, PNG or GIF files are allowed"
                    });
                    return false;
                }
				//status.text('Uploading...');
			},
			onComplete: function(file, response){
				if(response.error==null){
					var filename = response.file_name;
					$('#image').hide();
					$('#upload_image').val(filename);
					$('#upload_image_name').html("<img src='"+base_url+"/uploads/event/thumb/"+filename+"' max-width='400px' height='70px'>");
					$('#upload_image_name').show();
					$('#change_image').show();
				}
				else
				{
					$.notify({
						title: "<strong><?php  echo lang('error')?>:</strong> ",
						message: response.error
					});
				}
			}		
		});	
	}	
	$(function () {
		// name=is_guest_event][id=is_guest_event1

		$("#form-event input[name=is_guest_event]").click(function () {
			console.log($('#form-event input[name=is_guest_event]:checked').val());
			if ($('#form-event input[name=is_guest_event]:checked').val() == "1") {
				$('#findguest').show();

			} else if ($('#form-event input[name=is_guest_event]:checked').val() == "0") {
				$('#findguest').hide();

			}
		});
	});
	function ticket(id)
	{

		active_event = id;
		$('#mdModal').modal('show');
		ticket_dataTable = $('#ticket-table').DataTable({
			dom: 'Bfrtip',
			scrollX: true,
			"serverSide": true,
			destroy: true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "<?php  echo site_url('ticket/admin/ticket/json'); ?>/"+active_event+"/" ,type: 'POST' },
			columns: [


			{ data: "event_id",name: "event_id"},
			{ data: "image",name: "ticket_image"},
			{ data: "ticket_count",name: "ticket_count"},
			{ data: "ticket_price",name: "ticket_price"},
			{ data: "currency",name: "currency"},
			{ data: "status",name: "ticket_status"},
			{ data: function(data,b,c,table) { 
				var buttons = '';

				buttons += "<button type='button' class='btn bg-grey waves-effect' data-toggle='modal' onclick='ticket_order()'>Issue</button>&nbsp";
				

				return buttons;
			}, name:'action',searchable: false},
			{ data: function(data,b,c,table) { 
				var buttons = '';

				buttons += "<button type='button' class='btn bg-grey waves-effect glyphicon glyphicon-pencil' data-toggle='modal' data-target='#ticket-modal' onclick='edit_ticket("+table.row+")'></button>&nbsp";
				buttons += "<button class='btn bg-red waves-effect glyphicon glyphicon-trash' onclick='removeticket("+data.id+")'></button>";

				return buttons;
			}, name:'action',searchable: false},
			]

		});
		/*Change Image*/
		$("#change_ticket_image").click(function(){
			var filename = $('#upload_ticket_image').val();
			$.post("<?php echo site_url('ticket/admin/ticket/upload_delete')?>", {filename:filename}, function(){
				$("#change_ticket_image").hide();
				$('#ticket_image').show();
				$('#upload_ticket_image').val('');
				$('#upload_ticket_image_name').text('');
				$.notify({
					title: "<strong><?php  echo lang('success')?>:</strong> ",
					message: "Removed previous image !"
				});
			});
		});

	}

	function create_ticket()
	{
		$('#form-ticket')[0].reset();

		$('#ticket-modal').modal('show');
		$('#event_id').val(active_event);
		ticket_uploadReady();

	}
	function edit_ticket(index)
	{
		var row = ticket_dataTable.row(index).data();

		$('#ticket_id').val(row.id);
		$("#form-ticket").find('input:checkbox').prop('checked',false);
		$("#form-ticket").find('input:text,select').val(function(i,v){

			if(row.ticket_status == '1')
			{
				$('input:radio[name=status][id=status_1]').prop('checked',true);
			}else{
				$('input:radio[name=status][id=status_0]').prop('checked',true);
			}
			return row[this.name];
		});
		$('select').selectpicker('render');

		$("#form-ticket").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	
		if(Boolean(row.image))
		{
			$('#upload_ticket_image').val(row.image);
			$('#ticket_image').hide();
			$('#upload_ticket_image_name').html("<img src='"+base_url+"/uploads/ticket/thumb/"+row.image+"' max-width='400px' height='70px'>");
			$('#change_ticket_image').show();
		}
		ticket_uploadReady();
	}
	function removeticket(index)
	{
		$.post("<?php   echo site_url('ticket/admin/ticket/delete_json')?>", {id:[index]}, function(){
			ticket_dataTable.ajax.reload( null, false );
		});
	}

//saving ticket data
function save_ticket()
{
	var validator = $('#form-ticket').validate();
	if(validator.form())
	{
		$.ajax({
			url: '<?php   echo site_url('ticket/admin/ticket/save')?>',
			data: $('#form-ticket').serialize(),
			dataType: 'json',
			success: function(result){
				console.log(result);
				if(result.success)
				{
					$('#ticket-modal').modal('hide');
					$('#form-ticket')[0].reset();
					ticket_dataTable.ajax.reload( null, false );
				}
			},
			type: 'POST'
		});
	}
}
function ticket_uploadReady()
{
	uploader=$('#ticket_image');
	new AjaxUpload(uploader, {
		action: '<?php  echo site_url('ticket/admin/ticket/upload_image')?>',
		name: 'userfile',
		responseType: "json",
		onSubmit: function(file, ext){
			if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
                    $.notify({
                    	title: "<strong><?php  echo lang('error')?>:</strong> ",
                    	message: "Only JPG, PNG or GIF files are allowed"
                    });
                    return false;
                }
				//status.text('Uploading...');
			},
			onComplete: function(file, response){
				if(response.error==null){
					var filename = response.file_name;
					$('#ticket_image').hide();
					$('#upload_ticket_image').val(filename);
					$('#upload_ticket_image_name').html("<img src='"+base_url+"/uploads/ticket/thumb/"+filename+"' max-width='400px' height='70px'>");
					$('#upload_ticket_image_name').show();
					$('#change_ticket_image').show();
				}
				else
				{
					$.notify({
						title: "<strong><?php  echo lang('error')?>:</strong> ",
						message: response.error
					});
				}
			}		
		});	
}	
//ticket order save

function ticket_order()
{
		$('#form-ticket_order')[0].reset();
		$('#order_event_id').val(active_event);
		$('#ticket-order-modal').modal('show');
}
function ticket_order_save()
	{
		var validator = $('#form-ticket_order').validate();
		if(validator.form())
		{
			$.ajax({
				url: '<?php   echo site_url('ticket_order/admin/ticket_order/save')?>',
				
				data: $('#form-ticket_order').serialize(),
				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#ticket_order-modal').modal('hide');
						$('#form-ticket_order')[0].reset();
						
						
					}
				},
				type: 'POST'
			});
		}
	}
</script>
<script type="text/javascript">
	/* Rules for the form*/
	$(function () {
		$('#form-event').validate({
			rules: {
				// 'name': {
				// 	required: true
				// }
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	});

	$(function () {
		$('#form-ticket').validate({
			rules: {
				// 'name': {
				// 	required: true
				// }
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	});
</script>