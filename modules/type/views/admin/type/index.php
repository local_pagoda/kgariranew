<!-- JQuery DataTable Css -->
<link href="<?php  echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?php  echo lang('type')?></h2>
		</div>

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('type')?>
						</h2>
					</div>

					<div class="body">
					</div>
				</div>
			</div>
		</div> <!-- close row clearfix -->

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('type')?>
						</h2>
						<div id="toolbar" style="padding:5px;height:auto">
							<p>
								<a href="#" class="btn bg-green waves-effect" onclick="create()" title="<?php   echo lang('create_type')?>"><?php  echo lang('create')?></a>
								<a href="#" class="btn bg-blue-grey waves-effect" onclick="removeSelected()"  title="<?php   echo lang('delete_type')?>"><?php  echo lang('remove_selected')?></a>
									<select style="background-color: #2b8714;"  name="parent_id" id="select" placeholder="Select Items">
										 <option value="" disabled selected>Select Types</option>
									<?php foreach ($types as $type):  ?>
									
                                        <option value="<?php echo $type['id'] ?>"><?php echo $type['parent_name'] ?></option>
                                        
                                     <?php endforeach; ?>   
                                    </select>
							</p>

						</div>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);"><a href="#" class="btn bg-green waves-effect" onclick="create2()" title="<?php   echo lang('create_type')?>"><?php  echo lang('Type create')?></a></a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<table id="type-table" class="table table-bordered table-striped table-hover dataTable ">
							<thead>
								
<th ><?php echo lang('type')?></th>
<th ><?php echo lang('parent_id')?></th>
<!-- <th ><?php echo lang('parent_name')?></th> -->
<th><?php echo lang('status')?></th>
<!-- <th ><?php echo lang('created_date')?></th>
<th ><?php echo lang('updated_date')?></th>
<th ><?php echo lang('created_by')?></th>
<th ><?php echo lang('updated_by')?></th> -->

								<th>Action</th>

							</thead>

							<tbody>
							</tbody>

						</table>
					</div><!-- close body -->
				</div><!-- close card -->
			</div><!-- close col -->
		</div><!-- close rowclearfix -->

		<!--for create and edit type form-->
		<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none">

			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Add or Edit Members</h4>
					</div>
					<div class="modal-body">
						<form id="form-type" method="post" class="form-horizontal" >

				
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="parent_id"><?php echo lang("parent_id")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line">
									<select class="form-control show-tick" tabindex="-98" name="parent_id">

									<?php foreach ($types as $type):  ?>
									
                                        <option value="<?php echo $type['id'] ?>"><?php echo $type['parent_name'] ?></option>
                                        
                                     <?php endforeach; ?>   
                                    </select>
                                    </div>
						</div>
					</div>
				</div><!-- close row clearfix -->

							<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="type"><?php echo lang("type")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="type" id="type" class="form-control" placeholder="<?php echo lang('type'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->

			
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="status"><?php echo lang("status")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="radio" value="1" name="status" id="status_1" checked /><label for="status_1" ><?php echo lang("general_yes")?></label> 
								<input type="radio" value="0" name="status" id="status_0" /><label for="status_0"><?php echo lang("general_no")?></label></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				
			
				<input type="hidden" name="id" id="id"/>

						</form>
					</div> <!-- end body -->
					<div class="modal-footer">
						<button type="button" class="btn bg-green waves-effect" onClick="save()"><?php  echo  lang('general_save')?></button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
					</div>

				</div><!-- end content -->
			</div><!-- end modal-dialog -->



		</div><!-- end edit modal -->

	<!-- create and edit type -->
<div id="edit-modal2" class="modal fade" tabindex="-1" role="dialog" style="display: none">

			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Add or Edit Types</h4>
					</div>
					<div class="modal-body">
						<form id="form-type1" method="post" class="form-horizontal" >

						
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="parent_name"><?php echo lang("parent_name")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="parent_name" id="parent_name" class="form-control" placeholder="<?php echo lang('parent_name'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="status"><?php echo lang("status")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="radio" value="1" name="status" id="status1" checked/><label for="status1"><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="status" id="status0" /><label for="status0"><?php echo lang("general_no")?></label></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				
				<!-- <input type="hidden" name="id" id=""/> -->
				<input type="hidden" name="parent_id" value ="0" />

						</form>
					</div> <!-- end body -->
					<div class="modal-footer">
						<button type="button" class="btn bg-green waves-effect" onClick="savetype()"><?php  echo  lang('general_save')?></button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
					</div>

				</div><!-- end content -->
			</div><!-- end modal-dialog -->



		</div><!-- end edit modal -->

	</div><!-- end container fluid -->
</section> <!-- end content -->


<!-- Input Mask Plugin Js -->
<script src="<?php  echo base_url(); ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script language="javascript" type="text/javascript">

	var dataTable; 
	$(function(){

		$('.date').inputmask('yyyy-mm-dd', { placeholder: '____-__-__' });
		$('.email').inputmask({ alias: "email" });


	 $('#select').on('change' , function() {
		var id  =  $('#select').val();
		console.log(id);
		dataTable = $('#type-table').DataTable({
			
			destroy: true,dom: 'Bfrtip',
			scrollX: true,
			"serverSide": true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "<?php  echo site_url('type/admin/type/json'); ?>/"+id+"/",type: 'POST' },
			columns: [
			// { data: "id",name: "id"},

				{ data: "type",name: "type"},
				{ data: "parent_id",name: "parent_id"},
				
				{ data: "status",name: "status"},
				// { data: "created_date",name: "created_date"},
				// { data: "updated_date",name: "updated_date"},
				// { data: "created_by",name: "created_by"},
				// { data: "updated_by",name: "updated_by"},
				
			{ data: function(data,b,c,table) { 
				var buttons = '';

				buttons += "<button type='button' class='btn bg-grey waves-effect glyphicon glyphicon-pencil' data-toggle='modal' data-target='#edit-modal' onclick='edit("+table.row+")'></button>&nbsp";
				buttons += "<button class='btn bg-red waves-effect glyphicon glyphicon-trash' onclick='removetype("+data.id+")'></button>";

				return buttons;
			}, name:'action',searchable: false},
			]
		});
	});

	});

	function create(){
		$('#form-type')[0].reset();
		$('#id').val('');
		$('#edit-modal').modal('show');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

		function create2(){
		$('#form-type1')[0].reset();
		$('#id').val('');
		$('#edit-modal2').modal('show');
		//uploadReady(); //Uncomment This function if ajax uploading
	}

	function edit(index)
	{
		var row = dataTable.row(index).data();

		$('#id').val(row.id);
		$("#form-type").find('input:checkbox').prop('checked',false);
		$("#form-type").find('input:text,select').val(function(i,v){

			if(row.status == '1')
			{
				$('input:radio[name=status][id=status_1]').prop('checked',true);
			}else{
				$('input:radio[name=status][id=status_0]').prop('checked',true);
			}
			return row[this.name];
			console.log(row[this.name]);
		});
		$('select').selectpicker('render');

		$("#form-type").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	
	

	}
	function edittype(index){
		$("#form-type1").find('input:checkbox').prop('checked',false);
		$("#form-type1").find('input:text,select').val(function(i,v){

			if(row.status == '1')
			{
				$('input:radio[name=status][id=status1]').prop('checked',true);
			}else{
				$('input:radio[name=status][id=status0]').prop('checked',true);
			}
			return row[this.name];
		});
		$('select').selectpicker('render');

		$("#form-type1").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	
	}	
		
	
	function removetype(index)
	{
		$.post("<?php   echo site_url('type/admin/type/delete_json')?>", {id:[index]}, function(){
			dataTable.ajax.reload( null, false );
		});
	}

	function save()
	{
		var validator = $('#form-type').validate();
		if(validator.form())
		{
			$.ajax({
				url: '<?php   echo site_url('type/admin/type/save')?>',
				data: $('#form-type').serialize(),
				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#edit-modal').modal('hide');
						$('#form-type')[0].reset();
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
		
	}

	function savetype()
	{
		var validator = $('#form-type1').validate();
		if(validator.form())
		{
			$.ajax({
				url: '<?php   echo site_url('type/admin/type/save')?>',
				data: $('#form-type1').serialize(),
				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#edit-modal2').modal('hide');
						$('#form-type1')[0].reset();
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
	}
</script>
<script type="text/javascript">
	/* Rules for the form*/
	$(function () {
		$('#form-type').validate({
			rules: {
				// 'name': {
				// 	required: true
				// }
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	});
</script>