<?php

class Type extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('type','type_model');
        $this->lang->module_load('type','type');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$data['types'] = $this->type_model->getTypes(array('parent_id'=>0, 'status'=>1),null,null,'types.id, types.parent_name')->result_array();

		// Display Page
		$data['header'] = 'type';
		$data['page'] = $this->config->item('template_admin') . "type/index";
		$data['module'] = 'type';
		$this->load->view($this->_container,$data);		
	}

	public function json($id = NULL)
	{
		$where = array();
	
		$params = $this->input->post(); 
		
		$total=$this->type_model->count();

		/*Count for filters*/
		$this->_get_search_param($params);	
		$filter_total=$this->type_model->count();

		/*Filters*/
		$this->_get_search_param($params);	
		
		$rows=$this->type_model->getTypes(array('parent_id'=>$id),NULL,array('limit'=>$params['length'],'offset'=>$params['start']))->result_array();
		echo json_encode(array('draw'=>$params['draw'],'recordsTotal'=>$total,'recordsFiltered'=>$filter_total,'data'=>$rows));
	}
	
	public function _get_search_param($params)
	{

		foreach ($params['columns'] as $value) {
			if($value['searchable'] == 'true'){
				if($params['search']['value'] != '')
				{
					$this->db->or_where(array('types.'.$value["name"].' like'=>'%'.$params['search']['value'].'%'));
				}

				if($value['search']['value'] != '')
				{
					$temp = explode(',', $value['search']['value']);
					$this->db->where_in('types.'.$value['name'],$temp);
				}
			}
		}


		// Search Param Goes Here
		/*
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['type']!='')?$this->db->like('type',$params['search']['type']):'';
($params['search']['parent_id']!='')?$this->db->where('parent_id',$params['search']['parent_id']):'';
($params['search']['parent_name']!='')?$this->db->like('parent_name',$params['search']['parent_name']):'';
(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';
($params['search']['created_by']!='')?$this->db->where('created_by',$params['search']['created_by']):'';
($params['search']['updated_by']!='')?$this->db->where('updated_by',$params['search']['updated_by']):'';

		}  

		
		if(!empty($params['date']))
				{
					foreach($params['date'] as $key=>$value){
						$this->_datewise($key,$value['from'],$value['to']);	
					}
				}
		*/
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->type_model->getTypes()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->type_model->delete('TYPES',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $data['created_date'] = date("Y-m-d H:i:s");
            $data['created_by'] = $this->user_id;
            $success=$this->type_model->insert('TYPES',$data);
        }
        else
        {
            $data['updated_date'] = date("Y-m-d H:i:s");
            $data['updated_by'] =  $this->user_id;
            $success=$this->type_model->update('TYPES',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
   		
       if($this->input->post('id')){
   			 $data['id'] = $this->input->post('id');
   		}
$data['type'] = $this->input->post('type');
$data['parent_id'] = $this->input->post('parent_id');
$data['parent_name'] = $this->input->post('parent_name');
$data['status'] = $this->input->post('status');



        return array_filter($data,function($value){
        	return ($value !== '' && $value !== false && $value !== NULL);
        });
   }
   
   	
	    
}