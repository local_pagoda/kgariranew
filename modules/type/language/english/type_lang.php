<?php


$lang['id'] = 'Id';
$lang['type'] = 'Type';
$lang['parent_id'] = 'Parent Id';
$lang['parent_name'] = 'Parent Name';
$lang['status'] = 'Status';
$lang['created_date'] = 'Created Date';
$lang['updated_date'] = 'Updated Date';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';

$lang['create_type']='Create Type';
$lang['edit_type']='Edit Type';
$lang['delete_type']='Delete Type';
$lang['type_search']='Type Search';

$lang['type']='Type';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

