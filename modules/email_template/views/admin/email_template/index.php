<!-- JQuery DataTable Css -->
<link href="<?php  echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

</style>
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?php  echo lang('email_template')?></h2>
		</div>

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('email_template')?>
						</h2>
					</div>

					<div class="body">
					</div>
				</div>
			</div>
		</div> <!-- close row clearfix -->

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('email_template')?>
						</h2>
						<div id="toolbar" style="padding:5px;height:auto">
							<p>
								<!-- <a href="#" class="btn bg-green waves-effect" onclick="create()" title="<?php   echo lang('create_email_template')?>"><?php  echo lang('create')?></a> -->
								<a href="#" class="btn bg-blue-grey waves-effect" onclick="removeSelected()"  title="<?php   echo lang('delete_email_template')?>"><?php  echo lang('remove_selected')?></a>
							</p>

						</div>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<table id="email_template-table" class="table table-bordered table-striped table-hover dataTable " >
							<thead>

								<th ><?php echo lang('name'); ?></th>
								<th ><?php echo lang('slug_name'); ?></th>
								<th ><?php echo lang('subject'); ?></th>
								<th  ><?php echo lang('ckeditor'); ?></th>




								<th>Action</th>

							</thead>

							<tbody>
							</tbody>

						</table>
					</div><!-- close body -->
				</div><!-- close card -->
			</div><!-- close col -->
		</div><!-- close rowclearfix -->

		<!--for create and edit email_template form-->
		<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none; width: 100%;">

			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Add or Edit Members</h4>
					</div>
					<div class="modal-body">
						<form id="form-email_template" method="post" class="form-horizontal" >
							<div class="row clearfix" style="display: none;">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="name"><?php echo lang("name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="text" name="name" id="name" class="form-control" placeholder="<?php echo lang('name'); ?>"></div>
									</div>
								</div>
							</div><!-- close row clearfix -->
							<div class="row clearfix" style="display: none;">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="slug_name"><?php echo lang("slug_name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="text" name="slug_name" id="slug_name" class="form-control" placeholder="<?php echo lang('slug_name'); ?>"></div>
									</div>
								</div>
							</div><!-- close row clearfix -->


							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="subject"><?php echo lang("subject")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="text" name="subject" id="subject" class="form-control" placeholder="<?php echo lang('subject'); ?>"></div>
									</div>
								</div>
							</div><!-- close row clearfix -->

							<!-- CKEditor -->
							<div class="row clearfix" style="margin-top: 20px">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="ckeditor"><?php echo lang("ckeditor")?></label>
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="card">

										<div class="body">
											<textarea id="ckeditor" name="ckeditor">

											</textarea>
										</div>
									</div>
								</div>
							</div>
							<!-- #END# CKEditor -->

							<input type="hidden" name="email_template_id" id="email_template_id"/>

						</form>
					</div> <!-- end body -->
					<div class="modal-footer">
						<button type="button" class="btn bg-green waves-effect" onClick="save()"><?php  echo  lang('general_save')?></button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
					</div>

				</div><!-- end content -->
			</div><!-- end modal-dialog -->



		</div><!-- end edit modal -->

	</div><!-- end container fluid -->
</section> <!-- end content -->


<!-- Input Mask Plugin Js -->
<script src="<?php  echo base_url(); ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script language="javascript" type="text/javascript">

	var dataTable; 
	$(function(){

		$('.date').inputmask('yyyy-mm-dd', { placeholder: '____-__-__' });
		$('.email').inputmask({ alias: "email" });

		dataTable = $('#email_template-table').DataTable({
			dom: 'Bfrtip',
			scrollX: true,
			"serverSide": true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "<?php  echo site_url('email_template/admin/email_template/json'); ?>",type: 'POST' },
			columns: [
			
			{ data: "name",name: "name"},
			{ data: "slug_name",name: "slug_name"},
			{ data: "subject",name: "subject"},
			{ data: "body",name: "ckeditor"},


			{ data: function(data,b,c,table) { 
				var buttons = '';

				buttons += "<button type='button' class='btn bg-grey waves-effect glyphicon glyphicon-pencil' data-toggle='modal' data-target='#edit-modal' onclick='edit("+table.row+")'></button>&nbsp";
				buttons += "<button class='btn bg-red waves-effect glyphicon glyphicon-trash' onclick='removeemail_template("+data.email_template_id+")'></button>";

				return buttons;
			}, name:'action',searchable: false},
			],
			"columnDefs": [
            {
                "targets": [ 3 ],
                "visible": false,
                "searchable": false
            }
           
			]

		});

	});

	function create(){
		$('#form-email_template')[0].reset();
		$('#email_template_id').val('');
		$('#edit-modal').modal('show');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = dataTable.row(index).data();
	

		$('#email_template_id').val(row.email_template_id);
		$("#form-email_template").find('input:checkbox').prop('checked',false);
		$("#form-email_template").find('input:text,select').val(function(i,v){

		CKEDITOR.instances['ckeditor'].setData(row.body);
			/*if(row.gender == 'M')
			{
				$('input:radio[name=gender][id=radio_1]').prop('checked',true);
			}else{
				$('input:radio[name=gender][id=radio_2]').prop('checked',true);
			}*/
			return row[this.name];
		});
		$('select').selectpicker('render');

		$("#form-email_template").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	
	}

	function removeemail_template(index)
	{
		$.post("<?php   echo site_url('email_template/admin/email_template/delete_json')?>", {id:[index]}, function(){
			dataTable.ajax.reload( null, false );
		});
	}

	function save()
	{
		var validator = $('#form-email_template').validate();
		if(validator.form())
		{
			 var data = CKEDITOR.instances.ckeditor.getData();
			 var formdata =  $('#form-email_template').serialize();
			 console.log(data);
			 console.log(formdata);
			$.ajax({
				url: '<?php   echo site_url('email_template/admin/email_template/save')?>',
				data: {bodydata:data,formdata},

				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#edit-modal').modal('hide');
						$('#form-email_template')[0].reset();
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
	}
</script>
<script type="text/javascript">
	/* Rules for the form*/
	$(function () {
		$('#form-email_template').validate({
			rules: {
				// 'name': {
				// 	required: true
				// }
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	});
</script>