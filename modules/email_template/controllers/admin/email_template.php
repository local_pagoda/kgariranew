<?php

class Email_template extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('email_template','email_template_model');
        $this->lang->module_load('email_template','email_template');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'email_template';
		$data['page'] = $this->config->item('template_admin') . "email_template/index";
		$data['module'] = 'email_template';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$where = array();
		$params = $this->input->post(); 

		$total=$this->email_template_model->count();

		/*Count for filters*/
		$this->_get_search_param($params);	
		$filter_total=$this->email_template_model->count();

		/*Filters*/
		$this->_get_search_param($params);	
		
		$rows=$this->email_template_model->getEmailTemplates(NULL,NULL,array('limit'=>$params['length'],'offset'=>$params['start']))->result_array();
 
		echo json_encode(array('draw'=>$params['draw'],'recordsTotal'=>$total,'recordsFiltered'=>$filter_total,'data'=>$rows));
	}
	
	public function _get_search_param($params)
	{

		foreach ($params['columns'] as $value) {
			if($value['searchable'] == 'true'){
				if($params['search']['value'] != '')
				{
					$this->db->or_where(array('email_templates.'.$value["name"].' like'=>'%'.$params['search']['value'].'%'));
				}

				if($value['search']['value'] != '')
				{
					$temp = explode(',', $value['search']['value']);
					$this->db->where_in('email_templates.'.$value['name'],$temp);
				}
			}
		}


		// Search Param Goes Here
		/*
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['name']!='')?$this->db->like('name',$params['search']['name']):'';
($params['search']['slug_name']!='')?$this->db->like('slug_name',$params['search']['slug_name']):'';
($params['search']['subject']!='')?$this->db->like('subject',$params['search']['subject']):'';

		}  

		
		if(!empty($params['date']))
				{
					foreach($params['date'] as $key=>$value){
						$this->_datewise($key,$value['from'],$value['to']);	
					}
				}
		*/
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->email_template_model->getEmailTemplates()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->email_template_model->delete('EMAIL_TEMPLATES',array('email_template_id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
		
		
     	$data=$this->_get_posted_data(); //Retrive Posted Data	
 
        if(!$data['email_template_id'])
        {
            $data['created_date'] = date("Y-m-d H:i:s");
        	// $data['created_by'] = $this->user_id;
            $success=$this->email_template_model->insert('EMAIL_TEMPLATES',$data);
        }
        else
        {
            $success=$this->email_template_model->update('EMAIL_TEMPLATES',$data,array('email_template_id'=>$data['email_template_id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		// $data=array();
   		// if($this->input->post('email_template_id')){
   		// 	$data['email_template_id'] = $this->input->post('email_template_id');
   		// }
parse_str($this->input->post('formdata'), $searcharray);
if($searcharray['email_template_id'])
    	{
    		$data['email_template_id'] = $searcharray['email_template_id'];
    	}              
$data['name'] = $searcharray['name'];
// $data['slug_name'] = $searcharray['slug_name'];
$data['subject'] = $searcharray['subject'];
$data['body'] = $this->input->post('bodydata');


        return $data;
   }
   
   	
	    
}