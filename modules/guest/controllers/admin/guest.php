<?php

class Guest extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('guest','guest_model');
        $this->lang->module_load('guest','guest');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$data['countries'] = $this->db->get('mst_countries')->result_array();
		
		// Display Page
		$data['header'] = 'guest';
		$data['page'] = $this->config->item('template_admin') . "guest/index";
		$data['module'] = 'guest';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$where = array();
		$params = $this->input->post(); 

		$total=$this->guest_model->count();

		/*Count for filters*/
		$this->_get_search_param($params);	
		$filter_total=$this->guest_model->count();

		/*Filters*/
		$this->_get_search_param($params);	
		
		$rows=$this->guest_model->getGuests(NULL,NULL,array('limit'=>$params['length'],'offset'=>$params['start']))->result_array();
		echo json_encode(array('draw'=>$params['draw'],'recordsTotal'=>$total,'recordsFiltered'=>$filter_total,'data'=>$rows));
	}
	
	public function _get_search_param($params)
	{

		foreach ($params['columns'] as $value) {
			if($value['searchable'] == 'true'){
				if($params['search']['value'] != '')
				{
					$this->db->or_where(array('guests.'.$value["name"].' like'=>'%'.$params['search']['value'].'%'));
				}

				if($value['search']['value'] != '')
				{
					$temp = explode(',', $value['search']['value']);
					$this->db->where_in('guests.'.$value['name'],$temp);
				}
			}
		}


		// Search Param Goes Here
		/*
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['promoter_id']!='')?$this->db->where('promoter_id',$params['search']['promoter_id']):'';
($params['search']['firstname']!='')?$this->db->like('firstname',$params['search']['firstname']):'';
($params['search']['lastname']!='')?$this->db->like('lastname',$params['search']['lastname']):'';
($params['search']['guest_email']!='')?$this->db->like('guest_email',$params['search']['guest_email']):'';
($params['search']['counrty_code']!='')?$this->db->like('counrty_code',$params['search']['counrty_code']):'';
($params['search']['mesage']!='')?$this->db->like('mesage',$params['search']['mesage']):'';
($params['search']['ip_address']!='')?$this->db->like('ip_address',$params['search']['ip_address']):'';
(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';
($params['search']['created_by']!='')?$this->db->where('created_by',$params['search']['created_by']):'';
($params['search']['updated_by']!='')?$this->db->where('updated_by',$params['search']['updated_by']):'';

		}  

		
		if(!empty($params['date']))
				{
					foreach($params['date'] as $key=>$value){
						$this->_datewise($key,$value['from'],$value['to']);	
					}
				}
		*/
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->guest_model->getGuests()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->guest_model->delete('GUESTS',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
              $data['created_date'] = date("Y-m-d H:i:s");
            $data['created_by'] = $this->user_id;
            $success=$this->guest_model->insert('GUESTS',$data);
        }
        else
        {
           $data['updated_date'] = date("Y-m-d H:i:s");
            $data['updated_by'] =  $this->user_id;
            $success=$this->guest_model->update('GUESTS',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        
$data['promoter_id'] = $this->input->post('promoter_id');
$data['firstname'] = $this->input->post('firstname');
$data['lastname'] = $this->input->post('lastname');
$data['guest_email'] = $this->input->post('guest_email');
$data['counrty_code'] = $this->input->post('counrty_code');
$data['mesage'] = $this->input->post('mesage');
$data['ip_address'] = $this->input->post('ip_address');
$data['status'] = $this->input->post('status');

        return $data;
   }
   
   	
	    
}