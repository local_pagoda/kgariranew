<?php

class Blog extends Admin_Controller
{
	protected $uploadPath = 'uploads/blog';
protected $uploadthumbpath= 'uploads/blog/thumb/';

	public function __construct(){
    	parent::__construct();
        $this->load->module_model('blog','blog_model');
        $this->lang->module_load('blog','blog');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		$data['blog_types'] = $this->db->get_where('mst_types', array('parent_id' => 4))->result_array();
		
		// Display Page
		$data['header'] = 'blog';
		$data['page'] = $this->config->item('template_admin') . "blog/index";
		$data['module'] = 'blog';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$where = array();
		$params = $this->input->post(); 

		$total=$this->blog_model->count();

		/*Count for filters*/
		$this->_get_search_param($params);	
		$filter_total=$this->blog_model->count();

		/*Filters*/
		$this->_get_search_param($params);	
		
		$rows=$this->blog_model->getBlogs(NULL,NULL,array('limit'=>$params['length'],'offset'=>$params['start']))->result_array();
		echo json_encode(array('draw'=>$params['draw'],'recordsTotal'=>$total,'recordsFiltered'=>$filter_total,'data'=>$rows));
	}
	
	public function _get_search_param($params)
	{

		foreach ($params['columns'] as $value) {
			if($value['searchable'] == 'true'){
				if($params['search']['value'] != '')
				{
					$this->db->or_where(array('blogs.'.$value["name"].' like'=>'%'.$params['search']['value'].'%'));
				}

				if($value['search']['value'] != '')
				{
					$temp = explode(',', $value['search']['value']);
					$this->db->where_in('blogs.'.$value['name'],$temp);
				}
			}
		}


		// Search Param Goes Here
		/*
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['title']!='')?$this->db->like('title',$params['search']['title']):'';
($params['search']['type_id']!='')?$this->db->where('type_id',$params['search']['type_id']):'';

		}  

		
		if(!empty($params['date']))
				{
					foreach($params['date'] as $key=>$value){
						$this->_datewise($key,$value['from'],$value['to']);	
					}
				}
		*/
		               
        
	}

		
    
	public function combo_json()
    {
		$rows=$this->blog_model->getBlogs()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->blog_model->delete('BLOGS',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->blog_model->insert('BLOGS',$data);
        }
        else
        {
            $success=$this->blog_model->update('BLOGS',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		parse_str($this->input->post('formdata'), $searcharray);
print_r($this->input->post('bodydata'));

   		print_r($searcharray);
   		exit;
   		if($searcharray['id'])
   		{
   			echo "nasjkfnks";
   		}
$data['title'] = $this->input->post('title');
$data['body'] = $this->input->post('body');
$data['type_id'] = $this->input->post('type_id');
$data['image'] = $this->input->post('image');

        return $data;
   }
   
      function upload_image(){
		//Image Upload Config
		$config['upload_path'] = $this->uploadPath;
		$config['allowed_types'] = 'gif|png|jpg';
		$config['max_size']	= '10240';
		$config['remove_spaces']  = true;
		//load upload library
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload())
		{
			$data['error'] = $this->upload->display_errors('','');
			echo json_encode($data);
		}
		else
		{
		  $data = $this->upload->data();
 		  $config['image_library'] = 'gd2';
		  $config['source_image'] = $data['full_path'];
          $config['new_image']    = $this->uploadthumbpath;
		  //$config['create_thumb'] = TRUE;
		  $config['maintain_ratio'] = TRUE;
		  $config['height'] =100;
		  $config['width'] = 100;

		  $this->load->library('image_lib', $config);
		  $this->image_lib->resize();
		  echo json_encode($data);
	    }
	}
	
	function upload_delete(){
		//get filename
		$filename = $this->input->post('filename');
		@unlink($this->uploadPath . '/' . $filename);
	} 	
	    
}