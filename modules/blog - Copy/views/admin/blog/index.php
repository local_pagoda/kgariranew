<!-- JQuery DataTable Css -->
<link href="<?php  echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?php  echo lang('blog')?></h2>
		</div>

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('blog')?>
						</h2>
					</div>

					<div class="body">
					</div>
				</div>
			</div>
		</div> <!-- close row clearfix -->

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('blog')?>
						</h2>
						<div id="toolbar" style="padding:5px;height:auto">
							<p>
								<a href="#" class="btn bg-green waves-effect" onclick="create()" title="<?php   echo lang('create_blog')?>"><?php  echo lang('create')?></a>
								<a href="#" class="btn bg-blue-grey waves-effect" onclick="removeSelected()"  title="<?php   echo lang('delete_blog')?>"><?php  echo lang('remove_selected')?></a>
							</p>

						</div>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<table id="blog-table" class="table table-bordered table-striped table-hover dataTable " >
							<thead>
								
								<th ><?php echo lang('title')?></th>
								<th ><?php echo lang('type_id')?></th>
								<th ><?php echo lang('image')?></th>
								<th ><?php echo lang('body')?></th>


								<th>Action</th>

							</thead>

							<tbody>
							</tbody>

						</table>
					</div><!-- close body -->
				</div><!-- close card -->
			</div><!-- close col -->
		</div><!-- close rowclearfix -->

		<!--for create and edit blog form-->
		<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none">

			<div class="modal-dialog" role="document">
				<div class="modal-content modal-lg">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Add or Edit Blogs</h4>
					</div>
					<div class="modal-body">
						<form id="form-blog" method="post" class="form-horizontal" enctype="multipart/form-data" >

							<div class="row">
							<div class="col-md-6">
								<div class="row clearfix">
									<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
										<label for="title"><?php echo lang("title")?></label>
									</div>
									<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line"><input type="text" name="title" id="title" class="form-control" placeholder="<?php echo lang('title'); ?>"></div>
										</div>
									</div>
								</div><!-- close row clearfix -->
							</div>

							<div class="col-md-6">
								<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="type_id"><?php echo lang("type_name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">

											<select style="background-color: #2b8714;"  name="type_id" id="select" placeholder="Select Items">

												<?php foreach ($blog_types as $blog):  ?>



													<option value="<?php echo $blog['id']; ?>"><?php echo $blog['type']; ?></option>

												<?php endforeach; ?>   
											</select>
										</div>
									</div>
								</div>
							</div><!-- close row clearfix -->


							</div>
								
							</div>	
						
								<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="image"><?php echo lang("image")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">

											<label id="upload_image_name" ></label>
											<input type="file" name="image" id="image" class="form-control" placeholder="<?php echo lang('image'); ?>">
											<input type="text" name="image" id="upload_image" hidden>

											<button type="button" class="btn btn-default waves-effect" id="change_image" title="Change Image" style="display: none;" > <i class="material-icons">switch_camera</i>Change Image </button>

										</div>
									</div>
								</div>
							</div><!-- close row clearfix -->
							<!-- CKEditor -->
							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="ckeditor"><?php echo lang("body")?></label>
								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="card">

										<div class="body">
											<textarea id="ckeditor" name="ckeditor">

											</textarea>
										</div>
									</div>
								</div>
							</div>
							<!-- #END# CKEditor -->

							<input type="hidden" name="id" id="id"/>

						</form>
					</div> <!-- end body -->
					<div class="modal-footer">
						<button type="button" class="btn bg-green waves-effect" onClick="save()"><?php  echo  lang('general_save')?></button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
					</div>

				</div><!-- end content -->
			</div><!-- end modal-dialog -->



		</div><!-- end edit modal -->

	</div><!-- end container fluid -->
</section> <!-- end content -->


<!-- Input Mask Plugin Js -->
<script src="<?php  echo base_url(); ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script language="javascript" type="text/javascript">
	var base_url = '<?php echo base_url(); ?>';

	var dataTable; 
	$(function(){

		$('.date').inputmask('yyyy-mm-dd', { placeholder: '____-__-__' });
		$('.email').inputmask({ alias: "email" });

		dataTable = $('#blog-table').DataTable({
			dom: 'Bfrtip',
			scrollX: true,
			"serverSide": true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "<?php  echo site_url('blog/admin/blog/json'); ?>",type: 'POST' },
			columns: [
			
			{ data: "title",name: "title"},
			{ data: "ckeditor",name: "ckeditor"},
			{ data: "type_id",name: "type_id"},
			{ data: "image",name: "image"},

			{ data: function(data,b,c,table) { 
				var buttons = '';

				buttons += "<button type='button' class='btn bg-grey waves-effect' data-toggle='modal' data-target='#edit-modal' onclick='edit("+table.row+")'>Edit</button>&nbsp";
				buttons += "<button class='btn bg-red waves-effect' onclick='removeblog("+data.id+")'>Delete</button>";

				return buttons;
			}, name:'action',searchable: false},
			],
			"columnDefs": [
            {
                "targets": [ 3 ],
                "visible": false,
                "searchable": false
            }
           
			]

		});
		/*Change Image*/
		$("#change_image").click(function(){
			var filename = $('#upload_image').val();
			$.post("<?php echo site_url('blog/admin/blog/upload_delete')?>", {filename:filename}, function(){
				$("#change_image").hide();
				$('#image').show();
				$('#upload_image').val('');
				$('#upload_image_name').text('');
				$.notify({
					title: "<strong><?php  echo lang('success')?>:</strong> ",
					message: "Removed previous image !"
				});
			});
		});
	});

	function create(){
		$('#form-blog')[0].reset();
		$('#id').val('');
		$('#edit-modal').modal('show');
		uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = dataTable.row(index).data();

		$('#id').val(row.id);
		$("#form-blog").find('input:checkbox').prop('checked',false);
		$("#form-blog").find('input:text,select').val(function(i,v){

			/*if(row.gender == 'M')
			{
				$('input:radio[name=gender][id=radio_1]').prop('checked',true);
			}else{
				$('input:radio[name=gender][id=radio_2]').prop('checked',true);
			}*/
			return row[this.name];
		});
		$('select').selectpicker('render');

		$("#form-blog").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	

		if(Boolean(row.image))
		{
			$('#upload_image').val(row.image);
			$('#image').hide();
			$('#upload_image_name').html("<img src='"+base_url+"/uploads/blog/thumb/"+row.image+"' max-width='400px' height='70px'>");
			$('#change_image').show();
		}
		uploadReady();
	}

	function removeblog(index)
	{
		$.post("<?php   echo site_url('blog/admin/blog/delete_json')?>", {id:[index]}, function(){
			dataTable.ajax.reload( null, false );
		});
	}

	function save()
	{
		var validator = $('#form-blog').validate();

		if(validator.form())
		{
			var data = CKEDITOR.instances.ckeditor.getData();
			var formdata =$('#form-blog').serialize();
			$.ajax({
				url: '<?php   echo site_url('blog/admin/blog/save')?>',
				data: {bodydata:data,formdata},

				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#edit-modal').modal('hide');
						$('#form-blog')[0].reset();
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
	}
	function uploadReady()
	{
		uploader=$('#image');
		new AjaxUpload(uploader, {
			action: '<?php  echo site_url('blog/admin/blog/upload_image')?>',
			name: 'userfile',
			responseType: "json",
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
                    $.notify({
                    	title: "<strong><?php  echo lang('error')?>:</strong> ",
                    	message: "Only JPG, PNG or GIF files are allowed"
                    });
                    return false;
                }
				//status.text('Uploading...');
			},
			onComplete: function(file, response){
				if(response.error==null){
					var filename = response.file_name;
					$('#image').hide();
					$('#upload_image').val(filename);
					$('#upload_image_name').html("<img src='"+base_url+"/uploads/blog/thumb/"+filename+"' max-width='400px' height='70px'>");
					$('#upload_image_name').show();
					$('#change_image').show();
				}
				else
				{
					$.notify({
						title: "<strong><?php  echo lang('error')?>:</strong> ",
						message: response.error
					});
				}
			}		
		});	
	}	
</script>
<script type="text/javascript">
	/* Rules for the form*/
	$(function () {
		$('#form-blog').validate({
			rules: {
				// 'name': {
				// 	required: true
				// }
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	});
</script>