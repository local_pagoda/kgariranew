<?php


$lang['id'] = 'Id';
$lang['event_id'] = 'Event Id';
$lang['ticket_order_id'] = 'Ticket Order Id';
$lang['code'] = 'Code';
$lang['status'] = 'Status';
$lang['created_date'] = 'Created Date';
$lang['updated_date'] = 'Updated Date';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';

$lang['create_ticket_code']='Create Ticket Code';
$lang['edit_ticket_code']='Edit Ticket Code';
$lang['delete_ticket_code']='Delete Ticket Code';
$lang['ticket_code_search']='Ticket Code Search';

$lang['ticket_code']='Ticket Code';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

