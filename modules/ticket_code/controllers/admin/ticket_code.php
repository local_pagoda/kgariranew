<?php

class Ticket_code extends Admin_Controller
{
	
	public function __construct(){
    	parent::__construct();
        $this->load->module_model('ticket_code','ticket_code_model');
        $this->lang->module_load('ticket_code','ticket_code');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
    
	public function index()
	{
		// Display Page
		$data['header'] = 'ticket_code';
		$data['page'] = $this->config->item('template_admin') . "ticket_code/index";
		$data['module'] = 'ticket_code';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$where = array();
		$params = $this->input->post(); 

		$total=$this->ticket_code_model->count();

		/*Count for filters*/
		$this->_get_search_param($params);	
		$filter_total=$this->ticket_code_model->count();

		/*Filters*/
		$this->_get_search_param($params);	
		
		$rows=$this->ticket_code_model->getTicketCodes(NULL,NULL,array('limit'=>$params['length'],'offset'=>$params['start']))->result_array();
		echo json_encode(array('draw'=>$params['draw'],'recordsTotal'=>$total,'recordsFiltered'=>$filter_total,'data'=>$rows));
	}
	
	public function _get_search_param($params)
	{

		foreach ($params['columns'] as $value) {
			if($value['searchable'] == 'true'){
				if($params['search']['value'] != '')
				{
					$this->db->or_where(array('ticket_codes.'.$value["name"].' like'=>'%'.$params['search']['value'].'%'));
				}

				if($value['search']['value'] != '')
				{
					$temp = explode(',', $value['search']['value']);
					$this->db->where_in('ticket_codes.'.$value['name'],$temp);
				}
			}
		}


		// Search Param Goes Here
		/*
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['event_id']!='')?$this->db->where('event_id',$params['search']['event_id']):'';
($params['search']['ticket_order_id']!='')?$this->db->where('ticket_order_id',$params['search']['ticket_order_id']):'';
($params['search']['code']!='')?$this->db->like('code',$params['search']['code']):'';
($params['search']['status']!='')?$this->db->like('status',$params['search']['status']):'';
($params['search']['created_by']!='')?$this->db->like('created_by',$params['search']['created_by']):'';
($params['search']['updated_by']!='')?$this->db->like('updated_by',$params['search']['updated_by']):'';

		}  

		
		if(!empty($params['date']))
				{
					foreach($params['date'] as $key=>$value){
						$this->_datewise($key,$value['from'],$value['to']);	
					}
				}
		*/
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->ticket_code_model->getTicketCodes()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->ticket_code_model->delete('TICKET_CODES',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $success=$this->ticket_code_model->insert('TICKET_CODES',$data);
        }
        else
        {
            $success=$this->ticket_code_model->update('TICKET_CODES',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			$msg=lang('success_message'); 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   {
   		$data=array();
        $data['id'] = $this->input->post('id');
$data['event_id'] = $this->input->post('event_id');
$data['ticket_order_id'] = $this->input->post('ticket_order_id');
$data['code'] = $this->input->post('code');
$data['status'] = $this->input->post('status');
$data['created_date'] = $this->input->post('created_date');
$data['updated_date'] = $this->input->post('updated_date');
$data['created_by'] = $this->input->post('created_by');
$data['updated_by'] = $this->input->post('updated_by');

        return $data;
   }
   
   	
	    
}