<?php


$lang['id'] = 'Id';
$lang['banner_image'] = 'Banner Image';
$lang['event_id'] = 'Event Id';
$lang['event_name'] = 'Event Name';

$lang['status'] = 'Status';
$lang['created_date'] = 'Created Date';
$lang['created_by'] = 'Created By';
$lang['updated_date'] = 'Updated Date';
$lang['updated_by'] = 'Updated By';

$lang['create_banner']='Create Banner';
$lang['edit_banner']='Edit Banner';
$lang['delete_banner']='Delete Banner';
$lang['banner_search']='Banner Search';

$lang['banner']='Banner';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

