<!-- JQuery DataTable Css -->
<link href="<?php  echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?php  echo lang('banner')?></h2>
		</div>

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('banner')?>
						</h2>
					</div>

					<div class="body">
					</div>
				</div>
			</div>
		</div> <!-- close row clearfix -->

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('banner')?>
						</h2>
						<div id="toolbar" style="padding:5px;height:auto">
							<p>
								<a href="#" class="btn bg-green waves-effect" onclick="create()" title="<?php   echo lang('create_banner')?>"><?php  echo lang('create')?></a>
								<a href="#" class="btn bg-blue-grey waves-effect" onclick="removeSelected()"  title="<?php   echo lang('delete_banner')?>"><?php  echo lang('remove_selected')?></a>
							</p>

						</div>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<table id="banner-table" class="table table-bordered table-striped table-hover dataTable " >
							<thead>
								
								<th ><?php echo lang('banner_image')?></th>
								<th ><?php echo lang('event_id')?></th>
								<th><?php echo lang('status')?></th>


								<th>Action</th>

							</thead>

							<tbody>
							</tbody>

						</table>
					</div><!-- close body -->
				</div><!-- close card -->
			</div><!-- close col -->
		</div><!-- close rowclearfix -->

		<!--for create and edit banner form-->
		<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none">

			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Add or Edit Members</h4>
					</div>
					<div class="modal-body">
						<form id="form-banner" method="post" class="form-horizontal" enctype="multipart/form-data" >

							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="event_id"><?php echo lang("event_name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">

											<select class="form-control"  name="event_id" id="select" placeholder="Select Items">

												<?php foreach ($events as $event):  ?>


													<option value="<?php echo $event['id']; ?>"><?php echo $event['name']; ?></option>

												<?php endforeach; ?>   
											</select>
										</div>
									</div>
								</div>
							</div><!-- close row clearfix -->
							
						
							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="banner_image"><?php echo lang("banner_image")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">

											<label id="upload_image_name" ></label>
											<input type="file" name="banner_image" id="banner_image" class="form-control" placeholder="<?php echo lang('image'); ?>">
											<input type="text" name="banner_image" id="upload_image" hidden>

											<button type="button" class="btn btn-default waves-effect" id="change_image" title="Change Image" style="display: none;" > <i class="material-icons">switch_camera</i>Change Image </button>

										</div>
									</div>
								</div>
							</div><!-- close row clearfix -->

							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="status"><?php echo lang("status")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="radio" value="1" name="status" id="status1" checked/><label for="status1"><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="status" id="status0" /><label for="status0"><?php echo lang("general_no")?></label></div>
									</div>
								</div>
							</div><!-- close row clearfix -->

							<input type="hidden" name="id" id="id"/>

						</form>
					</div> <!-- end body -->
					<div class="modal-footer">
						<button type="button" class="btn bg-green waves-effect" onClick="save()"><?php  echo  lang('general_save')?></button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
					</div>

				</div><!-- end content -->
			</div><!-- end modal-dialog -->



		</div><!-- end edit modal -->

	</div><!-- end container fluid -->
</section> <!-- end content -->


<!-- Input Mask Plugin Js -->
<script src="<?php  echo base_url(); ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script language="javascript" type="text/javascript">
	var base_url = '<?php echo base_url(); ?>';

	var dataTable; 
	$(function(){

		$('.date').inputmask('yyyy-mm-dd', { placeholder: '____-__-__' });
		$('.email').inputmask({ alias: "email" });

		dataTable = $('#banner-table').DataTable({
			dom: 'Bfrtip',
			scrollX: true,
			"serverSide": true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "<?php  echo site_url('banner/admin/banner/json'); ?>",type: 'POST' },
			columns: [

			{ data: "banner_image",name: "banner_image"},
			{ data: "event_id",name: "event_id"},
			{ data: "status",name: "status"},
			

			{ data: function(data,b,c,table) { 
				var buttons = '';

				buttons += "<button type='button' class='btn bg-grey waves-effect glyphicon glyphicon-pencil' data-toggle='modal' data-target='#edit-modal' onclick='edit("+table.row+")'></button>&nbsp";
				buttons += "<button class='btn bg-red waves-effect glyphicon glyphicon-trash' onclick='removebanner("+data.id+")'></button>";

				return buttons;
			}, name:'action',searchable: false},
			]
		});
			/*Change Image*/
		$("#change_image").click(function(){
			var filename = $('#upload_image').val();
			$.post("<?php echo site_url('banner/admin/banner/upload_delete')?>", {filename:filename}, function(){
				$("#change_image").hide();
				$('#banner_image').show();
				$('#upload_image').val('');
				$('#upload_image_name').text('');
				$.notify({
					title: "<strong><?php  echo lang('success')?>:</strong> ",
					message: "Removed previous image !"
				});
			});
		});

	});

	function create(){
		$('#form-banner')[0].reset();
		$('#id').val('');
		$('#edit-modal').modal('show');
		uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = dataTable.row(index).data();

		$('#id').val(row.id);
		$("#form-banner").find('input:checkbox').prop('checked',false);
		$("#form-banner").find('input:text,select').val(function(i,v){

			if(row.status == '1')
			{
				$('input:radio[name=status][id=status1]').prop('checked',true);
			}else{
				$('input:radio[name=status][id=status0]').prop('checked',true);
			}
			return row[this.name];
		});
		$('select').selectpicker('render');

		$("#form-banner").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	
		if(Boolean(row.image))
		{
			$('#upload_image').val(row.image);
			$('#banner_image').hide();
			$('#upload_image_name').html("<img src='"+base_url+"/uploads/banner/thumb/"+row.image+"' max-width='400px' height='70px'>");
			$('#change_image').show();
		}
		uploadReady();
	}

	function removebanner(index)
	{
		$.post("<?php   echo site_url('banner/admin/banner/delete_json')?>", {id:[index]}, function(){
			dataTable.ajax.reload( null, false );
		});
	}

	function save()
	{
		var validator = $('#form-banner').validate();
		if(validator.form())
		{
			$.ajax({
				url: '<?php   echo site_url('banner/admin/banner/save')?>',
				data: $('#form-banner').serialize(),
				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#edit-modal').modal('hide');
						$('#form-banner')[0].reset();
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
	}
	function uploadReady()
	{
		uploader=$('#banner_image');
		new AjaxUpload(uploader, {
			action: '<?php  echo site_url('banner/admin/banner/upload_image')?>',
			name: 'userfile',
			responseType: "json",
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
                    $.notify({
                    	title: "<strong><?php  echo lang('error')?>:</strong> ",
                    	message: "Only JPG, PNG or GIF files are allowed"
                    });
                    return false;
                }
				//status.text('Uploading...');
			},
			onComplete: function(file, response){
				if(response.error==null){
					var filename = response.file_name;
					$('#banner_image').hide();
					$('#upload_image').val(filename);
					$('#upload_image_name').html("<img src='"+base_url+"/uploads/banner/thumb/"+filename+"' max-width='400px' height='70px'>");
					$('#upload_image_name').show();
					$('#change_image').show();
				}
				else
				{
					$.notify({
						title: "<strong><?php  echo lang('error')?>:</strong> ",
						message: response.error
					});
				}
			}		
		});	
	}	
</script>
<script type="text/javascript">
	/* Rules for the form*/
	$(function () {
		$('#form-banner').validate({
			rules: {
				// 'name': {
				// 	required: true
				// }
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	});
</script>