<?php


$lang['id'] = 'Id';
$lang['title'] = 'Title';
$lang['body'] = 'Body';
$lang['type_id'] = 'Type Id';
$lang['type_name'] = 'Type Name';

$lang['image'] = 'Image';
$lang['status'] = 'Status';
$lang['created_date'] = 'Created Date';
$lang['created_by'] = 'Created By';
$lang['updated_date'] = 'Updated Date';
$lang['updated_by'] = 'Updated By';

$lang['create_blog']='Create Blog';
$lang['edit_blog']='Edit Blog';
$lang['delete_blog']='Delete Blog';
$lang['blog_search']='Blog Search';

$lang['blog']='Blog';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

