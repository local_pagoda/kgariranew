<!-- JQuery DataTable Css -->
<link href="<?php  echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link href="<?php  echo base_url(); ?>assets/css/dropzone.css" rel="stylesheet" />


<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?php  echo lang('gallery')?></h2>
		</div>

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('gallery')?>
						</h2>
					</div>

					<div class="body">
					</div>
				</div>
			</div>
		</div> <!-- close row clearfix -->

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('gallery')?>
						</h2>
						<div id="toolbar" style="padding:5px;height:auto">
							<p>
								<a href="#" class="btn bg-green waves-effect" onclick="create()" title="<?php   echo lang('create_gallery')?>"><?php  echo lang('create')?></a>
								<a href="#" class="btn bg-blue-grey waves-effect" onclick="removeSelected()"  title="<?php   echo lang('delete_gallery')?>"><?php  echo lang('remove_selected')?></a>
							</p>

						</div>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<table id="gallery-table" class="table table-bordered table-striped table-hover dataTable " >
							<thead>
								
<th ><?php echo lang('event_id')?></th>
<th ><?php echo lang('album_name')?></th>

<th><?php echo lang('status')?></th>

<th>Add Featured Image</th>
<th>Add Gallery</th>
<th>Action</th>
								
							</thead>

							<tbody>
							</tbody>

						</table>
					</div><!-- close body -->
				</div><!-- close card -->
			</div><!-- close col -->
		
		</div><!-- close rowclearfix -->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Upload Gallery</h4>
			  </div>
			  <div class="modal-body">
			   <form action="<?php   echo site_url('gallery/admin/gallery/upload_gallery')?>" id="dropzone" class="dropzone">
					<input type="hidden" name="gallery_id" id="gallery_id">
			   </form>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
		
		<div id="featured" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-lg">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Upload Featured Gallery</h4>
			  </div>
			  <div class="modal-body">
 				<div class="row" id="modal_image">
			     <!-- <?php foreach ($images as $image) {  ?>
			    
			     		<div class="col-md-4"><img src="<?php echo base_url();  ?>uploads/gallery/thumb/<?php echo $image['image_name']; ?>"></div>
			     		
			    
			   <?php  } ?>
 -->

			     	</div>		   
			  </div>
			</div>

		  </div>
		</div>
		<!--for create and edit gallery form-->
		<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none">

			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Add or Edit Members</h4>
					</div>
					<div class="modal-body">
					
						<form id="form-gallery" method="post" class="form-horizontal" >

						<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="event_id"><?php echo lang("event_name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
										<select style="background-color: #2b8714;"  name="event_id" id="select" placeholder="Select Items">
										
									<?php foreach ($events as $event):  ?>
									
                                        <option value="<?php echo $event['id'] ?>"><?php echo $event['name'] ?></option>
                                        
                                     <?php endforeach; ?>   
                                    </select>
                                    </div>
									</div>
								</div>
							</div><!-- close row clearfix -->
			
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="album_name"><?php echo lang("album_name")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="album_name" id="album_name" class="form-control" placeholder="<?php echo lang('album_name'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
			
					<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="status"><?php echo lang("status")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="radio" value="1" name="status" id="status1" checked /><label for="status1"><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="status" id="status0" /><label for="status0"><?php echo lang("general_no")?></label></div>
									</div>
								</div>
							</div><!-- close row clearfix -->
				
				<input type="hidden" name="id" id="id"/>

						</form>
					</div> <!-- end body -->
					<div class="modal-footer">
						<button type="button" class="btn bg-green waves-effect" onClick="save()"><?php  echo  lang('general_save')?></button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
					</div>

				</div><!-- end content -->
			</div><!-- end modal-dialog -->



		</div><!-- end edit modal -->

	</div><!-- end container fluid -->
</section> <!-- end content -->


<!-- Input Mask Plugin Js -->
<script src="<?php  echo base_url(); ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script language="javascript" type="text/javascript">

	var dataTable; 
	var base_url = '<?php echo base_url(); ?>';

	$(function(){

		$('.date').inputmask('yyyy-mm-dd', { placeholder: '____-__-__' });
		$('.email').inputmask({ alias: "email" });

		dataTable = $('#gallery-table').DataTable({
			dom: 'Bfrtip',
			scrollX: true,
			"serverSide": true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "<?php  echo site_url('gallery/admin/gallery/json'); ?>",type: 'POST' },
			columns: [

		



		
				{ data: "event_id",name: "event_id"},
			
				{ data: "album_name",name: "album_name"},
			
				{ data: "status",name: "status"},
			

			{ data: function(data,b,c,table) { 
				var buttons = '';
				buttons += "<button class='btn btn-info waves-effect glyphicon glyphicon-picture' onclick='showgallery("+table.row+")'>&nbsp;Featured Image</button>";
				return buttons;
			}, name:'gallery',searchable: false},


			{ data: function(data,b,c,table) { 
				var buttons = '';
				buttons += "<button class='btn btn-info waves-effect glyphicon glyphicon-picture' onclick='insertgallery("+table.row+")'>&nbsp;Add Image</button>";
				return buttons;
			}, name:'gallery',searchable: false},
			
				
			{ data: function(data,b,c,table) { 
				var buttons = '';

				buttons += "<button type='button' class='btn bg-grey waves-effect glyphicon glyphicon-pencil' data-toggle='modal' data-target='#edit-modal' onclick='edit("+table.row+")'></button>&nbsp";
				buttons += "<button class='btn bg-red waves-effect glyphicon glyphicon-trash' onclick='removegallery("+data.id+")'></button>";
				// buttons += "<button class='btn bg-red waves-effect' onclick='insertgallery()'>Add Image</button>";
				return buttons;
			}, name:'action',searchable: false},
	

			]
		});

	});

	function create(){
		$('#form-gallery')[0].reset();
		$('#id').val('');
		$('#edit-modal').modal('show');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = dataTable.row(index).data();

		$('#id').val(row.id);
		$("#form-gallery").find('input:checkbox').prop('checked',false);
		$("#form-gallery").find('input:text,select').val(function(i,v){

			if(row.status == '1')
			{
				$('input:radio[name=status1][id=radio_1]').prop('checked',true);
			}else{
				$('input:radio[name=status0][id=radio_2]').prop('checked',true);
			}
			return row[this.name];
		});
		$('select').selectpicker('render');

		$("#form-gallery").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	
	}
	function insertgallery(index){
		var row = dataTable.row(index).data();

		$('#dropzone')[0].reset();
		$('#gallery_id').val(row.id);
		$('#myModal').modal('show');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	
	//show all image in particular album
	function showgallery(index){
		var row = dataTable.row(index).data();
        $.post("<?php  echo site_url('gallery/admin/gallery/getimage')?>", {id: row.id}, function(data){
        	$.each(data, function( index, value ) {
			 
        		$("#modal_image").append("<div class='col-md-4'><img src='"+base_url+"/uploads/gallery/thumb/"+value.image_name+"'height='70px' onclick=make_feature("+value.id+")></div>");
			});
		},'json');
		
		$('#featured').modal('show');
		//uploadReady(); //Uncomment This function if ajax uploading
	}	

	function make_feature(id){
		var gallery_id = id;
		alert(gallery_id);
		$.post("<?php   echo site_url('gallery/admin/gallery/make_feature_image')?>", {id: gallery_id }, function(){
			dataTable.ajax.reload( null, false );
		});
	}

	function removegallery(index)
	{
		$.post("<?php   echo site_url('gallery/admin/gallery/delete_json')?>", {id:[index]}, function(){
			dataTable.ajax.reload( null, false );
		});
	}

	function save()
	{
		var validator = $('#form-gallery').validate();
		if(validator.form())
		{
			$.ajax({
				url: '<?php   echo site_url('gallery/admin/gallery/save')?>',
				data: $('#form-gallery').serialize(),
				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#edit-modal').modal('hide');
						$('#form-gallery')[0].reset();
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
	}

/*	function save2()
	{
		var validator = $('#form-drop').validate();
		if(validator.form())
		{
			$.ajax({
				url: '',
				data: $('#form-gallery').serialize(),
				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#edit-modal').modal('hide');
						$('#form-gallery')[0].reset();
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
	}*/
</script>
<script type="text/javascript" src="<?php  echo base_url(); ?>assets/js/dropzone.js" ></script>
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> -->
<script type="text/javascript">
	/* Rules for the form*/
	$(function () {
		$('#form-gallery').validate({
			rules: {
				// 'name': {
				// 	required: true
				// }
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	});
</script>