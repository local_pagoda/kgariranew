<?php


$lang['id'] = 'Id';
$lang['event_id'] = 'Event Id';
$lang['event_name'] = 'Event';
$lang['gallery_title'] = 'Gallery Title';
$lang['image_name'] = 'Image Name';
$lang['album_name'] = 'Album Name';
$lang['country_code'] = 'Country Code';
$lang['gallery_image_id'] = 'Gallery Image Id';
$lang['fb_gallery_id'] = 'Fb Gallery Id';
$lang['fb_gallery_image'] = 'Fb Gallery Image';
$lang['status'] = 'Status';
$lang['created_date'] = 'Created Date';
$lang['updated_date'] = 'Updated Date';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';

$lang['create_gallery']='Create Gallery';
$lang['edit_gallery']='Edit Gallery';
$lang['delete_gallery']='Delete Gallery';
$lang['gallery_search']='Gallery Search';

$lang['gallery']='Gallery';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

