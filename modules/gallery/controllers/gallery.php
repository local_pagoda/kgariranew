<?php

class Gallery extends Public_Controller
{
	protected $uploadPath = 'uploads/gallery/';
	protected $uploadthumbpath= 'uploads/gallery/thumb/';

	public function __construct(){
		parent::__construct();
		$this->load->module_model('gallery','gallery_model');
		$this->lang->module_load('gallery','gallery');

		$this->load->module_model('event','event_model');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
	}

	public function index(){
	    

	    $this->event_model->_table = "viewtbl_gallery";
        $this->db->order_by('gallery_id desc');
        $this->db->group_by('event_id');
        $data['galleries'] = $this->event_model->getEvents()->result_array();
        // print_r($data['galleries']);exit;
        // $data['galleries'] = $this->db->get('viewtbl_gallery')->result_array();

        
     	$data['header'] = "Gallery";
		$data['view_page'] = 'gallery/index';
		$this->load->view($this->_container,$data);
	}
}