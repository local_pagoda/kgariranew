<?php

class Gallery extends Admin_Controller
{
	protected $uploadPath = 'uploads/gallery/';
	protected $uploadthumbpath= 'uploads/gallery/thumb/';

	public function __construct(){
		parent::__construct();
		$this->load->module_model('gallery','gallery_model');
		$this->lang->module_load('gallery','gallery');

		$this->load->module_model('event','event_model');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
	}

	public function index()
	{
		$data['events'] = $this->db->get('tbl_events')->result_array();

		// Display Page
		$data['header'] = 'gallery';
		$data['page'] = $this->config->item('template_admin') . "gallery/index";
		$data['module'] = 'gallery';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$where = array();
		$params = $this->input->post(); 

		$total=$this->gallery_model->count();

		/*Count for filters*/
		$this->_get_search_param($params);	
		$filter_total=$this->gallery_model->count();

		/*Filters*/
		$this->_get_search_param($params);	
		
		$rows=$this->gallery_model->getGalleries(NULL,NULL,array('limit'=>$params['length'],'offset'=>$params['start']))->result_array();
		echo json_encode(array('draw'=>$params['draw'],'recordsTotal'=>$total,'recordsFiltered'=>$filter_total,'data'=>$rows));
	}
	
	public function _get_search_param($params)
	{

		foreach ($params['columns'] as $value) {
			if($value['searchable'] == 'true'){
				if($params['search']['value'] != '')
				{
					$this->db->or_where(array('galleries.'.$value["name"].' like'=>'%'.$params['search']['value'].'%'));
				}

				if($value['search']['value'] != '')
				{
					$temp = explode(',', $value['search']['value']);
					$this->db->where_in('galleries.'.$value['name'],$temp);
				}
			}
		}


		// Search Param Goes Here
		/*
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['event_id']!='')?$this->db->where('event_id',$params['search']['event_id']):'';
($params['search']['gallery_title']!='')?$this->db->like('gallery_title',$params['search']['gallery_title']):'';
($params['search']['album_name']!='')?$this->db->like('album_name',$params['search']['album_name']):'';
($params['search']['country_code']!='')?$this->db->like('country_code',$params['search']['country_code']):'';
($params['search']['fb_gallery_id']!='')?$this->db->where('fb_gallery_id',$params['search']['fb_gallery_id']):'';
($params['search']['status']!='')?$this->db->where('status',$params['search']['status']):'';
($params['search']['created_by']!='')?$this->db->like('created_by',$params['search']['created_by']):'';
($params['search']['updated_by']!='')?$this->db->like('updated_by',$params['search']['updated_by']):'';

		}  

		
		if(!empty($params['date']))
				{
					foreach($params['date'] as $key=>$value){
						$this->_datewise($key,$value['from'],$value['to']);	
					}
				}
		*/


			}


			private function _datewise($field,$from,$to)
			{
				if(!empty($from) && !empty($to))
				{
					$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
				}
				else if(!empty($from))
				{
					$this->db->like($field,date('Y-m-d',strtotime($from)));				
				}		
			}	

			public function combo_json()
			{
				$rows=$this->gallery_model->getGalleries()->result_array();
				echo json_encode($rows);    	
			}    

			public function delete_json()
			{
				$id=$this->input->post('id');

				if($id && is_array($id))
				{
					foreach($id as $row):
						$this->gallery_model->delete('GALLERY',array('id'=>$row));
					endforeach;
				}
			}   
			public function getimage()
			{
				$id=$this->input->post('id');
		
				$this->db->from("tbl_gallery_image");
				$this->db->where('gallery_id',$id);
				$images = $this->db->get()->result_array();
				
				echo json_encode($images);
				
			} 

			public function make_feature_image()
			{
				$id=$this->input->post('id');	
				$success = false;
				if($id != NULL){
					$data['featured'] = 1;
					$this->db->where('id', $id);
					$success = $this->db->update('tbl_gallery_image',$data);
				}
				echo json_encode($success);
			}

			public function save()
			{

        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
        	$data['created_date'] = date("Y-m-d H:i:s");
        	$data['created_by'] = $this->user_id;
        	$success=$this->gallery_model->insert('GALLERY',$data);
        }
        else
        {

        	$data['updated_date'] = date('Y-m-d H:i:s');
        	$data['updated_by'] = $this->user_id;

        	$success=$this->gallery_model->update('GALLERY',$data,array('id'=>$data['id']));
        }
        
        if($success)
        {
        	$success = TRUE;
        	$msg=lang('success_message'); 
        } 
        else
        {
        	$success = FALSE;
        	$msg=lang('failure_message');
        }

        echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
    }


    public function upload_gallery()
    {


				//Image Upload Config
    	$config['upload_path'] = $this->uploadPath;
    	$config['allowed_types'] = 'gif|png|jpg';
    	$config['max_size']	= '10240';
    	$config['encrypt_name']	= true;
    	$config['remove_spaces']  = true;
		//load upload library
    	$this->load->library('upload', $config);
    	if(!$this->upload->do_upload('file'))
    	{
    		$data['error'] = $this->upload->display_errors('','');
    		echo json_encode($data);
    		exit;
    	}

    	$data = $this->upload->data();
    	$config['image_library'] = 'gd2';
    	$config['source_image'] = $data['full_path'];
    	$config['new_image']    = $this->uploadthumbpath;
		  // $config['maintain_ratio'] = TRUE;
    	$config['height'] =100;
    	$config['width'] = 100;

    	$this->load->library('image_lib', $config);
    	$this->image_lib->resize();

    	// echo json_encode($data);

    	$input = array(

    		'image_name' => $data['file_name'] ,
    		'gallery_id' => $this->input->post('gallery_id'),
    		);
    	$this->db->insert('tbl_gallery_image', $input); 

    }



    private function _get_posted_data()
    {
    	$data=array();
    	if($this->input->post('id'))
    	{
    		$data['id'] = $this->input->post('id');
    	}        
    	$data['event_id'] = $this->input->post('event_id');


    	$data['album_name'] = $this->input->post('album_name');

    	$data['status'] = $this->input->post('status');


    	return $data;
    }

    function upload_image(){
		//Image Upload Config
    	$config['upload_path'] = $this->uploadPath;
    	$config['allowed_types'] = 'gif|png|jpg';
    	$config['max_size']	= '10240';
    	$config['remove_spaces']  = true;
		//load upload library
    	$this->load->library('upload', $config);
    	if(!$this->upload->do_upload())
    	{
    		$data['error'] = $this->upload->display_errors('','');
    		echo json_encode($data);
    	}
    	else
    	{
    		$data = $this->upload->data();
    		$config['image_library'] = 'gd2';
    		$config['source_image'] = $data['full_path'];
    		$config['new_image']    = $this->uploadthumbpath;
		  //$config['create_thumb'] = TRUE;
    		$config['maintain_ratio'] = TRUE;
    		$config['height'] =100;
    		$config['width'] = 100;

    		$this->load->library('image_lib', $config);
    		$this->image_lib->resize();
    		echo json_encode($data);
    	}
    }

    function upload_delete(){
		//get filename
    	$filename = $this->input->post('filename');
    	@unlink($this->uploadPath . '/' . $filename);
    } 	

}