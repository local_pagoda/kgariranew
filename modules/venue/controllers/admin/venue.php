 <?php

class Venue extends Admin_Controller
{
	protected $uploadPath = 'uploads/venue';
protected $uploadthumbpath= 'uploads/venue/thumb/';

	public function __construct(){
    	parent::__construct();
        $this->load->module_model('venue','venue_model');
        $this->lang->module_load('venue','venue');
        $this->load->module_model('type','type_model');
        //$this->bep_assets->load_asset('jquery.upload'); // uncomment if image ajax upload
    }
     
	public function index()
	{
		$data['venues'] = $this->db->get_where('mst_types', array('parent_id' => 3))->result_array();
				$data['countries'] = $this->db->get('mst_countries')->result_array();
		// Display Page
		$data['header'] = 'venue';
		$data['page'] = $this->config->item('template_admin') . "venue/index";
		$data['module'] = 'venue';
		$this->load->view($this->_container,$data);		
	}

	public function json()
	{
		$where = array();
		$params = $this->input->post(); 

		$total=$this->venue_model->count();

		/*Count for filters*/
		$this->_get_search_param($params);	
		$filter_total=$this->venue_model->count();

		/*Filters*/
		$this->_get_search_param($params);	
		
		$rows=$this->venue_model->getVenues(NULL,NULL,array('limit'=>$params['length'],'offset'=>$params['start']))->result_array();
	
		echo json_encode(array('draw'=>$params['draw'],'recordsTotal'=>$total,'recordsFiltered'=>$filter_total,'data'=>$rows));
	}
	
	public function _get_search_param($params)
	{

		foreach ($params['columns'] as $value) {
			if($value['searchable'] == 'true'){
				if($params['search']['value'] != '')
				{
					$this->db->or_where(array('venues.'.$value["name"].' like'=>'%'.$params['search']['value'].'%'));
				}

				if($value['search']['value'] != '')
				{
					$temp = explode(',', $value['search']['value']);
					$this->db->where_in('venues.'.$value['name'],$temp);
				}
			}
		}


		// Search Param Goes Here
		/*
		parse_str($this->input->post('data'),$params);
		if(!empty($params['search']))
		{
			($params['search']['venue_name']!='')?$this->db->like('venue_name',$params['search']['venue_name']):'';
($params['search']['venue_type_id']!='')?$this->db->where('venue_type_id',$params['search']['venue_type_id']):'';
($params['search']['venue_location']!='')?$this->db->like('venue_location',$params['search']['venue_location']):'';
($params['search']['venue_city']!='')?$this->db->like('venue_city',$params['search']['venue_city']):'';
($params['search']['venue_longitude']!='')?$this->db->where('venue_longitude',$params['search']['venue_longitude']):'';
($params['search']['venue_latitude']!='')?$this->db->where('venue_latitude',$params['search']['venue_latitude']):'';
($params['search']['cusine']!='')?$this->db->like('cusine',$params['search']['cusine']):'';
($params['search']['venue_drink']!='')?$this->db->like('venue_drink',$params['search']['venue_drink']):'';
($params['search']['venue_food']!='')?$this->db->like('venue_food',$params['search']['venue_food']):'';
($params['search']['food_price_range']!='')?$this->db->like('food_price_range',$params['search']['food_price_range']):'';
($params['search']['drink_price_range']!='')?$this->db->like('drink_price_range',$params['search']['drink_price_range']):'';
($params['search']['country_code']!='')?$this->db->like('country_code',$params['search']['country_code']):'';
(isset($params['search']['status']))?$this->db->where('status',$params['search']['status']):'';
($params['search']['created_by']!='')?$this->db->where('created_by',$params['search']['created_by']):'';
($params['search']['updated_by']!='')?$this->db->where('updated_by',$params['search']['updated_by']):'';

		}  

		
		if(!empty($params['date']))
				{
					foreach($params['date'] as $key=>$value){
						$this->_datewise($key,$value['from'],$value['to']);	
					}
				}
		*/
		               
        
	}

	
	private function _datewise($field,$from,$to)
	{
			if(!empty($from) && !empty($to))
			{
				$this->db->where("(date_format(".$field.",'%Y-%m-%d') between '".date('Y-m-d',strtotime($from)).
						"' and '".date('Y-m-d',strtotime($to))."')");
			}
			else if(!empty($from))
			{
				$this->db->like($field,date('Y-m-d',strtotime($from)));				
			}		
	}	
    
	public function combo_json()
    {
		$rows=$this->venue_model->getVenues()->result_array();
		echo json_encode($rows);    	
    }    
    
	public function delete_json()
	{
    	$id=$this->input->post('id');
		if($id && is_array($id))
		{
        	foreach($id as $row):
				$this->venue_model->delete('VENUES',array('id'=>$row));
            endforeach;
		}
	}    

	public function save()
	{
		
        $data=$this->_get_posted_data(); //Retrive Posted Data		

        if(!$this->input->post('id'))
        {
            $data['created_date'] = date("Y-m-d H:i:s");
            $data['created_by'] = $this->user_id;
            $success=$this->venue_model->insert('VENUES',$data);
        }
        else
        {
            $data['updated_date'] = date("Y-m-d H:i:s");
            $data['updated_by'] =  $this->user_id;
            $success=$this->venue_model->update('VENUES',$data,array('id'=>$data['id']));
        }
        
		if($success)
		{
			$success = TRUE;
			// $msg=lang('success_message'); 
			$msg='success'; 
		} 
		else
		{
			$success = FALSE;
			$msg=lang('failure_message');
		}
		 
		 echo json_encode(array('msg'=>$msg,'success'=>$success));		
        
	}
   
   private function _get_posted_data()
   { 
   		$data=array();
   		
   		if($this->input->post('id')){
   			 $data['id'] = $this->input->post('id');
   		}
       
		$data['venue_name'] = $this->input->post('venue_name');
		$data['venue_type_id'] = $this->input->post('venue_type_id');
		$data['venue_location'] = $this->input->post('venue_location');
		$data['venue_city'] = $this->input->post('venue_city');
		$data['venue_description'] = $this->input->post('venue_description');
		$data['venue_longitude'] = $this->input->post('venue_longitude');
		$data['venue_latitude'] = $this->input->post('venue_latitude');
		$data['cusine'] = $this->input->post('cusine');
		$data['venue_drink'] = $this->input->post('venue_drink');
		$data['venue_food'] = $this->input->post('venue_food');
		$data['food_price_range'] = $this->input->post('food_price_range');
		$data['drink_price_range'] = $this->input->post('drink_price_range');
		$data['venue_image'] = $this->input->post('venue_image');
		$data['country_code'] = $this->input->post('country_code');
		$data['status'] = $this->input->post('status');




        return $data;
   }
   
      function upload_image(){
		//Image Upload Config
		$config['upload_path'] = $this->uploadPath;
		$config['allowed_types'] = 'gif|png|jpg';
		$config['max_size']	= '10240';
		$config['remove_spaces']  = true;
		//load upload library
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload())
		{
			$data['error'] = $this->upload->display_errors('','');
			echo json_encode($data);
		}
		else
		{
		  $data = $this->upload->data();
 		  $config['image_library'] = 'gd2';
		  $config['source_image'] = $data['full_path'];
          $config['new_image']    = $this->uploadthumbpath;
		  //$config['create_thumb'] = TRUE;
		  $config['maintain_ratio'] = TRUE;
		  $config['height'] =100;
		  $config['width'] = 100;

		  $this->load->library('image_lib', $config);
		  $this->image_lib->resize();
		  echo json_encode($data);
	    }
	}
	
	function upload_delete(){
		//get filename
		$filename = $this->input->post('filename');
		@unlink($this->uploadPath . '/' . $filename);
	} 	
	    
}