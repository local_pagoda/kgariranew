<!-- JQuery DataTable Css -->
<link href="<?php  echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<!-- <script src="http://maps.google.com/maps/api/js?sensor=false&.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script> -->
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?php  echo lang('venue')?></h2>
		</div>

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('venue')?>
						</h2>
					</div>

					<div class="body">
					</div>
				</div>
			</div>
		</div> <!-- close row clearfix -->

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('venue')?>
						</h2>
						<div id="toolbar" style="padding:5px;height:auto">
							<p>
								<a href="#" class="btn bg-green waves-effect" onclick="create()" title="<?php   echo lang('create_venue')?>"><?php  echo lang('create')?></a>
								<a href="#" class="btn bg-blue-grey waves-effect" onclick="removeSelected()"  title="<?php   echo lang('delete_venue')?>"><?php  echo lang('remove_selected')?></a>
							</p>

						</div>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<table id="venue-table" class="table table-bordered table-striped table-hover dataTable " >
							<thead>
						
<th ><?php echo lang('venue_name')?></th>
<th ><?php echo lang('venue_type_id')?></th>
<th ><?php echo lang('venue_location')?></th>
<th ><?php echo lang('venue_city')?></th>
<th ><?php echo lang('venue_description')?></th>

<th ><?php echo lang('venue_longitude')?></th>
<th ><?php echo lang('venue_latitude')?></th>
<th ><?php echo lang('cusine')?></th>
<th ><?php echo lang('venue_drink')?></th>
<th ><?php echo lang('venue_food')?></th>
<th ><?php echo lang('food_price_range')?></th>
<th ><?php echo lang('drink_price_range')?></th>
<th ><?php echo lang('venue_image')?></th>
<th ><?php echo lang('country_code')?></th>
<th><?php echo lang('status')?></th>


								<th>Action</th>

							</thead>

							<tbody>
							</tbody>

						</table>
					</div><!-- close body -->
				</div><!-- close card -->
			</div><!-- close col -->
		</div><!-- close rowclearfix -->

		<!--for create and edit venue form-->
		<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none">

			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Add or Edit Members</h4>
					</div>
					<div class="modal-body">
						<form id="form-venue" method="post" class="form-horizontal" >

							<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="venue_name"><?php echo lang("venue_name")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="venue_name" id="venue_name" class="form-control" placeholder="<?php echo lang('venue_name'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="venue_type_id"><?php echo lang("venue_type")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line">
									<select style="background-color: #2b8714;"  name="venue_type_id" id="select">
										
									<?php foreach ($venues as $venue):  ?>
									
                                        <option value="<?php echo $venue['id'] ?>"><?php echo $venue['type'] ?></option>
                                        
                                     <?php endforeach; ?>   
                                    </select>
								</div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="venue_location"><?php echo lang("venue_location")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="venue_location" id="venue_location" class="form-control" placeholder="<?php echo lang('venue_location'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="venue_city"><?php echo lang("venue_city")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="venue_city" id="venue_city" class="form-control" placeholder="<?php echo lang('venue_city'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="venue_description"><?php echo lang("venue_description")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><textarea name="venue_description" id="venue_description" class="form-control" ></textarea></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="venue_longitude"><?php echo lang("venue_longitude")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="venue_longitude" id="venue_longitude" class="form-control number" placeholder="<?php echo lang('venue_longitude'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="venue_latitude"><?php echo lang("venue_latitude")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="venue_latitude" id="venue_latitude" class="form-control number" placeholder="<?php echo lang('venue_latitude'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="cusine"><?php echo lang("cusine")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="cusine" id="cusine" class="form-control" placeholder="<?php echo lang('cusine'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="venue_drink"><?php echo lang("venue_drink")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="venue_drink" id="venue_drink" class="form-control" placeholder="<?php echo lang('venue_drink'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="venue_food"><?php echo lang("venue_food")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="venue_food" id="venue_food" class="form-control" placeholder="<?php echo lang('venue_food'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="food_price_range"><?php echo lang("food_price_range")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="food_price_range" id="food_price_range" class="form-control" placeholder="<?php echo lang('food_price_range'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="drink_price_range"><?php echo lang("drink_price_range")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="text" name="drink_price_range" id="drink_price_range" class="form-control" placeholder="<?php echo lang('drink_price_range'); ?>"></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
			

				<div class="row clearfix">
									<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
										<label for="image"><?php echo lang("venue_image")?></label>
									</div>
									<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
										<div class="form-group">
											<div class="form-line">

												<label id="upload_image_name" ></label>
												<input type="file" name="image" id="venue_image" class="form-control" placeholder="<?php echo lang('venue_image'); ?>">
												<input type="text" name="venue_image" id="upload_image" hidden>

												<button type="button" class="btn btn-default waves-effect" id="change_image" title="Change Image" style = "display : none"> <i class="material-icons">switch_camera</i>Change Image </button>

											</div>
										</div>
									</div>
								</div><!-- close row clearfix -->
							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="country_code"><?php echo lang("country_name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
										<select style="background-color: #2b8714;"  name="country_code" id="select" placeholder="Select Items">
										
									<?php foreach ($countries as $country):  ?>
									
                                        <option value="<?php echo $country['country_code']; ?>"> <?php echo $country['country_name']; ?></option>
                                        
                                     <?php endforeach; ?>   
                                    </select>
                                    </div>
									</div>
								</div>
							</div><!-- close row clearfix -->
				<div class="row clearfix">
						<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
							<label for="status"><?php echo lang("status")?></label>
						</div>
						<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
							<div class="form-group">
								<div class="form-line"><input type="radio" value="1" name="status" id="status1" checked /><label for="status1"><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="status" id="status0" /><label for="status0"><?php echo lang("general_no")?></label></div>
						</div>
					</div>
				</div><!-- close row clearfix -->
				
				<input type="hidden" name="id" id="id"/>

						</form>
					</div> <!-- end body -->
					<div class="modal-footer">
						<button type="button" class="btn bg-green waves-effect" onClick="save()"><?php  echo  lang('general_save')?></button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
					</div>

				</div><!-- end content -->
			</div><!-- end modal-dialog -->



		</div><!-- end edit modal -->

	</div><!-- end container fluid -->
</section> <!-- end content -->


<!-- Input Mask Plugin Js -->
<script src="<?php  echo base_url(); ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script language="javascript" type="text/javascript">
   var base_url= '<?php echo base_url(); ?>';
	var dataTable; 
	$(function(){

		$('.date').inputmask('yyyy-mm-dd', { placeholder: '____-__-__' });
		$('.email').inputmask({ alias: "email" });

		dataTable = $('#venue-table').DataTable({
			dom: 'Bfrtip',
			scrollX: true,
			"serverSide": true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "<?php  echo site_url('venue/admin/venue/json'); ?>",type: 'POST' },
			columns: [
											

				{ data: "venue_name",name: "venue_name"},
				{ data: "venue_type_id",name: "venue_type_id"},
				{ data: "venue_location",name: "venue_location"},
				{ data: "venue_city",name: "venue_city"},
				{ data: "venue_description",name: "venue_description"},
				{ data: "venue_longitude",name: "venue_longitude"},
				{ data: "venue_latitude",name: "venue_latitude"},
				{ data: "cusine",name: "cusine"},
				{ data: "venue_drink",name: "venue_drink"},
				{ data: "venue_food",name: "venue_food"},
				{ data: "food_price_range",name: "food_price_range"},
				{ data: "drink_price_range",name: "drink_price_range"},
				{ data: "venue_image",name: "venue_image"},
				{ data: "country_code",name: "country_code"},
				{ data: "status",name: "status"},
				
			{ data: function(data,b,c,table) { 
				var buttons = '';

				buttons += "<button type='button' class='btn bg-grey waves-effect glyphicon glyphicon-pencil' data-toggle='modal' data-target='#edit-modal' onclick='edit("+table.row+")'></button>&nbsp";
				buttons += "<button class='btn bg-red waves-effect glyphicon glyphicon-trash' onclick='removevenue("+data.id+")'></button>";

				return buttons;
			}, name:'action',searchable: false},
			]
		});

			/*Change Image*/
			$("#change_image").click(function(){
				var filename = $('#upload_image').val();
				$.post("<?php echo site_url('venue/admin/venue/upload_delete')?>", {filename:filename}, function(){
					$("#change_image").hide();
					$('#venue_image').show();
					$('#upload_image').val('');
					$('#upload_image_name').text('');
					$.notify({
						title: "<strong><?php  echo lang('success')?>:</strong> ",
						message: "Removed previous image !"
					});
				});
			});


	});

	function create(){
		$('#form-venue')[0].reset();
		$('#id').val('');
		$('#edit-modal').modal('show');
		uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = dataTable.row(index).data();

		$('#id').val(row.id);
		$("#form-venue").find('input:checkbox').prop('checked',false);
		$("#form-venue").find('input:text,select').val(function(i,v){

			if(row.status == '1')
			{
				$('input:radio[name=status1][id=radio_1]').prop('checked',true);
			}else{
				$('input:radio[name=status0][id=radio_2]').prop('checked',true);
			}
			return row[this.name];
		});
		$('select').selectpicker('render');

		$("#form-venue").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	

		if(Boolean(row.image))
		{
			$('#upload_image').val(row.image);
			$('#venue_image').hide();
			$('#upload_image_name').html("<img src='"+base_url+"/uploads/venue/thumb/"+row.image+"' max-width='400px' height='70px'>");
			$('#change_image').sstyle = "display : none"
		}
		uploadReady();
	}

	function removevenue(index)
	{
		$.post("<?php   echo site_url('venue/admin/venue/delete_json')?>", {id:[index]}, function(){
			dataTable.ajax.reload( null, false );
		});
	}

	function save()
	{
		var validator = $('#form-venue').validate();
		if(validator.form())
		{
			$.ajax({
				url: '<?php   echo site_url('venue/admin/venue/save')?>',
				data: $('#form-venue').serialize(),
				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#edit-modal').modal('hide');
						$('#form-venue')[0].reset();
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
	}

	function uploadReady()
	{
		uploader=$('#venue_image');
		new AjaxUpload(uploader, {
			action: '<?php  echo site_url('venue/admin/venue/upload_image')?>',
			name: 'userfile',
			responseType: "json",
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
                    $.notify({
                    	title: "<strong><?php  echo lang('error')?>:</strong> ",
                    	message: "Only JPG, PNG or GIF files are allowed"
                    });
                    return false;
                }
				//status.text('Uploading...');
			},
			onComplete: function(file, response){
				if(response.error==null){
					var filename = response.file_name;
					$('#venue_image').hide();
					$('#upload_image').val(filename);
					$('#upload_image_name').html("<img src='"+base_url+"/uploads/venue/thumb/"+filename+"' max-width='400px' height='70px'>");
					$('#upload_image_name').sstyle = "display : none"
					$('#change_image').show();
				}
				else
				{
					$.notify({
						title: "<strong><?php  echo lang('error')?>:</strong> ",
						message: response.error
					});
				}
			}		
		});		
	}
</script>
<script type="text/javascript">
	/* Rules for the form*/
	$(function () {
		$('#form-venue').validate({
			rules: {
				// 'name': {
				// 	required: true
				// }
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	});
</script>