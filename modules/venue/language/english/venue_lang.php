<?php


$lang['id'] = 'Id';
$lang['venue_name'] = 'Venue Name';
$lang['venue_type_id'] = 'Venue Type Id';
$lang['venue_type'] = 'Venue Type';
$lang['venue_location'] = 'Venue Location';
$lang['venue_city'] = 'Venue City';
$lang['venue_description'] = 'Venue Description';
$lang['venue_longitude'] = 'Venue Longitude';
$lang['venue_latitude'] = 'Venue Latitude';
$lang['cusine'] = 'Cusine';
$lang['venue_drink'] = 'Venue Drink';
$lang['venue_food'] = 'Venue Food';
$lang['food_price_range'] = 'Food Price Range';
$lang['drink_price_range'] = 'Drink Price Range';
$lang['venue_image'] = 'Venue Image';
$lang['country_code'] = 'Country Code';
$lang['country_name'] = 'Counrty';
$lang['status'] = 'Status';
$lang['created_data'] = 'Created Data';
$lang['updated_date'] = 'Updated Date';
$lang['created_by'] = 'Created By';
$lang['updated_by'] = 'Updated By';

$lang['create_venue']='Create Venue';
$lang['edit_venue']='Edit Venue';
$lang['delete_venue']='Delete Venue';
$lang['venue_search']='Venue Search';

$lang['venue']='Venue';
//$lang['action']='Actions';
//$lang['saved_successful']='Saved Successful';
//$lang['delete_successful']='Delete Successful';

