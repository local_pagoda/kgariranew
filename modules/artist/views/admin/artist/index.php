<!-- JQuery DataTable Css -->
<link href="<?php  echo base_url(); ?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<link href="<?php  echo base_url(); ?>assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2><?php  echo lang('artist')?></h2>
		</div>

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('artist')?>
						</h2>
					</div>

					<div class="body">
					</div>
				</div>
			</div>
		</div> <!-- close row clearfix -->

		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

				<div class="card">
					<div class="header">
						<h2>
							<?php  echo lang('artist')?>
						</h2>
						<div id="toolbar" style="padding:5px;height:auto">
							<p>
								<a href="#" class="btn bg-green waves-effect" onclick="create()" title="<?php   echo lang('create_artist')?>"><?php  echo lang('create')?></a>
								<a href="#" class="btn bg-blue-grey waves-effect" onclick="removeSelected()"  title="<?php   echo lang('delete_artist')?>"><?php  echo lang('remove_selected')?></a>
							</p>

						</div>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons">more_vert</i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="body">

						<table id="artist-table" class="table table-bordered table-striped table-hover dataTable " >
							<thead>
								<th ><?php echo "Sn"?></th>
								<th ><?php echo lang('artist_name')?></th>
								<th ><?php echo lang('artist_image')?></th>
								<th ><?php echo lang('contact_number')?></th>
								<th ><?php echo lang('genre_id')?></th>
								<!-- <th><?php echo lang('status')?></th> -->
								<!-- <th ><?php echo lang('created_date')?></th> -->
								<!-- <th ><?php echo lang('updated_date')?></th> -->
								<!-- <th ><?php echo lang('created_by')?></th> -->
								<!-- <th ><?php echo lang('updated_by')?></th> -->

								<th>Action</th>

							</thead>

							<tbody>
							</tbody>

						</table>
					</div><!-- close body -->
				</div><!-- close card -->
			</div><!-- close col -->
		</div><!-- close rowclearfix -->

		<!--for create and edit artist form-->
		<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" style="display: none">

			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="defaultModalLabel">Add or Edit Members</h4>
					</div>
					<div class="modal-body">
						<form id="form-artist" method="post" class="form-horizontal" enctype="multipart/form-data">

							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="artist_name"><?php echo lang("artist_name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="text" name="artist_name" id="artist_name" class="form-control" placeholder="<?php echo lang('artist_name'); ?>"></div>
									</div>
								</div>
							</div><!-- close row clearfix -->
							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="artist_description"><?php echo lang("artist_description")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><textarea name="artist_description" id="artist_description" class="form-control" ></textarea></div>
									</div>
								</div>
							</div><!-- close row clearfix -->
							

							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="artist_image"><?php echo lang("artist_image")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">

											<label id="upload_image_name" ></label>
											<input type="file" name="image" id="artist_image" class="form-control" placeholder="<?php echo lang('artist_image'); ?>">
											<input type="text" name="artist_image" id="upload_image" hidden>

											<button type="button" class="btn btn-default waves-effect" id="change_image" title="Change Image" style="display: none;" > <i class="material-icons">switch_camera</i>Change Image </button>

										</div>
									</div>
								</div>
							</div><!-- close row clearfix -->
							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="contact_number"><?php echo lang("contact_number")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="text" name="contact_number" id="contact_number" class="form-control" placeholder="<?php echo lang('contact_number'); ?>"></div>
									</div>
								</div>
							</div><!-- close row clearfix -->
							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="genre_id"><?php echo lang("genre_name")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line">
										<select style="background-color: #2b8714;"  name="genre_id" id="select" placeholder="Select Items">
										
									<?php foreach ($genres as $genre):  ?>
									
                                        <option value="<?php echo $genre['id'] ?>"><?php echo $genre['type'] ?></option>
                                        
                                     <?php endforeach; ?>   
                                    </select>
                                    </div>
									</div>
								</div>
							</div><!-- close row clearfix -->
							<div class="row clearfix">
								<div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
									<label for="status"><?php echo lang("status")?></label>
								</div>
								<div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
									<div class="form-group">
										<div class="form-line"><input type="radio" value="1" name="status" id="status1" checked /><label for="status1"><?php echo lang("general_yes")?></label> <input type="radio" value="0" name="status" id="status0" /><label for="status0"><?php echo lang("general_no")?></label></div>
									</div>
								</div>
							</div><!-- close row clearfix -->
							
							<input type="hidden" name="id" id="id"/>

						</form>
					</div> <!-- end body -->
					<div class="modal-footer">
						<button type="button" class="btn bg-green waves-effect" onClick="save()"><?php  echo  lang('general_save')?></button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal"><?php  echo  lang('general_cancel')?></button>
					</div>

				</div><!-- end content -->
			</div><!-- end modal-dialog -->



		</div><!-- end edit modal -->

	</div><!-- end container fluid -->
</section> <!-- end content -->


<!-- Input Mask Plugin Js -->
<script src="<?php  echo base_url(); ?>assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script language="javascript" type="text/javascript">
	var base_url = '<?php echo base_url(); ?>';

	var dataTable; 
	$(function(){
		
		$('.date').inputmask('yyyy-mm-dd', { placeholder: '____-__-__' });
		$('.datetime').inputmask('yyyy-mm-dd', { placeholder: '____-__-__' });
		$('.email').inputmask({ alias: "email" });

		dataTable = $('#artist-table').DataTable({
			dom: 'Bfrtip',
			// scrollX: true,
			"serverSide": true,
			buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
			],
			'ajax' : { url: "<?php  echo site_url('artist/admin/artist/json'); ?>",type: 'POST' },
			columns: [
			// { data: "id",name: "id"},
			{ data: function (data, type, row, meta) {
				return meta.row + meta.settings._iDisplayStart + 1;
			}},
			{ data: "artist_name",name: "artist_name"},
				// { data: "artist_description",name: "artist_description"},
				{ data: "artist_image",name: "artist_image"},
				{ data: "contact_number",name: "contact_number"},
				{ data: "genre_id",name: "genre_id"},
				// { data: "status",name: "status"},
				// { data: "created_date",name: "created_date"},
				// { data: "updated_date",name: "updated_date"},
				// { data: "created_by",name: "created_by"},
				// { data: "updated_by",name: "updated_by"},
				
				{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<button type='button' class='btn bg-grey waves-effect glyphicon glyphicon-pencil' data-toggle='modal' data-target='#edit-modal' onclick='edit("+table.row+")'></button>&nbsp";
					buttons += "<button class='btn bg-red waves-effect glyphicon glyphicon-trash' onclick='removeartist("+data.id+")'></button>";

					return buttons;
				}, name:'action',searchable: false},
				]
			});

		/*Change Image*/
		$("#change_image").click(function(){
			var filename = $('#upload_image').val();
			$.post("<?php echo site_url('artist/admin/artist/upload_delete')?>", {filename:filename}, function(){
				$("#change_image").hide();
				$('#artist_image').show();
				$('#upload_image').val('');
				$('#upload_image_name').text('');
				$.notify({
					title: "<strong><?php  echo lang('success')?>:</strong> ",
					message: "Removed previous image !"
				});
			});
		});

	});

	function create(){
		$('#form-artist')[0].reset();
		$('#id').val('');
		$('#edit-modal').modal('show');
		uploadReady(); //Uncomment This function if ajax uploading
	}	

	function edit(index)
	{
		var row = dataTable.row(index).data();
		console.log(row);
		$('#id').val(row.id);
		$("#form-artist").find('input:checkbox').prop('checked',false);
		$("#form-artist").find('input:text,select').val(function(i,v){

			if(row.status == '1')
			{
				$('input:radio[name=status][id=status1]').prop('checked',true);
			}else{
				$('input:radio[name=status][id=status0]').prop('checked',true);
			}
			return row[this.name];
		});
		$('select').selectpicker('render');

		$("#form-artist").find('input:checkbox').prop('checked',function(){
			// if($.inArray(this.value,row.array) >= 0)
			// { 
				return true; 
			// }

		});	

		if(Boolean(row.image))
		{
			$('#upload_image').val(row.image);
			$('#artist_image').hide();
			$('#upload_image_name').html("<img src='"+base_url+"/uploads/artist/thumb/"+row.image+"' max-width='400px' height='70px'>");
			$('#change_image').show();
		}
		uploadReady();
	}

	function removeartist(index)
	{
		$.post("<?php   echo site_url('artist/admin/artist/delete_json')?>", {id:[index]}, function(){
			dataTable.ajax.reload( null, false );
		});
	}

	function save()
	{
		var validator = $('#form-artist').validate();
		if(validator.form())
		{
			$.ajax({
				url: '<?php   echo site_url('artist/admin/artist/save')?>',
				data: $('#form-artist').serialize(),
				dataType: 'json',
				success: function(result){
					if(result.success)
					{
						$('#edit-modal').modal('hide');
						$('#form-artist')[0].reset();
						dataTable.ajax.reload( null, false );
					}
				},
				type: 'POST'
			});
		}
		
	}
	function uploadReady()
	{
		uploader=$('#artist_image');
		new AjaxUpload(uploader, {
			action: '<?php  echo site_url('artist/admin/artist/upload_image')?>',
			name: 'userfile',
			responseType: "json",
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){ 
                    // extension is not allowed 
                    $.notify({
                    	title: "<strong><?php  echo lang('error')?>:</strong> ",
                    	message: "Only JPG, PNG or GIF files are allowed"
                    });
                    return false;
                }
				//status.text('Uploading...');
			},
			onComplete: function(file, response){
				if(response.error==null){
					var filename = response.file_name;
					$('#artist_image').hide();
					$('#upload_image').val(filename);
					$('#upload_image_name').html("<img src='"+base_url+"/uploads/artist/thumb/"+filename+"' max-width='400px' height='70px'>");
					$('#upload_image_name').show();
					$('#change_image').show();
				}
				else
				{
					$.notify({
						title: "<strong><?php  echo lang('error')?>:</strong> ",
						message: response.error
					});
				}
			}		
		});	
	}	
</script>
<script type="text/javascript">
	/* Rules for the form*/
	$(function () {
		$('#form-artist').validate({
			rules: {
				// 'name': {
				// 	required: true
				// }
			},
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
	});
</script>